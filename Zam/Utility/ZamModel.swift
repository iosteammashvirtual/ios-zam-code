//
//  ZamModel.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import Foundation
import Social
import SystemConfiguration
import Alamofire
import CoreLocation

class ZamModel {
    
  var themecolor = UIColor(red: 0.0/255.0, green: 192.0/255.0, blue: 195.0/255.0, alpha: 1.0)
  
    
   func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    func buttonCorner(Button : UIButton,radius:Int)  {
        Button.layer.borderColor = UIColor.white.cgColor
        Button.layer.borderWidth = 1.0
        Button.clipsToBounds = true
        Button.layer.cornerRadius = CGFloat(radius)
    }
    func textFieldCorner(txtField : UITextField,radius:Int)  {
        txtField.layer.borderColor = themecolor.cgColor
        txtField.layer.borderWidth = 2.0
        txtField.clipsToBounds = true
        txtField.layer.cornerRadius = CGFloat(radius)
    }
    
    func buttonCornerWithThemeBorder(Button : UIButton,radius:Int,borderWidth:Int)  {
        Button.layer.borderColor = themecolor.cgColor
        Button.layer.borderWidth = CGFloat(borderWidth)
        Button.clipsToBounds = true
        Button.layer.cornerRadius = CGFloat(radius)
    }

      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func viewCorner(Button : UIView,radius:Int)  {
        Button.layer.borderColor = themecolor.cgColor
        Button.layer.borderWidth = 1.0
        Button.clipsToBounds = true
        Button.layer.cornerRadius = CGFloat(radius)
    }
    
   
    
    
    /////////////////////////////////////////////////////////////////////////////
    
    func textFieldCorner(txtfield : UITextField,radius:Int)  {
        txtfield.layer.borderColor = themecolor.cgColor
        txtfield.layer.borderWidth = 2.0
        txtfield.clipsToBounds = true
        txtfield.layer.cornerRadius = CGFloat(radius)
    }
    /////////////////////////////////////////////////////////////////////////////
    
    func CollCellCorner(collcell : UICollectionViewCell,radius:Int)  {
        collcell.layer.borderColor = themecolor.cgColor
        collcell.layer.borderWidth = 1.0
        collcell.clipsToBounds = true
        collcell.layer.cornerRadius = CGFloat(radius)
    }
    
     /////////////////////////////////////////////////////////////////////////////
    
    func textViewCorner(txtview : UITextView,radius:Int)  {
        txtview.layer.borderColor = themecolor.cgColor
        txtview.layer.borderWidth = 2.0
        txtview.clipsToBounds = true
        txtview.layer.cornerRadius = CGFloat(radius)
    }
    
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func imageCorner(img : UIImageView,radius:Int)  {
        img.layer.borderColor = themecolor.cgColor
        img.layer.borderWidth = 2.0
        img.clipsToBounds = true
        img.layer.cornerRadius = CGFloat(radius)
    }
    
     //////////////////////////////////////////////////////////////
    
    
    
    func validateEmail(txtEmail: String) -> Bool {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegEx).evaluate(with: txtEmail)
    }
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func regexPhoneNotCorrect(phoneNo: String) -> Bool {
        
        if phoneNo.characters.count == 10 {
            let emailRegex = "[6789][0-9]{4,10}"
            return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: phoneNo)
        }else {
        return false
        }
        
        
    }
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func GradiantColor(view:UIView){
//print(self.view.frame.size)
        let colorTop =  UIColor(red: 62.0/255.0, green: 196.0/255.0, blue: 175.0/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 82.0/255.0, green: 180/255.0, blue: 216.0/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height:  view.frame.height)
        view.layer.addSublayer(gradientLayer)
    }
      ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    func roundImageView(image :UIImageView){
        image.layer.borderColor = themecolor.cgColor
        image.layer.borderWidth = 2.0
        image.clipsToBounds = true
        image.layer.cornerRadius = CGFloat(image.frame.size.height/2)
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        image.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    ////////////////////////
    
    
    func getNoOfLines(label:UILabel) -> Int{
    
        var lineCount = 0;
        let textSize = CGSize(width : label.frame.size.width, height : CGFloat(Float.infinity));
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float(label.font.lineHeight));
        lineCount = rHeight/charSize
         return lineCount
    }
    
    func getNoOfLinesTextView(label:UITextView) -> Int{
        
        var lineCount = 0;
        let textSize = CGSize(width : label.frame.size.width, height : CGFloat(Float.infinity));
        let rHeight = lroundf(Float(label.sizeThatFits(textSize).height))
        let charSize = lroundf(Float((label.font?.lineHeight)!));
        lineCount = rHeight/charSize
        return lineCount
    }

    
    
    
    
    ///////////////////////
    func stringFromHtml(string: String) -> NSMutableAttributedString? {
        do {
            let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSMutableAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                 documentAttributes: nil)
                str.addAttribute(
                    NSFontAttributeName,
                    value: UIFont(
                        name: "HelveticaNeueLTStd-Cn",
                        size: 20.0)!,
                    range: NSRange(
                        location: 0,
                        length: str.length))
                
                return str
            }
        } catch {
        }
        return nil
    }
    
    func setSmileImageThemeColor(btn:UIButton,rateCount:Int){
        
        switch rateCount
        {
        case 0:
            btn.setImage(UIImage(named: "awesome50"), for: UIControlState.normal)
            break;
        case 1:
            btn.setImage(UIImage(named: "angry50"), for: UIControlState.normal)
            break;
        case 2:
            btn.setImage(UIImage(named: "sad50"), for: UIControlState.normal)
            break;
        case 3:
            btn.setImage(UIImage(named: "natural50"), for: UIControlState.normal)
            break;
        case 4:
            btn.setImage(UIImage(named: "happy50"), for: UIControlState.normal)
            break;
        case 5:
            btn.setImage(UIImage(named: "awesome50"), for: UIControlState.normal)
            break;
        default:
            break;
        }
    
    }
    
    
    func setSmileImageThemeColorBackGround(btn:UIButton,rateCount:Int){
        
        switch rateCount
        {
        case 0:
            btn.setBackgroundImage(UIImage(named: "awesome50"), for: UIControlState.normal)
            break;
        case 1:
            btn.setBackgroundImage(UIImage(named: "angry50"), for: UIControlState.normal)
            break;
        case 2:
            btn.setBackgroundImage(UIImage(named: "sad50"), for: UIControlState.normal)
            break;
        case 3:
            btn.setBackgroundImage(UIImage(named: "natural50"), for: UIControlState.normal)
            break;
        case 4:
            btn.setBackgroundImage(UIImage(named: "happy50"), for: UIControlState.normal)
            break;
        case 5:
            btn.setBackgroundImage(UIImage(named: "awesome50"), for: UIControlState.normal)
            break;
        default:
            break;
        }
        
    }

    
    
    
    
    func setSmileImageWhite(btn:UIButton,rateCount:Int){
        
        switch rateCount
        {
        case 0:
            btn.setImage(UIImage(named: "awesome50w"), for: UIControlState.normal)
            break;
        case 1:
            btn.setImage(UIImage(named: "angry50w"), for: UIControlState.normal)
            break;
        case 2:
            btn.setImage(UIImage(named: "sad50w"), for: UIControlState.normal)
            break;
        case 3:
            btn.setImage(UIImage(named: "natural50w"), for: UIControlState.normal)
            break;
        case 4:
            btn.setImage(UIImage(named: "happy50w"), for: UIControlState.normal)
            break;
        case 5:
            btn.setImage(UIImage(named: "awesome50w"), for: UIControlState.normal)
            break;
        default:
            break;
        }
        
    }
    
   
    
    
    ////////////////////////////////////////////////
    
    func condenseWhitespace(str: String) -> String {
        return str.components(separatedBy: CharacterSet.whitespacesAndNewlines)
            .filter { !$0.isEmpty }
            .joined(separator: " ")
    }
    
//    func getLatLongFromAddress(address: String) -> CLLocationCoordinate2D {
//        
//        CLGeocoder().geocodeAddressString("\(address)") { (placemarks, error) -> Void in
//            
//            if let firstPlacemark = placemarks?[0] {
//                print(firstPlacemark.location)
//                coordinate = (firstPlacemark.location?.coordinate)!
//            }   
//        }
//        return coordinate
//    }
    
    
//    func getLatLongFromAddress(address : String,completionHandler: @escaping (CLLocationCoordinate2D) -> ())
//    {
//       
//        CLGeocoder().geocodeAddressString("\(address)") { (placemarks, error) -> Void in
//            
//            if let firstPlacemark = placemarks?[0] {
//                print(firstPlacemark.location)
//                completionHandler((firstPlacemark.location?.coordinate)!)
//               // coordinate = (firstPlacemark.location?.coordinate)!
//            }
//        }
//        
//    }
}


