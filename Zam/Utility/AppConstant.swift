//
//  AppConstant.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import Foundation



// local
//let loginBase_url =  "http://192.168.1.8:91/oauth/token"
//let appMainUrl = "http://192.168.1.8:76/api/"
//let appAuthUrl = "http://192.168.1.8:91/api/"
//let weburl = "http://192.168.1.8:88/#/autoLogin"

// live
let loginBase_url =  "https://zam.mashvirtual.com/MASHAuthenticateV1/oauth/token"
let appMainUrl = "https://zam.mashvirtual.com/MASHPrepAPIV1/api/"
let appAuthUrl = "https://zam.mashvirtual.com/MASHAuthenticateV1/api/"
let weburl = "https://zam.mashvirtual.com/#/autoLogin"

let generateOtp =  "Accounts/GenerateOtp"
let RegisterApp = "Accounts/RegisterApp"
let GenerateForgotOtp = "Accounts/GenerateForgotOtp"
let ResetPasswordWithOtp = "Accounts/ResetPasswordWithOtp"
let UpdateProfileAccounts = "Accounts/UpdateProfile"
let ChangePassword = "Accounts/ChangePassword"
let GetAllRolesApp = "Accounts/GetAllRolesForApp"
let UpdateProfile = "App/UpdateProfile"
let SearchNearByCentre = "App/SearchNearByCentre"
let GetCentreDetail = "App/GetCentreDetail"
let CentreComparison = "App/CentreComparisonIOS"
let GetUserInfo = "App/GetUserInfo"
let GetUserDetails = "App/GetUserDetails"
let UpdateProfileImage = "App/UpdateProfileImage"
let GetCityList = "App/GetCityList"
let GetUserFavorites = "App/GetUserFavorites"
let CreateExamsInterest = "App/CreateExamsInterest"
let AddToFavourite = "App/AddToFavourite"
let RemoveFavouriteFromList = "App/RemoveFavouriteFromList"
let GetSearchCity = "App/GetSearchCity"
let GetSearchKeywords = "App/GetSearchKeywords"
let SearchCentrebyKeyword = "App/SearchCentrebyKeyword"
let ViewTrainerProfile = "App/ViewTrainerProfile"
let GetCentreCourses = "App/GetCentreCourses"
let GetSearchCentreAssociate = "App/GetSearchCentreAssociate"
let CreateCentreAssociate = "App/CreateCentreAssociate"
let RemoveCentreAssociate = "App/RemoveCentreAssociate"
let SearchProducts = "Shop/SearchProducts"
let ViewAllCentreRates = "App/ViewAllCentreRates"
let GetRatingCritaria = "App/GetRatingCritaria"
let AddCentreRate = "App/AddCentreRate"
let UpdateUserRecomandation = "App/UpdateUserRecomandation"
let ViewCentreReviews = "App/ViewCentreReviews"
let AddCentreRiview = "App/AddCentreRiview"
let PayOrder = "Payment/PayUMoneyPayOrder"
let ViewTrainerRates = "App/ViewTrainerRates"
let GetTrainerRatingCritaria = "App/GetTrainerRatingCritaria"
let AddTrainerRate = "App/AddTrainerRate"
let UpdateTrainerRecomandation = "App/UpdateTrainerRecomandation"
let ViewTrainerReviews = "App/ViewTrainerReviews"
let AddTrainerReview = "App/AddTrainerReview"
let UpdateRole = "App/UpdateRole"
let AboutApp = "App/AboutApp"
let ViewAllUserReviews = "App/ViewAllUserReviews"
let ViewAllUserRates = "App/ViewAllUserRates"
let GetTrainerInterest = "App/GetTrainerInterest"







let networkError = "Network error."
let WrongOtp = "The OTP you entered could not be authenticated. Please try again."
let ServerError = "Server error."
let MobilenoNil = "Please enter Mobile Number"
let MobilenoNotValid = "Please enter valid Mobile Number."
let PasswordNil = "Please enter Password"
let NewPasswordNil = "Enter New Password"
let ConfirmMobilenoNil   = "Please Re-Enter Mobile Number"
let PasswordShort = "Password length must be at least 6 characters"
let passwordlong = "Password can contain up to 15 characters"
let mobileNoNotSame = "Mobile Number does not match"
let firstNamenil = "Please enter First Name"
let lastNamenil = "Please enter Family Name"
let cancelRate = "Do you want to cancel your rating."
let emailnil = "Please enter Email Address"
let emailnotvalid = "Please enter valid Email Address"
let confirmSaveChange = "Do you want to exit without saving changes to your profile?"
let passNotSame = "New Password  & Confirm Password does not Match"
let olpPasswordNil = "Enter Old Password"
let ConfirmPassnil = "Enter Confirm Password"
let AssociatedExamNil = "Please select atleast one exams"
let InterestNil = "Please select atleast one role"
let compareNil = "Choose at least two centres for comparison"
let compareMoreThanFour = "Maximum of 4 centres can be compared at a time"
let CityNil = "Please Select City."
let OTPNil = "Please enter OTP."
let ReviewNil = "Please enter your Review"

let OTPExpire = "Your OTP may have already expired. To request a new OTP click on Re-Send link."
let passwordChanged = "Your password has been changed"
let centerAlreadyAdded = "You are already associated with this centre"
let confirmRemoveFromCompare = "Are you sure you want to remove this centre from compare?"
let selectJobFirst = "Please select your job type"
let ShippingAddressNil = "Please select shipping address"
let EnterValidCoupon = "Please enter valid coupon"
let QuantityUpdated = "Item successfully updated to "




//Shop
let ShopProductList = "Shop/ProductList"
let ShopTopAds = "Ad/GetTopAds"
let AddToCart = "Shop/CreateShoppingCart"
let ProductDetailsList = "Shop/ProductDetails"
let CartList = "Shop/GetShoppingCart"
let removeCart = "Shop/RemoveShoppingCart"
let updateShoppingCart = "Shop/UpdateShoppingCart"
let getShippingCart = "Shop/GetShippingByPrimaryID"
let getStateListing = "App/GetStateListShip"
let getCityListing = "App/GetCityList"
let createShippingAddress = "/Shop/CreateShipping"
let buyNowOrder = "Shop/ByNowProceed"
let couponDiscount = "Shop/GetCouponDiscount"
let productRatingCriteria = "Shop/GetProductRatingCritaria"
let addProductRating = "Shop/AddProductRate"
let viewProductRating = "Shop/ViewProductRates"
let updateProductRecomend = "Shop/UpdateProductRecomandation"
let viewProductReview = "Shop/ProductReviews"
let addProductReview = "Shop/AddProductReview"
let viewCourseRating = "App/ViewCourseRates"
let courseRatingCriteria = "app/GetCourseRatingCritaria"
let courseAddRating = "App/AddCourseRate"
let viewCourseReview = "App/ViewExamCourseReviews"
let addCourseReview = "App/AddCentreCourseRiview"
let updateCourseRecommend = "App/UpdateCourseRecomandation"

//"Enter correct Old Password"
//
//[6:18]
//"Enter New Password"
//
//[6:19]
//"Password must be atleast 6 character"
//
//[6:19]
//"Enter Confirm Password"
//
//[6:19]
//"New Password  & Confirm Password does not Match"
//





//"Please Re-Enter Mobile Number"
//
//"Password can contain up to 15 characters"
//
//User Information Screen:
//
//
//
//[5:31]
//"First Name can contain up to 25 characters"
//
//[5:32]
//"Please enter valid Family Name"
//[5:32]
//"Please enter valid First Name"
//[5:32]
//"Family Name can contain up to 25 characters"




