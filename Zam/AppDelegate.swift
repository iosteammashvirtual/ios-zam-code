//
//  AppDelegate.swift
//  Zam
//
//  Created by Dinesh Rana on 12/27/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UITabBarControllerDelegate,UINavigationControllerDelegate {
    
    var isFullScreen:Bool = false
    var centerIdForMyRate = String()
    var comparedCenterId = Int()
     var parameterForMyRate = NSDictionary()
    var currentState = ""
     var userLocation = CLLocation()
    var comeFromCenterReview = ""
     var comeFromCenterRate = ""
     var comeFromTrainerRate = ""
     var comeFromTrainerReview = ""
    var comeFromSearchOnMap:Bool = false
    var searchparameter = NSDictionary()
    var compareSelected : Bool = false
    var SwipeRightOpen:Bool = false
    var addToCompareClicked:Bool = true
    var window: UIWindow?
     let defaults = UserDefaults.standard
     var tabBarController: UITabBarController?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        Thread.sleep(forTimeInterval: 3)
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.willEnterFullScreen), name: NSNotification.Name(rawValue: "MPMoviePlayerWillEnterFullscreenNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.willExitFullScreen), name: NSNotification.Name(rawValue: "MPMoviePlayerWillExitFullscreenNotification"), object: nil)
        
        
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        GMSServices.provideAPIKey("AIzaSyA1y2wIYrAuF-cKUxtA0JSRcXX3pNOdito")
                self.window!.makeKeyAndVisible()
        IQKeyboardManager.sharedManager().enable = true
        
        let viewObject:LaunchViewController = LaunchViewController(nibName: "LaunchViewController", bundle: nil)
        
        let navigation: UINavigationController = UINavigationController(rootViewController: viewObject)
        navigation.navigationBar.isHidden = true
        
        
        self.window!.rootViewController = navigation
        
                let viewController1 = NearViewController(nibName: "NearViewController", bundle: nil)
                let tabBarItem1: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "locationtabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "locationtab.png"))
                tabBarItem1.title="Near Me"
                viewController1.tabBarItem=tabBarItem1
        
                let viewController2 = CompareViewController(nibName: "CompareViewController", bundle: nil)
        
                let tabBarItem2: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "comparetabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "comparetab.png"))
                tabBarItem2.title="Compare"
                viewController2.tabBarItem=tabBarItem2
        
                let viewController3 = ShopViewController(nibName: "ShopViewController", bundle: nil)
        
                let tabBarItem3: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "shoptabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "shoptab.png"))
                UserDefaults.standard.setValue(false, forKey: "navigate_Search")
                UserDefaults.standard.setValue(nil, forKey: "dataList")
                tabBarItem3.title="Shop"
                viewController3.tabBarItem=tabBarItem3
        
        let viewController4 = BookNowViewController(nibName: "BookNowViewController", bundle: nil)
                
             let tabBarItem4: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "Book-nowtabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "Book-nowtab.png"))
                tabBarItem4.title="Book Now"

                viewController4.tabBarItem=tabBarItem4

        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.selected)
         UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(white: 255/255.0, alpha: 0.3)], for:.normal)
         self.tabBarController = UITabBarController()
      self.tabBarController?.tabBar.tintColor = UIColor.white
    //  self.tabBarController?.tabBar.backgroundColor = ZamModel().themecolor
        self.self.tabBarController?.tabBar.barTintColor = ZamModel().themecolor
        
        
                let navController3: UINavigationController = UINavigationController(rootViewController: viewController1) as UINavigationController
                navController3.navigationBar.isHidden = true
        
                let navController2: UINavigationController = UINavigationController(rootViewController: viewController2) as UINavigationController
                navController2.navigationBar.isHidden = true
        
                let navController1: UINavigationController = UINavigationController(rootViewController: viewController3) as UINavigationController
                navController1.navigationBar.isHidden = true
        
            let navController4: UINavigationController = UINavigationController(rootViewController: viewController4) as UINavigationController
            navController4.navigationBar.isHidden = true

       
        self.tabBarController?.setViewControllers([navController3, navController2,navController1,navController4], animated: false);

      
        
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func makeRootVieController(){
        
        let viewObject:LoginViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
        
        let navigation: UINavigationController = UINavigationController(rootViewController: viewObject)
        navigation.navigationBar.isHidden = true
        self.window!.rootViewController = navigation
        
        
        
        let viewController1 = NearViewController(nibName: "NearViewController", bundle: nil)
        let tabBarItem1: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "locationtabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "locationtab.png"))
        tabBarItem1.title="Near Me"
        viewController1.tabBarItem=tabBarItem1
        
        let viewController2 = CompareViewController(nibName: "CompareViewController", bundle: nil)
        
        let tabBarItem2: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "comparetabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "comparetab.png"))
        tabBarItem2.title="Compare"
        viewController2.tabBarItem=tabBarItem2
        
        let viewController3 = ShopViewController(nibName: "ShopViewController", bundle: nil)
        
        let tabBarItem3: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "shoptabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "shoptab.png"))
        UserDefaults.standard.setValue(false, forKey: "navigate_Search")
        UserDefaults.standard.setValue(nil, forKey: "dataList")
        tabBarItem3.title="Shop"
        viewController3.tabBarItem=tabBarItem3
        
        let viewController4 = BookNowViewController(nibName: "BookNowViewController", bundle: nil)
        
        let tabBarItem4: UITabBarItem = UITabBarItem(title: nil, image: UIImage(named: "Book-nowtabUnselected.png")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), selectedImage: UIImage(named: "Book-nowtab.png"))
        tabBarItem4.title="Book Now"
        
        viewController4.tabBarItem=tabBarItem4
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for:.selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(white: 255/255.0, alpha: 0.3)], for:.normal)
        self.tabBarController = UITabBarController()
        self.tabBarController?.tabBar.tintColor = UIColor.white
        //  self.tabBarController?.tabBar.backgroundColor = ZamModel().themecolor
        self.self.tabBarController?.tabBar.barTintColor = ZamModel().themecolor
        
        
        let navController3: UINavigationController = UINavigationController(rootViewController: viewController1) as UINavigationController
        navController3.navigationBar.isHidden = true
        
        let navController2: UINavigationController = UINavigationController(rootViewController: viewController2) as UINavigationController
        navController2.navigationBar.isHidden = true
        
        let navController1: UINavigationController = UINavigationController(rootViewController: viewController3) as UINavigationController
        navController1.navigationBar.isHidden = true
        
        let navController4: UINavigationController = UINavigationController(rootViewController: viewController4) as UINavigationController
        navController4.navigationBar.isHidden = true
        
        
        self.tabBarController?.setViewControllers([navController3, navController2,navController1,navController4], animated: false);
        
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = appDelegate.tabBarController
    }
  
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
    
        if self.window?.rootViewController?.presentedViewController is CenterComparisionViewController {
            
            let secondController = self.window!.rootViewController!.presentedViewController as! CenterComparisionViewController
            
            if secondController.isPresented
            {
                return UIInterfaceOrientationMask.landscapeLeft;
            }
            else
            {
                return UIInterfaceOrientationMask.portrait;
            }
        } else
        {
            return UIInterfaceOrientationMask.portrait;
        }
        
    }
    
    func willEnterFullScreen(notification: NSNotification)
    {
        self.isFullScreen = true
    }
    
    func willExitFullScreen(notification: NSNotification)
    {
        self.isFullScreen = false
    }

    
}

