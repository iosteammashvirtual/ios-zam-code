//
//  BookNowViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/2/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class BookNowViewController: UIViewController {

    @IBOutlet weak var btnRegister: UIButton!
    var ScrollEnable:Bool = true
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(BookNowViewController.SwipeRightGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        ZamModel().buttonCorner(Button: btnRegister, radius: Int(10.0))
        // Swipe
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
      
       // colseSliderView()
        
//        if ScrollEnable == false{
//            self.view.addGestureRecognizer(swipeLeft)
//        }else {
//        self.view.removeGestureRecognizer(swipeLeft)
//        }
        //  navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnRegisterClicked(_ sender: Any) {
        UIApplication.shared.openURL(URL(string: "https://www.ieltsidpindia.com/")!)
        
//        let vc = WebViewController(nibName: "WebViewController", bundle: nil)
//        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func sliderclicked(_ sender: Any) {
        SliderOpen()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if ScrollEnable == false{
         ScrollEnable = true
        }
       
    }
    
//    func SwipeLeftGesture(gesture: UIGestureRecognizer) {
//        if(ScrollEnable == false)
//        {
//            ScrollEnable = true
//            if let swipeGesture = gesture as? UISwipeGestureRecognizer {
//                switch swipeGesture.direction {
//                case UISwipeGestureRecognizerDirection.left:
//                    let viewMenuBack : UIView = view.subviews.last!
//                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                        var frameMenu : CGRect = viewMenuBack.frame
//                        frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
//                        viewMenuBack.frame = frameMenu
//                        viewMenuBack.layoutIfNeeded()
//                        viewMenuBack.backgroundColor = UIColor.clear
//                    }, completion: { (finished) -> Void in
//                        viewMenuBack.removeFromSuperview()
//                        //  self.mapView.backgroundColor = UIColor(white: 0/255.0, alpha: 0.0)
//                    })
//                default:
//                    break
//                }
//            }}
//    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnSearchClicked(_ sender: Any) {
        let vc = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func SliderOpen(){
        let menuVC : SliderViewController = SliderViewController(nibName: "SliderViewController", bundle: nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        appDelegate.SwipeRightOpen = true
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.frame = CGRect(x:0 - UIScreen.main.bounds.size.width, y:0, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame = CGRect(x:0 , y:0, width:UIScreen.main.bounds.size.width , height:UIScreen.main.bounds.size.height);
        }, completion:nil)
    }
    func SwipeRightGesture(gesture: UIGestureRecognizer) {
        
        if appDelegate.SwipeRightOpen == false
        {
            SliderOpen()
        }
    }
}
