//
//  AboutViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/27/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        getAboutData()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getAboutData(){
        self.view.makeToastActivity(message: "Loading", view: self.view)
       
        AppEngine().getAboutUsData() { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                
              var dataDic = response?.object(forKey: "data") as! NSDictionary
              self.lblAbout.attributedText = ZamModel().stringFromHtml(string: dataDic.value(forKey: "shortDesc") as! String)
              
                 self.lblAbout.textColor = ZamModel().themecolor
                self.lblDescription.attributedText = ZamModel().stringFromHtml(string: dataDic.value(forKey: "longDesc") as! String)
                 self.lblDescription.textColor = ZamModel().themecolor
                
                
                
               
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }

    
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
