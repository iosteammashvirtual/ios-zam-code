//
//  AccordionHeaderView.swift
//  FZAccordionTableViewExample
//
//  Created by Krisjanis Gaidis on 10/5/15.
//  Copyright © 2015 Fuzz. All rights reserved.
//

import UIKit

class AccordionHeaderView: FZAccordionTableViewHeaderView {
    
    @IBOutlet weak var btnRate: UIButton!
    
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var lblName: UILabel!
    
    
    static let kDefaultAccordionHeaderViewHeight: CGFloat = 94.0;
    static let kAccordionHeaderViewReuseIdentifier = "AccordionHeaderViewReuseIdentifier";
}
