//
//  MyRateViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/30/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class MyRateViewController: UIViewController {

    static private let kTableViewCellReuseIdentifier = "TableViewCellReuseIdentifier"
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var lblRateNil: UILabel!
    var openedSection:Int = 0
    @IBOutlet weak var tableView: FZAccordionTableView!
    var dataArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowMultipleSectionsOpen = false
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: MyRateViewController.kTableViewCellReuseIdentifier)
        
        tableView.register(UINib(nibName: "AccordionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier)
        
        let nibName = UINib(nibName: "MyRateTableViewCell", bundle:nil)
        tableView.register(nibName, forCellReuseIdentifier: "MyRateTableViewCell")
        
        let nibName1 = UINib(nibName: "MyRateModifyTableViewCell", bundle:nil)
        tableView.register(nibName1, forCellReuseIdentifier: "MyRateModifyTableViewCell")
       self.tabBarController?.tabBar.isHidden = true
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        getdata()
    }
    
    func getdata(){
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let  parameters = ["userID" : UserDefaults.standard.object(forKey: "userId") as! String,"PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String]
        print(parameters as! NSDictionary)
        
        AppEngine().getMyRateList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
             //   debugPrint(response)
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.lblRateNil.text = ""
               self.tableView.reloadData()
            } else {
                self.lblRateNil.text = response?.object(forKey: "message") as! String
                //self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    func ModifyClicked(sender:UIButton){
        appDelegate.comeFromCenterRate = "myrate"
   let data = dataArray.object(at: sender.tag) as! NSDictionary
        print(data)
        let array = (data.object(forKey: "criteriaRates") as! NSArray).mutableCopy() as! NSMutableArray
        let SelectedRateArray = NSMutableArray()
        for i in 0..<array.count{
            SelectedRateArray.add((array[i] as AnyObject).object(forKey: "criteriaRateValue") as! NSNumber)
        }
        
        
        
        /////////////////  For Add in Naviagtion While Back
        let rateParameter = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: data.value(forKey: "centreID") as! NSNumber),"userID":UserDefaults.standard.object(forKey: "userId") as! String]
        
        appDelegate.centerIdForMyRate = String(describing: data.value(forKey: "centreID") as! NSNumber)
        appDelegate.parameterForMyRate = rateParameter as NSDictionary
        /////////////////////////
        
        
        let vc = RateNowViewController(nibName: "RateNowViewController", bundle: nil)
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : data.value(forKey: "centreID") as! NSNumber,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"RateingID":data.object(forKey: "ratingID") as! NSNumber] as [String : Any]
        vc.parameter = parameters as NSDictionary
        vc.cenetrID = String(describing: data.value(forKey: "centreID") as! NSNumber)
        vc.comeFromMyRate = true
        //  print(response?.object(forKey: "data") as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: false)
        }
        
        
    
    
}

// MARK: - Extra Overrides -

//extension MyRateViewController {
//     func prefersStatusBarHidden() -> Bool {
//        return true
//    }
//}

// MARK: - <UITableViewDataSource> / <UITableViewDelegate> -

extension MyRateViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         let dataarray = ((self.dataArray.object(at:section) as AnyObject).object(forKey: "criteriaRates") as? NSArray)?.mutableCopy() as! NSMutableArray
        return dataarray.count + 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return AccordionHeaderView.kDefaultAccordionHeaderViewHeight
    }
    
//    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
//        return self.tableView(tableView, heightForRowAtIndexPath: indexPath)
//    }
//    
//    func tableView(tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
//        return self.tableView(tableView, heightForHeaderInSection:section)
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let dataarray = ((self.dataArray.object(at: indexPath.section) as AnyObject).object(forKey: "criteriaRates") as? NSArray)?.mutableCopy() as! NSMutableArray
        
        if indexPath.row == dataarray.count{
        
            let cell:MyRateModifyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyRateModifyTableViewCell") as! MyRateModifyTableViewCell
            ZamModel().buttonCorner(Button: cell.btnModify, radius: 10)
             cell.btnModify.tag = indexPath.section
            cell.btnModify.addTarget(self, action: #selector(MyRateViewController.ModifyClicked), for: .touchUpInside)
            
            return cell
        
        }else{
            let cell:MyRateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyRateTableViewCell") as! MyRateTableViewCell
            
            
            cell.lblType.text = (dataarray[indexPath.row] as AnyObject).value(forKey: "criteriaName") as? String
            cell.lblRateCount.text = String(describing: ((dataarray[indexPath.row] as AnyObject).value(forKey: "criteriaRateValue") as! NSNumber)) + "/5"
            
            ZamModel().setSmileImageThemeColor(btn: cell.btnRate, rateCount: Int(((dataarray[indexPath.row] as AnyObject).value(forKey: "criteriaRateValue") as? NSNumber)!))
            
            return cell
        }
        
      
        }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerCell =  tableView.dequeueReusableHeaderFooterView(withIdentifier: AccordionHeaderView.kAccordionHeaderViewReuseIdentifier) as! AccordionHeaderView

        headerCell.lblName.text = (self.dataArray[section] as AnyObject).value(forKey: "centreName") as? String
         headerCell.lblDate.text = (self.dataArray[section] as AnyObject).value(forKey: "rateDate") as? String
      headerCell.lblRateCount.text = String(describing: ((self.dataArray[section] as AnyObject).value(forKey: "overAllRateValue") as! NSNumber)) + "/5"
      
        
         ZamModel().setSmileImageThemeColorBackGround(btn: headerCell.btnRate, rateCount: Int(((self.dataArray[section] as AnyObject).value(forKey: "overAllRateValue") as? NSNumber)!))
        
        
        let imageurl = (dataArray[section] as AnyObject).value(forKey: "centreLogo") as! String
        headerCell.btnProfileImage.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
        ZamModel().buttonCornerWithThemeBorder(Button: headerCell.btnProfileImage, radius: Int(headerCell.btnProfileImage.frame.size.height / 2), borderWidth: 2)
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
}

// MARK: - <FZAccordionTableViewDelegate> -

extension MyRateViewController : FZAccordionTableViewDelegate {
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        openedSection = section
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}
