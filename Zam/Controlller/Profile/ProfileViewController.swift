//
//  ProfileViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/5/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import AssetsLibrary
import MobileCoreServices
import MessageUI
import CoreLocation


class ProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverPresentationControllerDelegate ,MFMailComposeViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,CropViewControllerDelegate{
    
    @IBOutlet weak var addressTopConstraint: NSLayoutConstraint!
   
    @IBOutlet weak var refisteredAsTitleTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var registeredAsTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var aboutTitleTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var aboutTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var associatedExamTitleTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var associatedExamTopConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgAddress: UIImageView!
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var lblAboutUs: UILabel!
    @IBOutlet weak var lblAboutTitle: UILabel!
    @IBOutlet weak var lblAssociatedExamTitle: UILabel!
    @IBOutlet weak var btnContinueReading: UIButton!
    @IBOutlet weak var lblRegister: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnProfileBackGroumd: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var btnCall: UIButton!
    
    
    var AssociatedExamArray = NSMutableArray()
    var AssociatedCenterArray = NSMutableArray()
     var imagePicker = UIImagePickerController()
    var pickedImage = UIImage()
    var dataDic = NSDictionary()
    
    var isImageChange:Bool = false
    var model = ZamModel()
    
    @IBOutlet weak var btnrate: UIButton!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var lblReview: UILabel!
    
    
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        ZamModel().buttonCornerWithThemeBorder(Button: btnProfileBackGroumd, radius:Int(btnProfileBackGroumd.frame.size.width/2.0),borderWidth: 6)
         ZamModel().buttonCorner(Button: btnProfilePic, radius: Int(btnProfilePic.frame.size.width/2.0))
        
        let nibName = UINib(nibName: "ProfileAssociatedCentresTableCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "ProfileAssociatedCentresTableCell")
        
        let nibName1 = UINib(nibName: "WorkingHoursHeaderTableCell", bundle:nil)
        tblView.register(nibName1, forCellReuseIdentifier: "WorkingHoursHeaderTableCell")
        
        
        let nib = UINib(nibName: "CenterimagesCollectionCell", bundle: nil)
        collView.register(nib, forCellWithReuseIdentifier: "CenterimagesCollectionCell")
        collView.backgroundColor  = UIColor.clear
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        if isImageChange == false {
         getdata()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        isImageChange = false
    }
    func getdata(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID" : UserDefaults.standard.object(forKey: "userId") as! String] as [String : Any]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getUserInfo(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
               self.dataDic = response?.object(forKey: "data") as! NSDictionary
//                debugPrint(response)
                print(self.dataDic)
                let imageurl = self.dataDic.value(forKey: "photoPath") as! String
                self.btnProfilePic.sd_setBackgroundImage(with: NSURL(string: imageurl) as URL!, for: .normal)
                self.btnCall.setTitle( "     " + String(describing: self.dataDic.value(forKey: "stdCode") as! String) + " " +  String(describing: self.dataDic.value(forKey: "mobileNo") as! String), for: .normal)
                self.btnMail.setTitle("     " + String(describing: self.dataDic.value(forKey: "emailID") as! String), for: .normal)
                self.lblName.text = String(describing: self.dataDic.value(forKey: "firstName") as! String) + " " + String(describing: self.dataDic.value(forKey: "lastName") as! String)
                if self.dataDic.value(forKey: "isTrainer") as! Bool == true {
                self.btnrate.isHidden  = false
                self.btnReview.isHidden  = false
                self.lblReview.isHidden = false
                self.lblRate.isHidden = false
                self.lblRate.text = String(Int(self.dataDic.value(forKey: "overAllRateValue") as! NSNumber)) + "/5"
                ZamModel().setSmileImageWhite(btn: self.btnrate, rateCount: Int(self.dataDic.value(forKey: "overAllRateValue") as! NSNumber))
                self.lblReview.text = String(describing: self.dataDic.value(forKey: "noReview") as! NSNumber)
                }else {
                    self.btnrate.isHidden  = true
                    self.btnReview.isHidden  = true
                    self.lblReview.isHidden = true
                    self.lblRate.isHidden = true
                }
              //  self.lblRegister.text = userRoles
                let role = self.dataDic.object(forKey: "userRoles") as? String
              self.lblRegister.attributedText = ZamModel().stringFromHtml(string: role!)
                 self.lblRegister.textColor = ZamModel().themecolor
                var address = String()
//                let address = String(describing: self.dataDic.value(forKey: "address1") as! String) + "," + String(describing: self.dataDic.value(forKey: "address2") as! String) + "," + String(describing: self.dataDic.value(forKey: "city") as! String) + "," +  String(describing: self.dataDic.value(forKey: "state") as! String) + ","  + String(describing: self.dataDic.value(forKey: "postCode") as! String)
                
                if String(describing: self.dataDic.value(forKey: "address1") as! String) != ""{
                address = address + String(describing: self.dataDic.value(forKey: "address1") as! String) + ", "
                }
                if String(describing: self.dataDic.value(forKey: "address2") as! String) != ""{
                    address = address + String(describing: self.dataDic.value(forKey: "address2") as! String) + ", "
                }
                if String(describing: self.dataDic.value(forKey: "city") as! String) != ""{
                    address = address + String(describing: self.dataDic.value(forKey: "city") as! String) + ", "
                }
                if String(describing: self.dataDic.value(forKey: "state") as! String) != ""{
                    address = address + String(describing: self.dataDic.value(forKey: "state") as! String) + ", "
                }
                if String(describing: self.dataDic.value(forKey: "postCode") as! String) != ""{
                    address = address + String(describing: self.dataDic.value(forKey: "postCode") as! String) + ", "
                }
                if address != ""{
                    address = String(address.characters.dropLast())
                    address = String(address.characters.dropLast())
                }
                
                if ZamModel().condenseWhitespace(str: address) != ""{
                     self.imgAddress.isHidden = false
                    self.addressTopConstraint.constant = 20
                    self.lblAddress.text = ZamModel().condenseWhitespace(str: address)
                }else {
                 self.addressTopConstraint.constant = 0
                 self.lblAddress.text = ""
                 self.imgAddress.isHidden = true
                }
                self.lblAboutUs.text = self.dataDic.object(forKey: "profileDesc") as? String
                if self.lblAboutUs.text == ""{
                self.lblAboutTitle.text = ""
                self.aboutTitleTopConstraint.constant = 0
                self.aboutTopConstraint.constant = 0
               self.btnContinueReading.isHidden = true
                }else {
                    self.aboutTitleTopConstraint.constant = 20
                    self.aboutTopConstraint.constant = 20
                    self.lblAboutTitle.text = "About"
                if self.model.getNoOfLines(label: self.lblAboutUs) > 3{
                    self.lblAboutUs.numberOfLines = 3
                    self.btnContinueReading.isHidden = false
                }else {
                    self.lblAboutUs.numberOfLines = 0
                    self.btnContinueReading.isHidden = true
                }
                }
                if self.dataDic.value(forKey: "trainerExams") != nil  && !(self.dataDic.value(forKey: "trainerExams") is NSNull) {
                    self.AssociatedExamArray = (self.dataDic.value(forKey: "trainerExams") as! NSArray).mutableCopy() as! NSMutableArray
                    if self.AssociatedExamArray.count > 0{
                         self.lblAssociatedExamTitle.text = "Associated Exams"
                        if self.AssociatedExamArray.count % 2 == 0 {
                            self.collViewHeight.constant = CGFloat(self.AssociatedExamArray.count / 2 * 60) + 20
                        }else {
                            self.collViewHeight.constant = CGFloat((self.AssociatedExamArray.count + 1) / 2 * 60) + 20
                        }
                        self.associatedExamTopConstraint.constant = 20
                        self.associatedExamTitleTopConstraint.constant = 20
                    }else {
                        
                        self.associatedExamTopConstraint.constant = 0
                        self.associatedExamTitleTopConstraint.constant = 0
                        self.lblAssociatedExamTitle.text = ""
                        self.collViewHeight.constant = 0
                    }
                }else {
                    self.associatedExamTopConstraint.constant = 0
                    self.associatedExamTitleTopConstraint.constant = 0
                   self.lblAssociatedExamTitle.text = ""
                   self.collViewHeight.constant = 0
                }
                if self.dataDic.value(forKey: "trainerAssoCentres") != nil  && !(self.dataDic.value(forKey: "trainerAssoCentres") is NSNull) {
                    
                 self.AssociatedCenterArray = (self.dataDic.value(forKey: "trainerAssoCentres") as! NSArray).mutableCopy() as! NSMutableArray
                    
                    if self.AssociatedCenterArray.count > 0 {
                  
                        self.tblViewHeight.constant = (CGFloat(30)) + (CGFloat((self.AssociatedCenterArray.count ) * 80))
                                
                    }else {
                        self.tblViewHeight.constant = 0
                    }
                }else {
               self.tblViewHeight.constant = 0
                }
                
                
               self.collView.reloadData()
                self.tblView.reloadData()
                
                } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    @IBAction func btnProfileClicked(_ sender: Any) {
        let actionSheet = UIAlertController(title: "Select Source", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.dismiss(animated: true, completion: nil)
            
        }
        let camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            // kUTTypeImage
            self.imagePicker.sourceType = .camera;
            self.imagePicker.mediaTypes =  [kUTTypeImage as String]
            self.imagePicker.allowsEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let library = UIAlertAction(title: "Library", style: UIAlertActionStyle.default) { (alertAction) -> Void in
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes =  [kUTTypeImage as String]
            
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
        {
            actionSheet.addAction(camera)
            actionSheet.addAction(library)
            actionSheet.addAction(cancel)
        }
        else{
            actionSheet.addAction(library)
            actionSheet.addAction(cancel)
        }
        imagePicker.delegate = self
        present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSelectImage(_ sender: Any) {
        let controller:ImagePopViewController = ImagePopViewController(nibName: "ImagePopViewController", bundle: nil)
        controller.imageurl = dataDic.object(forKey: "photoPath") as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        isImageChange = true
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
           // imageView.image = image
            dismiss(animated: false) { [unowned self] in
                self.openEditor(image: image)
            }
            
        } else{
            print("Something went wrong")
        }
       // self.dismiss(animated: true, completion: nil)
    }
    

    
    // MARK: - Actions

    @IBAction func btnCancelClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        self.tabBarController?.tabBar.isHidden = false
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    @IBAction func btnEmailClicked(_ sender: Any) {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.delegate = self
        mailVC.setToRecipients([(self.dataDic.value(forKey: "emailID") as! String)])
        mailVC.setSubject("Zam")
        mailVC.setMessageBody("Mail text", isHTML: false)
        if (MFMailComposeViewController.canSendMail()) {
            present(mailVC, animated: true, completion: nil)
        }
    }
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail sent")
        case MFMailComposeResult.failed.rawValue:
            print("Mail sent failure: %@", [error?.localizedDescription])
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func btnCallClicked(_ sender: Any) {
        
        let no = self.dataDic.value(forKey: "mobileNo") as! String
        if let url = NSURL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    @IBAction func btnEditClicked(_ sender: Any) {
        let vc = EditProfileViewController(nibName: "EditProfileViewController", bundle: nil)
        vc.isTrainer = self.dataDic.value(forKey: "isTrainer") as! Bool
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    // MARK: - TableViewDelegate & DataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if AssociatedCenterArray.count > 0 {
        return AssociatedCenterArray.count + 1
        }else{
         return 0
        }
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
        return 30
        }else{
         return 80
        }
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:ProfileAssociatedCentresTableCell = tableView.dequeueReusableCell(withIdentifier: "ProfileAssociatedCentresTableCell") as! ProfileAssociatedCentresTableCell
        let cellHeader:WorkingHoursHeaderTableCell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursHeaderTableCell") as! WorkingHoursHeaderTableCell
        if indexPath.row == 0 {
            cellHeader.lblTitle.text = "Associated Centres"
            return cellHeader
        }else {
            
         cell.lblType.text = (AssociatedCenterArray[indexPath.row - 1] as AnyObject).value(forKey: "associateType") as? String
         cell.lblName.text = (AssociatedCenterArray[indexPath.row - 1] as AnyObject).value(forKey: "centreName") as? String
         ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImgView, radius: Int(cell.btnImgView.frame.size.height / 2),borderWidth: 2)
       let imageurl = (AssociatedCenterArray[indexPath.row - 1] as AnyObject).value(forKey: "logoPath") as! String
            cell.btnImgView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let centreID = Int((AssociatedCenterArray[indexPath.row - 1] as AnyObject).value(forKey: "centreID") as! NSNumber)
        getDetail(centerId: String(describing: centreID))
    }
    

    // MARK: - CollectionView Delegate & DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return AssociatedExamArray.count
        
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CenterimagesCollectionCell", for: indexPath as IndexPath) as! CenterimagesCollectionCell
        let imageurl = (AssociatedExamArray[indexPath.row] as AnyObject).value(forKey: "examLogo") as! String
        ZamModel().CollCellCorner(collcell: cell, radius: 5)
        cell.btnImageView.tag = indexPath.row
        cell.btnImageView.addTarget(self, action: #selector(ProfileViewController.btnImageViewClicked), for: .touchUpInside)
        cell.btnImageView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize(width:collectionView.frame.size.width / 2 - 10, height:60)
            
    }
    
    @IBAction func btnViewMoreClicked(_ sender: Any) {
         self.lblAboutUs.numberOfLines = 0
        btnContinueReading.isHidden = true
    }
    
    
    func getDetail(centerId :String){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : centerId]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCenterDetail(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
                vc.CentreID = centerId
                vc.dataDic = response?.object(forKey: "data") as! NSDictionary
                //  print(response?.object(forKey: "data") as! NSDictionary)
                self.navigationController?.pushViewController(vc, animated: false)
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
       func btnImageViewClicked (sender :UIButton){
        let data = AssociatedExamArray[sender.tag]
        print(data)
        let examID = (AssociatedExamArray[sender.tag] as AnyObject).value(forKey: "examID") as! NSNumber
        let examName = (AssociatedExamArray[sender.tag] as AnyObject).value(forKey: "examName") as! String
      //  getAssociatedExamList(examId: Int(examID),name: examName)
    }
    func getAssociatedExamList(examId :Int,name :String){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : "3" ,"ExamID":examId] as [String : Any]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCenterCoursesList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                let data = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                print(data)
                let vc = SupportedExamsViewController(nibName: "SupportedExamsViewController", bundle: nil)
                vc.dataArray = data
                vc.nameTitle = name
                vc.rateValue = String(describing: self.dataDic.object(forKey: "ratevalue") as! NSNumber)
                
                self.navigationController?.pushViewController(vc, animated: false)
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }

    @IBAction func btnLocationClicked(_ sender: Any) {
        
       // getLatLongFromAddress(address: lblAddress.text!)
        
    }
    
    
//    func getLatLongFromAddress(address: String) {
//        CLGeocoder().geocodeAddressString(address, completionHandler: { (placemarks, error) in
//            if error != nil {
//                print(error)
//                return
//            }
//            if (placemarks?.count)! > 0 {
//                let placemark = placemarks?[0]
//                let location = placemark?.location
//                let coordinate = location?.coordinate
//                let url = "http://maps.google.com/maps?saddr=\(self.appDelegate.userLocation.coordinate.latitude),\(self.appDelegate.userLocation.coordinate.longitude)&daddr=\(coordinate!.latitude),\(coordinate!.longitude)"
//                UIApplication.shared.openURL(URL(string: url)!)
//                
//                print("\nlat: \(coordinate!.latitude), long: \(coordinate!.longitude)")
////                if (placemark?.areasOfInterest?.count)! > 0 {
////                    let areaOfInterest = placemark!.areasOfInterest![0]
////                    print(areaOfInterest)
////                } else {
////                    print("No area of interest found.")
////                }
//            }
//        })
//    }

    // MARK: - CropView
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        //        controller.dismissViewControllerAnimated(true, completion: nil)
        //        imageView.image = image
        //        updateEditButtonEnabled()
    }
    
    func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        
        let CompressedImage = ZamModel().resizeImage(image: image, newWidth: 200)
        btnProfilePic.setBackgroundImage(CompressedImage, for: .normal)
        
        let imageData = UIImagePNGRepresentation(CompressedImage)
        let base64String =  imageData?.base64EncodedString(options:.lineLength64Characters)
        
        
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID" : UserDefaults.standard.object(forKey: "userId") as! String, "UserPhoto":base64String] as [String : Any]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        
        AppEngine().uploaduserProfile(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.getdata()
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
        controller.dismiss(animated: true, completion: nil)
       // imageView.image = image
       // updateEditButtonEnabled()
    }
    
    func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
      //  updateEditButtonEnabled()
    }
    func openEditor(image:UIImage) {
       
        // Uncomment to use crop view directly
        //        let imgView = UIImageView(image: image)
        //        imgView.clipsToBounds = true
        //        imgView.contentMode = .ScaleAspectFit
        //
        //        let cropView = CropView(frame: imageView.frame)
        //
        //        cropView.opaque = false
        //        cropView.clipsToBounds = true
        //        cropView.backgroundColor = UIColor.clearColor()
        //        cropView.imageView = imgView
        //        cropView.showCroppedArea = true
        //        cropView.cropAspectRatio = 1.0
        //        cropView.keepAspectRatio = true
        //
        //        view.insertSubview(cropView, aboveSubview: imageView)
        
        // Use view controller
        let controller = CropViewController()
        controller.delegate = self
        controller.image = image
        
        let navController = UINavigationController(rootViewController: controller)
        present(navController, animated: true, completion: nil)
    }
    
}
