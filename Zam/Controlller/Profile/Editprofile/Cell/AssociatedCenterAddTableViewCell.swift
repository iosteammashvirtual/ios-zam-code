//
//  AssociatedCenterAddTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/19/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class AssociatedCenterAddTableViewCell: UITableViewCell {

    @IBOutlet weak var imgBorder: UIImageView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var btnOpenPicker: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
