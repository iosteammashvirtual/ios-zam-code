//
//  EditProfileViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/5/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class EditProfileViewController: UIViewController,UITextFieldDelegate,UIPopoverPresentationControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextViewDelegate,UIScrollViewDelegate{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtViewHeight: NSLayoutConstraint!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var tblViewAssociatedheight: NSLayoutConstraint!
    @IBOutlet weak var tblSearchBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblViewAssociated: UITableView!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtAddress2: UITextField!
    @IBOutlet weak var txtAddress1: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtPhoneno: UITextField!
    @IBOutlet weak var lblRole1: UILabel!
    @IBOutlet weak var btnRole1: UIButton!
    @IBOutlet weak var btnRole2: UIButton!
    @IBOutlet weak var lblRole2: UILabel!
    @IBOutlet weak var lblRole3: UILabel!
    @IBOutlet weak var btnRole3: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var btnProfileBackGroumd: UIButton!
    var firstTime = true
    var message = String()
    var iserror :Bool = false
    var ismobileNoChange = "false"
    
    var selectedStateid = String()
    var selectedCityId = String()
    
    
    var selectedTag = 0
    var ListArray = NSMutableArray()
    var MainStateArray = NSMutableArray()
    var MainCityList = NSMutableArray()
    
    var dataDic = NSDictionary()
    
    var IntListdataArray = NSArray()
    var selectedListArray = NSMutableArray()
    var interestExamList = NSMutableArray()
    var SelectedinterestExamList = NSMutableArray()
    
    var MainUnSelectedinterestExamList = NSMutableArray()
     var UnSelectedinterestExamList = NSMutableArray()
    var Alertdelegate = AlertConfirmViewController()
    var associatedCentreArray = NSMutableArray()
    var associatedCentreArrayKeyword = NSMutableArray()
    
    @IBOutlet weak var pickerView: UIView!
    var pickerData = [String]();
    
    var associationTypeArray = NSMutableArray()
    var txtAssociatedExam = ""
    var txtAssociatedCenter = ""
     var Centerid = ""
    var pickerSelectedValue = ""
    
    var isTrainer = Bool()
    var ispickerDoneClicked:Bool = false
    
        override func viewDidLoad() {
        super.viewDidLoad()
            
            picker.delegate = self
            picker.dataSource = self
          // self.automaticallyAdjustsScrollViewInsets = true;
        ZamModel().buttonCorner(Button: btnDone, radius: Int(10.0))
            ZamModel().buttonCornerWithThemeBorder(Button: btnProfileBackGroumd, radius:Int(btnProfileBackGroumd.frame.size.width/2.0),borderWidth: 6)
            ZamModel().buttonCorner(Button: btnProfilePic, radius: Int(btnProfilePic.frame.size.width/2.0))
            
            NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
            //////
            NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.PopToProfile(notification:)), name: Notification.Name("PopToProfile"), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(EditProfileViewController.ComeFromEditProfile(notification:)), name: Notification.Name("ComeFromOtpPoPUp"), object: nil)
          ZamModel().textViewCorner(txtview: txtView, radius: 5)
            let nibName = UINib(nibName: "InterestExamTableViewCell", bundle:nil)
            tblView.register(nibName, forCellReuseIdentifier: "InterestExamTableViewCell")
           let nibName1 = UINib(nibName: "EditProfileAssociatedTableCell", bundle:nil)
          tblViewAssociated.register(nibName1, forCellReuseIdentifier: "EditProfileAssociatedTableCell")
            let nibName2 = UINib(nibName: "AssociatedExamAddTableCell", bundle:nil)
            tblViewAssociated.register(nibName2, forCellReuseIdentifier: "AssociatedExamAddTableCell")
            let nibName3 = UINib(nibName: "WorkingHoursHeaderTableCell", bundle:nil)
            tblViewAssociated.register(nibName3, forCellReuseIdentifier: "WorkingHoursHeaderTableCell")
            let nibName4 = UINib(nibName: "AssociatedCenterAddTableViewCell", bundle:nil)
            tblViewAssociated.register(nibName4, forCellReuseIdentifier: "AssociatedCenterAddTableViewCell")
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
            
            
            
            self.tblView.layer.shadowOffset = CGSize(width:0, height:5)
            self.tblView.layer.shadowColor = UIColor.black.cgColor
            self.tblView.layer.shadowRadius = 2
            self.tblView.layer.masksToBounds = false
            self.tblView.layer.shadowOpacity = 0.4
        
            
            
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        getdata()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnRole1Clicked(_ sender: Any) {
       // getdata()
        if btnRole1.currentImage!.isEqual(UIImage(named: "selected.png"))
        {
            btnRole1.setImage(UIImage(named: "Unselected.png"), for: .normal)
            selectedListArray.remove((self.IntListdataArray.object(at: 0) as AnyObject).value(forKey: "roleName") as! String)
        }else {
            btnRole1.setImage(UIImage(named: "selected.png"), for: .normal)
            selectedListArray.add((self.IntListdataArray.object(at: 0) as AnyObject).value(forKey: "roleName") as! String)
            updateUserRole(isCenter: "true")
//            var strType = String()
//            for i in  0..<self.selectedListArray.count{
//                strType = strType + (selectedListArray[i] as! String) + ","
//            }
//            strType = String(strType.characters.dropLast())
//            UserDefaults.standard.set(strType, forKey: "role")
//            UserDefaults.standard.synchronize()
        }
    }
    @IBAction func btnRole2Clicked(_ sender: Any) {
       // getdata()
        if btnRole2.currentImage!.isEqual(UIImage(named: "selected.png"))
        {
            btnRole2.setImage(UIImage(named: "Unselected.png"), for: .normal)
            selectedListArray.remove((self.IntListdataArray.object(at: 1) as AnyObject).value(forKey: "roleName") as! String)
        }else {
            btnRole2.setImage(UIImage(named: "selected.png"), for: .normal)
            selectedListArray.add((self.IntListdataArray.object(at: 1) as AnyObject).value(forKey: "roleName") as! String)
        }
    }
    @IBAction func btnRole3Clicked(_ sender: Any) {
       
        if btnRole3.currentImage!.isEqual(UIImage(named: "selected.png"))
        { isTrainer = false
            btnRole3.setImage(UIImage(named: "Unselected.png"), for: .normal)
            selectedListArray.remove((self.IntListdataArray.object(at: 2) as AnyObject).value(forKey: "roleName") as! String)
            self.tblViewAssociatedheight.constant = 0
            // updateUserRole(isCenter: "false")
           
        }else {
             isTrainer = true
            btnRole3.setImage(UIImage(named: "selected.png"), for: .normal)
            selectedListArray.add((self.IntListdataArray.object(at: 2) as AnyObject).value(forKey: "roleName") as! String)
            
            getDtaWhenRoleTrainer()
           
            // updateUserRole(isCenter: "false")
           
        }
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        
        let controller: AlertConfirmViewController =  AlertConfirmViewController(nibName: "AlertConfirmViewController", bundle: nil)
        controller.message = confirmSaveChange
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
       
        
       // self.navigationController?.popViewController(animated: true)
    }
    
    func YesClicked(){
    self.navigationController?.popViewController(animated: true)
    }
    
    func getdata(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID" : UserDefaults.standard.object(forKey: "userId") as! String] as [String : Any]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getUserDetails(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           
            if response == nil{
                self.view.hideToastActivity(view: self.view)
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
              debugPrint(response)
                self.dataDic = response?.object(forKey: "data") as! NSDictionary
                let imageurl = self.dataDic.value(forKey: "photoPath") as! String
                self.btnProfilePic.sd_setBackgroundImage(with: NSURL(string: imageurl) as URL!, for: .normal)
                self.txtPostalCode.text = self.dataDic.value(forKey: "postCode") as? String
                self.txtFirstName.text = self.dataDic.value(forKey: "firstName") as? String
                self.txtLastName.text = self.dataDic.value(forKey: "lastName") as? String
                self.txtCity.text = self.dataDic.value(forKey: "city") as? String
                self.txtEmail.text = self.dataDic.value(forKey: "emailID") as? String
                self.txtState.text = self.dataDic.value(forKey: "state") as? String
                self.txtPhoneno.text = self.dataDic.value(forKey: "mobileNo") as? String
                self.txtAddress1.text = self.dataDic.value(forKey: "address1") as? String
                self.txtAddress2.text = self.dataDic.value(forKey: "address2") as? String
                let roleList = self.dataDic.value(forKey: "userRoles") as! String
                let array = roleList.components(separatedBy: ",")
                if self.firstTime == true{
                    self.firstTime = false
                self.selectedListArray = (array as NSArray).mutableCopy()
                    as! NSMutableArray
                }
                
               // self.ListArray = (self.dataDic.object(forKey: "cityList") as? NSArray)?.mutableCopy() as! NSMutableArray
                if self.dataDic.value(forKey: "profileDesc") as? String != ""{
                 self.txtView.text = self.dataDic.value(forKey: "profileDesc") as? String
                    
               let nooflines = ZamModel().getNoOfLinesTextView(label: self.txtView)
                self.txtViewHeight.constant = CGFloat(nooflines) * 20
                }else {
                 self.txtViewHeight.constant =  40
                 self.txtView.text = "About me"
                }
                self.MainStateArray = (self.dataDic.object(forKey: "stateList") as? NSArray)?.mutableCopy() as! NSMutableArray
                 self.MainCityList = (self.dataDic.object(forKey: "cityList") as? NSArray)?.mutableCopy() as! NSMutableArray
                
                if self.isTrainer == false {
                    self.tblViewAssociatedheight.constant = 0
                }else {
                    if (self.dataDic.value(forKey: "interestExams") is NSNull) && (self.dataDic.value(forKey: "interestExams")) != nil {
                   self.getDtaWhenRoleTrainer()
                    }
                    else {
                        if self.dataDic.value(forKey: "interestExams") != nil  && !(self.dataDic.value(forKey: "interestExams") is NSNull) {
                            self.interestExamList = (self.dataDic.object(forKey: "interestExams") as? NSArray)?.mutableCopy() as! NSMutableArray
                        }
                        self.associationTypeArray = NSMutableArray()
                        if self.dataDic.value(forKey: "associationType") != nil  && !(self.dataDic.value(forKey: "associationType") is NSNull) {
                            self.associationTypeArray = (self.dataDic.object(forKey: "associationType") as? NSArray)?.mutableCopy() as! NSMutableArray
                        }
                        self.pickerData.removeAll()
                        for i in 0..<self.associationTypeArray.count{
                            self.pickerData.append(((self.associationTypeArray[i] as AnyObject).value(forKey: "associateName") as! String))
                        }
                        self.picker.reloadAllComponents()
                        self.SelectedinterestExamList = NSMutableArray()
                        self.MainUnSelectedinterestExamList = NSMutableArray()
                        self.UnSelectedinterestExamList = NSMutableArray()
                        for i in 0..<self.interestExamList.count{
                            if (self.interestExamList[i] as AnyObject).value(forKey: "isOpted") as? Bool == true {
                                self.SelectedinterestExamList.add((self.interestExamList[i] as AnyObject).value(forKey: "examName") as! String)
                            }else {
                                self.MainUnSelectedinterestExamList.add((self.interestExamList[i] as AnyObject).value(forKey: "examName") as! String)
                                self.UnSelectedinterestExamList.add((self.interestExamList[i] as AnyObject).value(forKey: "examName") as! String)
                            }
                        }
                        if self.dataDic.value(forKey: "trainerAssoCentres") != nil  && !(self.dataDic.value(forKey: "trainerAssoCentres") is NSNull) {
                            self.associatedCentreArray = (self.dataDic.object(forKey: "trainerAssoCentres") as? NSArray)?.mutableCopy() as! NSMutableArray
                        }
                        self.tblViewAssociatedheight.constant = CGFloat((self.SelectedinterestExamList.count * 50) + 100)  +  CGFloat((self.associatedCentreArray.count * 50) + 160)
                       // self.tblViewAssociated.reloadData()
                    }
                  
                }
                
                let predicate = NSPredicate(format: "stateName == '\(self.txtState.text!)'")
                var list = self.MainStateArray.filtered(using: predicate)
                if list.count>0{
                    let stateid = (list[0] as AnyObject).value(forKey: "stateID") as! NSNumber
                    self.selectedStateid = String(describing: stateid)
                }
                
                self.callApiForRole()
                 self.tblViewAssociated.reloadData()
                     } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnDoneClicked(_ sender: Any) {
        if ZamModel().isConnectedToNetwork() == false {
            iserror = true
            message = networkError
        }else if txtFirstName.text == ""{
            iserror = true
            message = firstNamenil
            
        }else if txtPhoneno.text == ""{
            iserror = true
            message = MobilenoNil
        }
        else if ZamModel().regexPhoneNotCorrect(phoneNo: txtPhoneno.text!) == false
        {
            iserror = true
            message = MobilenoNotValid
        }else if txtEmail.text == "" {
            iserror = true
            message = emailnil
        } else if ZamModel().validateEmail(txtEmail: txtEmail.text!) == false
        {
            iserror = true
            message = emailnotvalid
        }
        
       if txtPhoneno.text != self.dataDic.value(forKey: "mobileNo") as? String{
        ismobileNoChange = "true"
       }
       else {
         ismobileNoChange = "false"
        }
        if iserror == true{
            iserror = false
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        if ismobileNoChange == "true"{
        
            self.view.makeToastActivity(message: "Loading", view: self.view)
            
            let parameters = [ "MobileNo" :txtPhoneno.text!]
            
            AppEngine().Register(parameter: parameters as NSDictionary,url: generateOtp)  { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true{
                    let controller:OTPPoPUpViewController = OTPPoPUpViewController(nibName: "OTPPoPUpViewController", bundle: nil)
                    debugPrint(response?.object(forKey: "otp") as! String)
                    controller.OtpNumber = response?.object(forKey: "otp") as! String
                    controller.mobileNo = self.txtPhoneno.text!
                    controller.comeFromEditProfile = true
                    self.addChildViewController(controller)
                    controller.view.frame = self.view.bounds
                    self.view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                    
                } else {
                    let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                    controller.message = response?.object(forKey: "message") as! String
                    self.addChildViewController(controller)
                    controller.view.frame = self.view.bounds
                    self.view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
            
        }else {
        updateProfile()
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 100{
            selectedTag = 100
            tblView.reloadData()
        }else if textField.tag == 200{
            selectedTag = 200
            tblView.reloadData()
        }else if textField.tag == 300{
            selectedTag = 300
            tblView.reloadData()
        }
        else if textField.tag == 400{
            selectedTag = 400
            tblView.reloadData()
        }
    }
        func keyboardWillShow(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
//        var contentInset:UIEdgeInsets = self.scrollView.contentInset
//        contentInset.bottom = keyboardFrame.size.height + 200
        tblSearchBottomConstraint.constant =  keyboardFrame.size.height + 50
    }
    func keyboardWillHide(notification: NSNotification) {
        tblView.isHidden = true
//        let contentInset:UIEdgeInsets = .zero
//        self.scrollView.contentInset = contentInset
    }
    // MARK: - TableViewDelegate
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView.tag == 500{
         return 2
        }else {
         return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView.tag == 500
        {
            if section == 0{
            return SelectedinterestExamList.count + 2
            }else {
            return associatedCentreArray.count + 2
            }
        }else {
            if selectedTag == 300 {
             return UnSelectedinterestExamList.count
            }else if selectedTag == 400{
                 return associatedCentreArrayKeyword.count
            }else{
             return ListArray.count
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 500{
            
            if indexPath.section == 0{
                if indexPath.row == 0{
                    return 50
                }else {
                    return 50
                }
            
            }else {
                
                if indexPath.row == 0{
                    return 50
                }
                else if indexPath.row == associatedCentreArray.count + 1{
                    return 110
                }
                else {
               return 50
                }
                }
            
           
        }
        return 50
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        if tableView.tag == 500 {
            if indexPath.section == 0 {
                if indexPath.row == 0 {
                    let cell:WorkingHoursHeaderTableCell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursHeaderTableCell") as! WorkingHoursHeaderTableCell
    
                    cell.lblTitle.text = "Associated Exams"
                    
                    return cell
                }else {
                    if indexPath.row == SelectedinterestExamList.count + 1{
                        let cell:AssociatedExamAddTableCell = tableView.dequeueReusableCell(withIdentifier: "AssociatedExamAddTableCell") as! AssociatedExamAddTableCell
                        ZamModel().buttonCorner(Button: cell.btnAdd, radius: Int(5.0))
                        ZamModel().imageCorner(img: cell.imgBorder, radius: Int(5.0))
                        cell.txtSearch.text = txtAssociatedExam
                         cell.btnAdd.addTarget(self, action: #selector(EditProfileViewController.AddAssociatedExamClicked), for: .touchUpInside)
                        cell.txtSearch.delegate = self
//                        cell.layer.borderColor = UIColor.lightGray.cgColor
//                        cell.layer.borderWidth = 1.0
//                        cell.layer.cornerRadius = 5.0
                        return cell
                    }else {
                        let cell:EditProfileAssociatedTableCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileAssociatedTableCell") as! EditProfileAssociatedTableCell
                        cell.lblName.text = SelectedinterestExamList[indexPath.row - 1] as! String
                         cell.btnRemove.tag = indexPath.row - 1
                         cell.btnRemove.addTarget(self, action: #selector(EditProfileViewController.RemoveAssociatedExamClicked), for: .touchUpInside)
                        ZamModel().buttonCornerWithThemeBorder(Button: cell.btnRemove, radius: 5, borderWidth: 2)
                        cell.layer.borderColor = UIColor.lightGray.cgColor
                        cell.layer.borderWidth = 1.0
                        cell.layer.cornerRadius = 5.0
                        return cell
                    }
                }
            
            }else {
                if indexPath.row == 0 {
                    let cell:WorkingHoursHeaderTableCell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursHeaderTableCell") as! WorkingHoursHeaderTableCell
                    cell.lblTitle.text = "Associated Centre"
                    
                    return cell
                }else {
                    if indexPath.row == associatedCentreArray.count + 1{
                        let cell:AssociatedCenterAddTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AssociatedCenterAddTableViewCell") as! AssociatedCenterAddTableViewCell
                        ZamModel().buttonCorner(Button: cell.btnAdd, radius: Int(5.0))
                        ZamModel().imageCorner(img: cell.imgBorder, radius: Int(5))
                        ZamModel().buttonCornerWithThemeBorder(Button: cell.btnOpenPicker,radius: 5, borderWidth: 2)
                        
                        if  ispickerDoneClicked == true{
                          cell.btnOpenPicker.setTitle(pickerSelectedValue, for: .normal)
//                        cell.btnOpenPicker.isHidden = false
//                            cell.btnAdd.isHidden = false
                        }else {
//                        cell.btnOpenPicker.isHidden = true
//                            cell.btnOpenPicker.isHidden = true
                        cell.btnOpenPicker.setTitle("Select Job Type", for: .normal)
                        }
                        cell.btnOpenPicker.addTarget(self, action: #selector(EditProfileViewController.openPickerClicked), for: .touchUpInside)
                        
                         cell.btnAdd.addTarget(self, action: #selector(EditProfileViewController.AddAssociatedCenterClicked), for: .touchUpInside)
                        
                        cell.txtSearch.delegate = self
                        cell.txtSearch.text = txtAssociatedCenter
                        
                        if  txtAssociatedCenter == ""{
                           // cell.btnOpenPicker.setTitle(pickerSelectedValue, for: .normal)
                            cell.btnOpenPicker.isHidden = true
                            cell.btnAdd.isHidden = true
                        }else {
                            cell.btnOpenPicker.isHidden = false
                            cell.btnAdd.isHidden = false
                          //  cell.btnOpenPicker.setTitle("Select Job Type", for: .normal)
                        }
                        
                        
                        
//                        cell.layer.borderColor = UIColor.lightGray.cgColor
//                        cell.layer.borderWidth = 1.0
//                        cell.layer.cornerRadius = 5.0
                        return cell
                    }else {
                        let cell:EditProfileAssociatedTableCell = tableView.dequeueReusableCell(withIdentifier: "EditProfileAssociatedTableCell") as! EditProfileAssociatedTableCell
                        cell.lblName.text = (associatedCentreArray[indexPath.row - 1] as AnyObject).value(forKey: "centreName") as! String
                        ZamModel().buttonCornerWithThemeBorder(Button: cell.btnRemove, radius: 5, borderWidth: 2)
                         cell.btnRemove.tag = indexPath.row - 1
                         cell.btnRemove.addTarget(self, action: #selector(EditProfileViewController.RemoveAssociatedCenterClicked), for: .touchUpInside)
                        
                        cell.layer.borderColor = UIColor.lightGray.cgColor
                        cell.layer.borderWidth = 1.0
                        cell.layer.cornerRadius = 5.0
                        return cell
                    }
                }
            }
            
        }else {
            let cell:InterestExamTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InterestExamTableViewCell") as! InterestExamTableViewCell
            if selectedTag == 300{
                cell.lblName.text = UnSelectedinterestExamList[indexPath.row] as? String
                return cell
            }else if selectedTag == 400{
           cell.lblName.text =  (associatedCentreArrayKeyword[indexPath.row] as AnyObject).value(forKey: "centreName") as? String
                 return cell
            }
            else {
                cell.lblName.text = ListArray[indexPath.row] as? String
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 500{
        }else {
            tblView.isHidden = true
           
            if selectedTag == 100 {
                 let value =  ListArray[indexPath.row] as! String
                txtState.text = value
                let predicate = NSPredicate(format: "stateName == '\(value)'")
                var list = MainStateArray.filtered(using: predicate)
                if list.count>0{
                    var stateid = (list[0] as AnyObject).value(forKey: "stateID") as! NSNumber
                    getCityList(stateId: String(describing: stateid))
                    selectedStateid = String(describing: stateid)
                }
                print(list)
                
            }else if selectedTag == 200 {
                 let value =  ListArray[indexPath.row] as! String
                txtCity.text = value
            }else if selectedTag == 300{
               txtAssociatedExam = UnSelectedinterestExamList[indexPath.row] as! String
               tblViewAssociated.reloadData()
            }else if selectedTag == 400{
                
             
                
              let value = ((associatedCentreArrayKeyword[indexPath.row] as AnyObject).value(forKey: "centreName") as? String)!
                
                let predicate = NSPredicate(format: "centreName == '\(value)'")
                var list = self.associatedCentreArray.filtered(using: predicate)
                
                if list.count > 0{
                /////
                    let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                    controller.message = centerAlreadyAdded
                    addChildViewController(controller)
                    controller.view.frame = view.bounds
                    view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
//                    txtAssociatedCenter = ""
                    tblView.reloadData()
                
                }else {
                    
                    
                    txtAssociatedCenter = ((associatedCentreArrayKeyword[indexPath.row] as AnyObject).value(forKey: "centreName") as? String)!
                    Centerid = String(describing: ((associatedCentreArrayKeyword[indexPath.row] as AnyObject).value(forKey: "centreID") as? NSNumber)!)
                     tblViewAssociated.reloadData()
                }
            }
            
        }
         }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        tblView.isHidden = false
        if selectedTag == 100 {
              let data = MainStateArray.value(forKey: "stateName")
            let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", textField.text! + string)
            let array = (data as! NSArray).filtered(using: searchPredicate)
            ListArray = (array as! NSMutableArray)
            
            tblView.reloadData()
        }else if selectedTag == 200{
            
            let data = MainCityList.value(forKey: "cityName")
            let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", textField.text! + string)
            let array = (data as! NSArray).filtered(using: searchPredicate)
            ListArray = (array as! NSMutableArray)
            tblView.reloadData()
        }else if selectedTag == 300 {
            let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", textField.text! + string)
            let array = MainUnSelectedinterestExamList.filtered(using: searchPredicate)
           
            UnSelectedinterestExamList = array as! NSMutableArray
            if UnSelectedinterestExamList.count > 0{
            self.tblView.isHidden = false
            }else {
             self.tblView.isHidden = true
            }
            tblView.reloadData()
        }
        else if selectedTag == 400{
           // DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
             GetSearchCentreAssociateByKeyword(txtValue: textField.text! + string)
           // }
        }  else if textField == txtPostalCode{
             tblView.isHidden = true
                guard let text = textField.text else { return true }
                let newLength = text.characters.count + string.characters.count - range.length
                if newLength <= 8{
                    return true
                }else {
                    return false
                }
                
            }

        return true
    }
    func getCityList(stateId :String){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["stateID": stateId]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCityList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
               self.MainCityList = (response?.object(forKey: "data") as? NSArray)?.mutableCopy() as! NSMutableArray
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    // Delegate
    func PopToProfile(notification: Notification) {
       self.navigationController?.popViewController(animated: true)
    }
    func ComeFromEditProfile(notification: Notification) {
        updateProfile()
    }

    
    //////////////////////// Selectors
    func AddAssociatedCenterClicked(){
        if ispickerDoneClicked == true {
            if  self.txtAssociatedCenter == ""{
            
            }else {
                ispickerDoneClicked = false
                self.txtAssociatedCenter = ""
                AddAssociatedCenterApiCall()
            }
        }else{
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
          // self.txtAssociatedCenter = ""
            controller.message = selectJobFirst
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
        }
    }
    
    func RemoveAssociatedCenterClicked(sender:UIButton){
//        if pickerSelectedValue != "" {
            let data =  associatedCentreArray[sender.tag]
            print(data)
            let centerID = (data as AnyObject).value(forKey: "trainerCentreID") as! NSNumber
            RemoveAssociatedCenterApiCall(centerID: String(describing: centerID),selectedIndex: sender.tag)
       
     
//        }
    }
    

    func AddAssociatedCenterApiCall(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        let predicate = NSPredicate(format: "associateName == '\(pickerSelectedValue)'")
        var list = self.associationTypeArray.filtered(using: predicate)
        var otypeId = ""
        if list.count>0{
         otypeId = String(describing: ((list[0] as AnyObject).value(forKey: "associateID") as? NSNumber)!)
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String, "CentreID":Centerid ,"OTypeID": otypeId]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().addAssociatedCentre(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                
             self.associatedCentreArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.tblViewAssociatedheight.constant = CGFloat((self.SelectedinterestExamList.count * 50) + 100)  +  CGFloat((self.associatedCentreArray.count * 50) + 160)
                self.tblViewAssociated.reloadData()
               // self.getdata()
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    func RemoveAssociatedCenterApiCall(centerID :String,selectedIndex :Int){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
//        let predicate = NSPredicate(format: "associateName == '\(pickerSelectedValue)'")
//        var list = self.associationTypeArray.filtered(using: predicate)
//        var otypeId = ""
//        if list.count>0{
//            otypeId = String(describing: ((list[0] as AnyObject).value(forKey: "associateID") as? NSNumber))
//        }
        
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerCentreCentreID": centerID]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().removeAssociatedCentre(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           
            if response == nil{
                self.view.makeToast(message: ServerError)
                 self.view.hideToastActivity(view: self.view)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
           self.associatedCentreArray.removeObject(at: selectedIndex)
          self.tblViewAssociatedheight.constant = CGFloat((self.SelectedinterestExamList.count * 50) + 100)  +  CGFloat((self.associatedCentreArray.count * 50) + 160)
            self.tblViewAssociated.reloadData()
                 self.view.hideToastActivity(view: self.view)
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
                 self.view.hideToastActivity(view: self.view)
            }
        }
    }
    func AddAssociatedExamClicked(sender:UIButton){
        if txtAssociatedExam != ""{
       SelectedinterestExamList.add(txtAssociatedExam)
       MainUnSelectedinterestExamList.remove(txtAssociatedExam)
        txtAssociatedExam = ""
       tblViewAssociated.reloadData()
self.tblViewAssociatedheight.constant = CGFloat((self.SelectedinterestExamList.count * 50) + 100)  +  CGFloat((self.associatedCentreArray.count * 50) + 160)
      }
    
    }
    func RemoveAssociatedExamClicked(sender:UIButton){
        MainUnSelectedinterestExamList.add(SelectedinterestExamList[sender.tag])
        SelectedinterestExamList.remove(SelectedinterestExamList[sender.tag])
        tblViewAssociated.reloadData()
        self.tblViewAssociatedheight.constant = CGFloat((self.SelectedinterestExamList.count * 50) + 100)  +  CGFloat((self.associatedCentreArray.count * 50) + 160)
    }
    func GetSearchCentreAssociateByKeyword(txtValue : String){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["keyword": txtValue]
      //  self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetSearchCentreAssociateByKeyword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
          //  self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
//           print(response?.object(forKey: "data") as? NSArray)
                self.associatedCentreArrayKeyword = (response?.object(forKey: "data") as? NSArray)?.mutableCopy() as! NSMutableArray
                if self.associatedCentreArrayKeyword.count > 0 {
                 self.tblView.isHidden = false
                 self.tblView.reloadData()
                }else {
                self.tblView.isHidden = true
                }
                
               
            } else {
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
 // MARK: - PickerViewDelegate,DataSource  & Functions
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    // Delegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        pickerSelectedValue = pickerData[row]
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        print(pickerView.selectedRow(inComponent: component))
    }
    
    func openPickerClicked(){
    openPicker()
    }
    
    @IBAction func btnCancelPicker(_ sender: Any) {
        closepicker()
    }
    @IBAction func btnDonePicker(_ sender: Any) {
        ispickerDoneClicked = true
        tblViewAssociated.reloadData()
        closepicker()
    }
    
    func openPicker(){
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
            self.pickerView.frame.origin.y = self.view.frame.height-250
        }, completion: nil)
    }
   func closepicker(){
    
    UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
    self.pickerView.frame.origin.y = self.view.frame.height+50
    }, completion: nil)
    }
    func updateProfile(){
        let selectedExamIdList = NSMutableArray()
        for i in 0..<self.SelectedinterestExamList.count{
            let predicate = NSPredicate(format: "examName == '\(SelectedinterestExamList[i])'")
            var list = self.interestExamList.filtered(using: predicate)
            if list.count>0{
                selectedExamIdList.add((list[0] as AnyObject).value(forKey: "examID") as! NSNumber)
            }
        }
        if isTrainer{
            
            if selectedExamIdList.count < 1{
                let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                controller.message = AssociatedExamNil
                addChildViewController(controller)
                controller.view.frame = view.bounds
                view.addSubview(controller.view)
                controller.didMove(toParentViewController: self)
                return
            }
            
        }
        
        if selectedListArray.count == 0 {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = InterestNil
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
        return
        }
        
        if txtView.text == "About me"{
        txtView.text = ""
        }
        
        var strType = String()
        for i in  0..<self.selectedListArray.count{
        strType = strType + (selectedListArray[i] as! String) + ","
        }
        strType = String(strType.characters.dropLast())
        
       UserDefaults.standard.set(strType, forKey: "role")
        UserDefaults.standard.synchronize()
        
         var parameters = NSDictionary()
        if selectedExamIdList.count > 0{
         parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID" : UserDefaults.standard.object(forKey: "userId") as! String, "FirstName":txtFirstName.text! ,"LastName":txtLastName.text!,"Address1":txtAddress1.text!,"Address2":txtAddress2.text!,"IsMobileChange":ismobileNoChange,"Cityname":txtCity.text! ,"Phonenumber" : txtPhoneno.text! ,"PostalCode": txtPostalCode.text!,"EmailID": txtEmail.text! ,"RoleName": selectedListArray,"CityID" : "00" , "ProfileDesc":txtView.text!,"ExamID": selectedExamIdList, "StateID": selectedStateid] as [String : Any] as NSDictionary
        }else {
        
         parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID" : UserDefaults.standard.object(forKey: "userId") as! String, "FirstName":txtFirstName.text! ,"LastName":txtLastName.text!,"Address1":txtAddress1.text!,"Address2":txtAddress2.text!,"IsMobileChange":ismobileNoChange,"Cityname":txtCity.text! ,"Phonenumber" : txtPhoneno.text! ,"PostalCode": txtPostalCode.text!,"EmailID": txtEmail.text! ,"RoleName": selectedListArray,"CityID" : "00" , "ProfileDesc":txtView.text!,"ExamID": "0", "StateID": selectedStateid] as [String : Any] as NSDictionary
        }
        
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().updateProfile(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                debugPrint(response)
                if self.ismobileNoChange == "true"{
                    let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
                    UserDefaults.standard.set(nil, forKey: "token")
                    UserDefaults.standard.synchronize()
                    self.navigationController?.pushViewController(vc, animated: false)
                    return
                }
                if response?.object(forKey: "isCentre") as! Bool == true{
                    let vc = TrainerViewController(nibName: "TrainerViewController", bundle: nil)
                    vc.message = response?.object(forKey: "message") as! String
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                else {
                    self.navigationController?.popViewController(animated: true)
                }
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    
    
    ////////
    
    func updateUserRole(isCenter:String){
    
    if ZamModel().isConnectedToNetwork() == false {
    let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
    controller.message = networkError
    addChildViewController(controller)
    controller.view.frame = view.bounds
    view.addSubview(controller.view)
    controller.didMove(toParentViewController: self)
    return
    }
        
    let parameters = ["userid":  UserDefaults.standard.object(forKey: "userId") as! String,"RoleName":selectedListArray] as [String : Any]
    self.view.makeToastActivity(message: "Loading", view: self.view)
    AppEngine().updateUserRole(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
   self.view.hideToastActivity(view: self.view)
    if response == nil{
    self.view.makeToast(message: ServerError)
    return
    }
    if response?.object(forKey: "success") as! Bool  == true{
        
        if  isCenter == "true"{
            let vc = WebViewController(nibName: "WebViewController", bundle: nil)
            vc.comFromEditProfile = true
            self.navigationController?.pushViewController(vc, animated: false)
        }else {
        self.getdata()
        }
       
    } else {
    self.view.makeToast(message: response?.object(forKey: "message") as! String)
    }
    }
    }
    // MARK: - TextView   Delegate
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.text == "About me"{
        textView.text = ""
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    
        let newLength = textView.text.characters.count + text.characters.count - range.length
        if newLength <= 500{
            let nooflines = ZamModel().getNoOfLinesTextView(label: txtView)
            txtViewHeight.constant = CGFloat(nooflines) * 20
            return true
        }else {
            return false
        }
      
       
    }
    
    
    
     // MARK: - TScroll View Delegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tblView.isHidden = true
    }
    
    
    func getDtaWhenRoleTrainer (){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userid":  UserDefaults.standard.object(forKey: "userId") as! String,"RoleName[]":selectedListArray] as [String : Any]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetTrainerInterestList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
               debugPrint(response)
                
                if response?.object(forKey: "data") != nil  && !(response?.object(forKey: "data") is NSNull) {
                    self.interestExamList = (response?.object(forKey: "data") as? NSArray)?.mutableCopy() as! NSMutableArray
                }
                self.associationTypeArray = NSMutableArray()
                if response?.object(forKey: "associationType") != nil  && !(response?.object(forKey: "associationType") is NSNull) {
                    self.associationTypeArray = (response?.object(forKey: "associationType") as? NSArray)?.mutableCopy() as! NSMutableArray
                }
                self.pickerData.removeAll()
                for i in 0..<self.associationTypeArray.count{
                    self.pickerData.append(((self.associationTypeArray[i] as AnyObject).value(forKey: "associateName") as! String))
                }
                self.picker.reloadAllComponents()
                self.SelectedinterestExamList = NSMutableArray()
                self.MainUnSelectedinterestExamList = NSMutableArray()
                self.UnSelectedinterestExamList = NSMutableArray()
                for i in 0..<self.interestExamList.count{
                    if (self.interestExamList[i] as AnyObject).value(forKey: "isOpted") as? Bool == true {
                        self.SelectedinterestExamList.add((self.interestExamList[i] as AnyObject).value(forKey: "examName") as! String)
                    }else {
                        self.MainUnSelectedinterestExamList.add((self.interestExamList[i] as AnyObject).value(forKey: "examName") as! String)
                        self.UnSelectedinterestExamList.add((self.interestExamList[i] as AnyObject).value(forKey: "examName") as! String)
                    }
                }
                if self.dataDic.value(forKey: "trainerAssoCentres") != nil  && !(self.dataDic.value(forKey: "trainerAssoCentres") is NSNull) {
                    self.associatedCentreArray = (self.dataDic.object(forKey: "trainerAssoCentres") as? NSArray)?.mutableCopy() as! NSMutableArray
                }
                self.tblViewAssociatedheight.constant = CGFloat((self.SelectedinterestExamList.count * 50) + 100)  +  CGFloat((self.associatedCentreArray.count * 50) + 160)
                self.tblViewAssociated.reloadData()

                /////
                
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    
    }
    
    @IBAction func btnProfileClicked(_ sender: Any) {
        let controller:ImagePopViewController = ImagePopViewController(nibName: "ImagePopViewController", bundle: nil)
        controller.imageurl = self.dataDic.value(forKey: "photoPath") as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    func callApiForRole(){
    
       // self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetAllRolesForApp() { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                self.IntListdataArray = (response as AnyObject).object(forKey: "data") as! NSArray
                self.lblRole1.text = (self.IntListdataArray.object(at: 0) as AnyObject).value(forKey: "roleName") as? String
                self.lblRole2.text = (self.IntListdataArray.object(at: 1) as AnyObject).value(forKey: "roleName") as? String
                self.lblRole3.text = (self.IntListdataArray.object(at: 2) as AnyObject).value(forKey: "roleName") as? String
                if self.selectedListArray.contains(self.lblRole1.text!){
                    self.btnRole1.setImage(UIImage(named: "selected.png"), for: .normal)
                }else {
                    self.btnRole1.setImage(UIImage(named: "Unselected.png"), for: .normal)
                }
                if self.selectedListArray.contains(self.lblRole2.text!){
                    self.btnRole2.setImage(UIImage(named: "selected.png"), for: .normal)
                }else {
                    self.btnRole2.setImage(UIImage(named: "Unselected.png"), for: .normal)
                }
                if self.selectedListArray.contains(self.lblRole3.text!){
                    self.btnRole3.setImage(UIImage(named: "selected.png"), for: .normal)
                }else {
                    self.btnRole3.setImage(UIImage(named: "Unselected.png"), for: .normal)
                }
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    
    }
    
    
    
    
    
}
