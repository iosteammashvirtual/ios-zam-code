//
//  ProfileAssociatedCentresTableCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/18/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class ProfileAssociatedCentresTableCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnImgView: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
