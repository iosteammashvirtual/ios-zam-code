//
//  MyReviewViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/27/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class MyReviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblReviewNil: UILabel!
    @IBOutlet weak var tblView: UITableView!
    var dataArray = NSMutableArray()
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nibName = UINib(nibName: "MyReviewTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "MyReviewTableViewCell")
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 104;
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        getdata()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - table View Delegate & Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        
        let cell:MyReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyReviewTableViewCell") as! MyReviewTableViewCell
        ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImg, radius: Int(cell.btnImg.frame.size.height / 2), borderWidth: 2)
        cell.lblName.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "centreName") as? String
        cell.lblDate.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewDate") as? String
        cell.lblDesc.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewComment") as? String
        let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "logoPath") as! String
        cell.btnImg.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
        
        if (dataArray[indexPath.row] as AnyObject).value(forKey: "status") as! Bool == true {
        cell.imgWaitingFor.isHidden = true
        cell.lblWaitingForApp.text = ""
           cell.imgWaitingHeight.constant = 0
        }else {
            cell.imgWaitingHeight.constant = 16
         cell.imgWaitingFor.isHidden = false
             cell.lblWaitingForApp.text = "Waiting for Approval"
        }
        
        // cell.lblName.text = dataArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (dataArray[indexPath.row] as AnyObject).value(forKey: "status") as! Bool == true {
            
          //  print(dataArray[indexPath.row])
            
       //    let reviewText = (dataArray[indexPath.row]
            appDelegate.comeFromCenterReview = "myreview"
            let vc = CenterReviewViewController(nibName: "CenterReviewViewController", bundle: nil)
            vc.textValue = (dataArray[indexPath.row] as AnyObject).value(forKey: "reviewComment") as! String
            vc.cenetrID = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "centreID") as! NSNumber))
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    func getdata(){
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let  parameters = ["userID" : UserDefaults.standard.object(forKey: "userId") as! String]
        
        AppEngine().getMyReviewList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                 debugPrint(response)
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.lblReviewNil.text = ""
                self.tblView.reloadData()
            } else {
                self.lblReviewNil.text = response?.object(forKey: "message") as! String
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
