//
//  MyReviewTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/27/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class MyReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblWaitingForApp: UILabel!
    @IBOutlet weak var imgWaitingHeight: NSLayoutConstraint!
    @IBOutlet weak var imgWaitingFor: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
