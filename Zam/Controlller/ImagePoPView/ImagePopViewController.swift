//
//  ImagePopViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/17/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class ImagePopViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnCancel: UIButton!
    
    var imageurl = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        ZamModel().imageCorner(img: imgView, radius: Int(10.0))
    }
    override func viewWillAppear(_ animated: Bool) {
       imgView.sd_setImage(with: NSURL(string: imageurl) as URL!)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
