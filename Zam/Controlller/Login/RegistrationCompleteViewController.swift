//
//  RegistrationCompleteViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit

class RegistrationCompleteViewController: UIViewController {
     var Model = ZamModel()
    
    @IBOutlet weak var btnComplete: UIButton!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblRole1: UILabel!
    @IBOutlet weak var btnRole1: UIButton!
    @IBOutlet weak var btnRole2: UIButton!
    @IBOutlet weak var lblRole2: UILabel!
    @IBOutlet weak var lblRole3: UILabel!
    @IBOutlet weak var btnRole3: UIButton!
    
    var alert = AlertViewController()
    
    var MobileNo = String()
    var userId = String()
    var IntListdataArray = NSArray()
    var selectedListArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtFirstName.attributedPlaceholder = NSAttributedString(string:"First Name",attributes:[NSForegroundColorAttributeName: UIColor.white])
        txtLastName.attributedPlaceholder = NSAttributedString(string:"Family Name",attributes:[NSForegroundColorAttributeName: UIColor.white])
        txtEmail.attributedPlaceholder = NSAttributedString(string:"E-Mail ID",attributes:[NSForegroundColorAttributeName: UIColor.white])
        Model.buttonCorner(Button: btnComplete,radius: 10)
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetAllRolesForApp() { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                
            self.IntListdataArray = (response as AnyObject).object(forKey: "data") as! NSArray
                
                self.lblRole1.text = (self.IntListdataArray.object(at: 0) as AnyObject).value(forKey: "roleName") as? String
                 self.lblRole2.text = (self.IntListdataArray.object(at: 1) as AnyObject).value(forKey: "roleName") as? String
                 self.lblRole3.text = (self.IntListdataArray.object(at: 2) as AnyObject).value(forKey: "roleName") as? String
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnCompleteClick(_ sender: Any) {
        
        var iserror:Bool = false
        var message = String()
        txtEmail.resignFirstResponder()
        txtLastName.resignFirstResponder()
        txtFirstName.resignFirstResponder()
        if Model.isConnectedToNetwork() == false {
            iserror = true
            message = networkError
        }else if ZamModel().condenseWhitespace(str: txtFirstName.text!) == ""{
            iserror = true
            message = firstNamenil
        }else if ZamModel().condenseWhitespace(str: txtLastName.text!) == ""{
            iserror = true
            message = lastNamenil
        }else if txtEmail.text == ""{
            iserror = true
            message = emailnil
        }
        else if Model.validateEmail(txtEmail: txtEmail.text!) == false
        {
            iserror = true
            message = emailnotvalid
        }else if selectedListArray.count == 0{
            iserror = true
            message = InterestNil
        }
        if iserror == true{
            iserror = false
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        let parameters = ["UserId" :userId , "FirstName" : txtFirstName.text!,"LastName": txtLastName.text! , "EmailID" : txtEmail.text!,"MobileNo": MobileNo , "RoleName" : selectedListArray] as [String : Any]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().Register(parameter: parameters as NSDictionary ,url: UpdateProfileAccounts) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
               
                    let vc = SelectInteresetViewController(nibName: "SelectInteresetViewController", bundle: nil)
                    vc.PrimaryInfoId = String(describing: response?.object(forKey: "primaryInfoID") as! NSNumber)
                    self.navigationController?.pushViewController(vc, animated: false)
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
  
    // MARK: - Actions

    @IBAction func btnRole1Clicked(_ sender: Any) {

        if btnRole1.currentImage!.isEqual(UIImage(named: "check.png"))
        {
             btnRole1.setImage(UIImage(named: "uncheck.png"), for: .normal)
            selectedListArray.remove((self.IntListdataArray.object(at: 0) as AnyObject).value(forKey: "roleName") as! String)
        }else {
            
            btnRole1.setImage(UIImage(named: "check.png"), for: .normal)
          selectedListArray.add((self.IntListdataArray.object(at: 0) as AnyObject).value(forKey: "roleName") as! String)
        }
    }
    @IBAction func btnRole2Clicked(_ sender: Any) {
        if btnRole2.currentImage!.isEqual(UIImage(named: "check.png"))
        {
             btnRole2.setImage(UIImage(named: "uncheck.png"), for: .normal)
             selectedListArray.remove((self.IntListdataArray.object(at: 1) as AnyObject).value(forKey: "roleName") as! String)
        }else {
             btnRole2.setImage(UIImage(named: "check.png"), for: .normal)
            selectedListArray.add((self.IntListdataArray.object(at: 1) as AnyObject).value(forKey: "roleName") as! String)
        }
    }
    @IBAction func btnRole3Clicked(_ sender: Any) {
        if btnRole3.currentImage!.isEqual(UIImage(named: "check.png"))
        {
            btnRole3.setImage(UIImage(named: "uncheck.png"), for: .normal)
             selectedListArray.remove((self.IntListdataArray.object(at: 2) as AnyObject).value(forKey: "roleName") as! String)
        }else {
             btnRole3.setImage(UIImage(named: "check.png"), for: .normal)
            selectedListArray.add((self.IntListdataArray.object(at: 2) as AnyObject).value(forKey: "roleName") as! String)
        }
    }


}

