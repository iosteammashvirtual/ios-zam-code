//
//  TrainerViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/4/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerViewController: UIViewController {
    @IBOutlet weak var btnLogOut: UIButton!
var message = String()
   
    @IBOutlet weak var lblMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        ZamModel().buttonCorner(Button: btnLogOut, radius: 10)
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
       lblMessage.text = message
    }
    
    @IBAction func btnLogOutClcked(_ sender: Any) {
        
        let dashboardVC = navigationController!.viewControllers.filter { $0 is LoginViewController }.first!
        navigationController!.popToViewController(dashboardVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
