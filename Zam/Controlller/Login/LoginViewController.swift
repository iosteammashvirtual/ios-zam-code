//
//  LoginViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController,UITextFieldDelegate {

    var Model = ZamModel()
    var comeFromRegistration:Bool = false
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtMobileNo: UITextField!
     let defaults = UserDefaults.standard
    override func viewDidLoad() {
        super.viewDidLoad()
   navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        txtMobileNo.attributedPlaceholder = NSAttributedString(string:"Mobile Number",attributes:[NSForegroundColorAttributeName: UIColor.white])
         txtPassword.attributedPlaceholder = NSAttributedString(string:"Password",attributes:[NSForegroundColorAttributeName: UIColor.white])
        Model.buttonCorner(Button: btnLogin,radius: 10)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnLoginClick(_ sender: Any) {
        var iserror:Bool = false
        var message = String()
        txtMobileNo.resignFirstResponder()
        txtPassword.resignFirstResponder()
            
        if Model.isConnectedToNetwork() == false {
           iserror = true
            message = networkError
        }
       else if txtMobileNo.text == ""{
            
            iserror = true
            message = MobilenoNil
           
        }else if Model.regexPhoneNotCorrect(phoneNo: txtMobileNo.text!) == false
        {
            iserror = true
            message = MobilenoNotValid
        }
        else if txtPassword.text == ""{
            
            iserror = true
            message = PasswordNil
        }
        else if (txtPassword.text?.characters.count)! < 6{
            iserror = true
            message = PasswordShort
        }
        else if (txtPassword.text?.characters.count)! > 15{
            iserror = true
            message = passwordlong
        }
        
        
        if iserror == true{
            iserror = false
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
       
        
          login()
        
   
//         let controller:TrainerViewController = TrainerViewController(nibName: "TrainerViewController", bundle: nil)
//        //vc.message = response?.object(forKey: "message") as! String
//     self.present(controller, animated: true, completion: nil)
   
        
        
    }
    @IBAction func btnSignUp(_ sender: Any) {
        self.txtMobileNo.text = ""
        self.txtPassword.text = ""
       if comeFromRegistration == false{
        let vc = RegistrationViewController(nibName: "RegistrationViewController", bundle: nil)
       vc.comFromLogin = true
        self.navigationController?.pushViewController(vc, animated: false)
        }else {
           self.navigationController?.popViewController(animated: false)
        }
       
        
//        let vc = RegistrationViewController(nibName: "RegistrationViewController", bundle: nil)
//        self.navigationController?.pushViewController(vc, animated: false)
    }
        func login ()
        {

            self.view.makeToastActivity(message: "Loading", view: self.view)
            let username:NSString = txtMobileNo.text! as NSString
            let password:NSString = txtPassword.text! as NSString
                let postData = NSMutableData(data: "grant_type=password&username=\(username)&password=\(password)".data(using: String.Encoding.utf8)!)
                let request = NSMutableURLRequest(url: NSURL(string: loginBase_url)! as URL,
                    cachePolicy: .useProtocolCachePolicy,
                    timeoutInterval: 60.0)
                request.httpMethod = "POST"
                request.httpBody = postData as Data
                let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest) {data,response,error in
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                    self.view.hideToastActivity(view: self.view)
                }
                    if (error != nil) {
                        self.view.makeToast(message: ServerError)
                    } else {
                        let message = try? JSONSerialization.jsonObject(with: data!, options:.allowFragments)
                        debugPrint(message)
                        if (message as AnyObject).object(forKey: "error") as! String == "true"{
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                                self.view.makeToast(message: (message as AnyObject).object(forKey: "error_description") as! String)
                            }
                            return
                        }
                         if (message as AnyObject).object(forKey: "primaryInfoID") as! String == ""{
                            DispatchQueue.main.async{
                                let vc = RegistrationCompleteViewController(nibName: "RegistrationCompleteViewController", bundle: nil)
                                vc.userId = (message as AnyObject).object(forKey: "userId") as! String
                                
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            return
                        }
                        if (message as AnyObject).object(forKey: "isInterest") as! String == "false"{
                        //////////
                              DispatchQueue.main.async{
                                let vc = SelectInteresetViewController(nibName: "SelectInteresetViewController", bundle: nil)
                                vc.PrimaryInfoId  = (message as AnyObject).object(forKey: "primaryInfoID") as! String
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                            return
                        }
                        if (message as AnyObject).object(forKey: "IsCentre") as! String == "True"{
                            DispatchQueue.main.async{
                                let vc = TrainerViewController(nibName: "TrainerViewController", bundle: nil)
                                vc.message = (message as AnyObject).object(forKey: "message") as! String
                                self.navigationController?.pushViewController(vc, animated: false)
                            }
                        }else{
//                             self.defaults.set((message as AnyObject).description as String, forKey: "LoginData")
                             DispatchQueue.main.async{
                                self.defaults.set((message as AnyObject).object(forKey: "access_token"), forKey: "token")
                                self.defaults.set((message as AnyObject).object(forKey: "primaryInfoID") as! String, forKey: "primaryInfoId")
                                self.defaults.set((message as AnyObject).object(forKey: "userId") as! String, forKey: "userId")
                                self.defaults.set((message as AnyObject).object(forKey: "role") as! String, forKey: "role")
                                self.defaults.set((message as AnyObject).object(forKey: "userName") as! String, forKey: "userName")
                                self.defaults.synchronize()
                                
                                 DispatchQueue.main.async{
                                
                                    
                                    if (self.navigationController?.viewControllers.contains(NearViewController()))!{
                                        let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                                self.navigationController?.viewControllers.remove(at: 0)
                                       
                                    }
                                    
                                     let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                    appDelegate.makeRootVieController()
                                    
//                                let vc = NearViewController(nibName: "NearViewController", bundle: nil)
//                                self.navigationController?.pushViewController(vc, animated: false)
                               
//                                appDelegate.window?.rootViewController = appDelegate.tabBarController
                                }
                            }
                        }
                        // print(UserDefaults.standard.object(forKey: "token") as! String)
                        //LOGINSUCCESS
                    }
                }
                dataTask.resume()
            }
    @IBAction func btnForgotClicked(_ sender: Any) {
    let controller: ForrgotViewController =  ForrgotViewController(nibName: "ForrgotViewController", bundle: nil)
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if newLength <= 10{
            return true
        }else {
            return false
        }
    }
    
}
