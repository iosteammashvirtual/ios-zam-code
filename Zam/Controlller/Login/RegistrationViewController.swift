//
//  RegistrationViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit


class RegistrationViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtReEnterMobile: UITextField!
    var comFromLogin : Bool = false
    
    @IBOutlet weak var btnRegister: UIButton!
    var Model = ZamModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        txtMobileNo.attributedPlaceholder = NSAttributedString(string:"Mobile Number",attributes:[NSForegroundColorAttributeName: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string:"Password",attributes:[NSForegroundColorAttributeName: UIColor.white])
        txtReEnterMobile.attributedPlaceholder = NSAttributedString(string:"Re-Enter Mobile Number",attributes:[NSForegroundColorAttributeName: UIColor.white])
        Model.buttonCorner(Button: btnRegister,radius: 10)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnRegisterClick(_ sender: Any) {
        
        var iserror:Bool = false
        var message = String()
        txtReEnterMobile.resignFirstResponder()
        txtMobileNo.resignFirstResponder()
        txtPassword.resignFirstResponder()
        
        if Model.isConnectedToNetwork() == false {
           iserror = true
            message = networkError
        }else if txtMobileNo.text == ""{
            iserror = true
            message = MobilenoNil
        }
        else if Model.regexPhoneNotCorrect(phoneNo: txtMobileNo.text!) == false
        {
            iserror = true
            message = MobilenoNotValid
        }
        else if txtReEnterMobile.text == ""{
            iserror = true
            message = ConfirmMobilenoNil
           
        }else if !(txtMobileNo.text == txtReEnterMobile.text) {
            iserror = true
            message = mobileNoNotSame
        }
        
        else if txtPassword.text == ""{
            iserror = true
            message = PasswordNil
            
        }else if (txtPassword.text?.characters.count)! < 6{
            iserror = true
            message = PasswordShort
        }
        else if (txtPassword.text?.characters.count)! > 15{
            iserror = true
            message = passwordlong
        }
        

        if iserror == true{
            iserror = false
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = [ "MobileNo" :txtMobileNo.text!]
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().Register(parameter: parameters as NSDictionary,url: generateOtp) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
               // debugPrint(response?.object(forKey: "otp") as! String)
                let vc = OTPViewController(nibName: "OTPViewController", bundle: nil)
                vc.OtpNumber = response?.object(forKey: "otp") as! String
                vc.mobileNo = self.txtMobileNo.text!
                vc.Password = self.txtPassword.text!
                vc.message = response?.object(forKey: "message") as! String
                self.navigationController?.pushViewController(vc, animated: false)
                
            } else {
            self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    @IBAction func btnLoginClick(_ sender: Any) {
        
       // self.navigationController?.popViewController(animated: false)
        self.txtMobileNo.text = ""
        self.txtPassword.text = ""
        self.txtReEnterMobile.text = ""
        if comFromLogin == true{
        self.navigationController?.popViewController(animated: false)
        }else {
            let vc = LoginViewController(nibName: "LoginViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if newLength <= 10{
            return true
        }else {
            return false
        }
    }
    

}
