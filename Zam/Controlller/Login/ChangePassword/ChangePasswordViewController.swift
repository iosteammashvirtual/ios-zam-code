//
//  ChangePasswordViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/10/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnChangePassword: UIButton!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var imgOldPassword: UIImageView!
    @IBOutlet weak var TopConstraint: NSLayoutConstraint!
    var comeFromForgot:Bool = false
    var MobileNo = String()
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ZamModel().buttonCorner(Button: btnChangePassword, radius: Int(10))
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
         NotificationCenter.default.addObserver(self, selector: #selector(ChangePasswordViewController.yesClicked(notification:)), name: Notification.Name("ChangePassword"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        if comeFromForgot == true{
        btnClose.isHidden = true
        imgOldPassword.isHidden = true
        txtOldPassword.isHidden = true
        TopConstraint.constant = 20
        }else {
        btnClose.isHidden = false
        TopConstraint.constant = 78
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnChangedPasswordClicked(_ sender: Any) {
        
        txtNewPassword.resignFirstResponder()
        txtOldPassword.resignFirstResponder()
        txtConfirmPassword.resignFirstResponder()
        
        var iserror:Bool = false
        var message = String()
        if ZamModel().isConnectedToNetwork() == false {
            iserror = true
            message = networkError
       }
        
        if comeFromForgot == false {
        
            if txtOldPassword.text! == ""{
                iserror = true
                message = olpPasswordNil
            }
            else if txtNewPassword.text! == ""{
                iserror = true
                message = NewPasswordNil
                
            }
            else if (txtNewPassword.text?.characters.count)! < 6{
                iserror = true
                message = PasswordShort
                
            }else if (txtNewPassword.text?.characters.count)! > 15{
                iserror = true
                message = passwordlong
            }
            else if txtConfirmPassword.text! == ""{
                iserror = true
                message = ConfirmPassnil
            }
            else if !(txtNewPassword.text! == txtConfirmPassword.text!) {
                iserror = true
                message = passNotSame
            }
            
        }else {
        
            if txtNewPassword.text! == ""{
                iserror = true
                message = NewPasswordNil
                
            }
            else if (txtNewPassword.text?.characters.count)! < 6{
                iserror = true
                message = PasswordShort
                
            }else if (txtNewPassword.text?.characters.count)! > 15{
                iserror = true
                message = passwordlong
            }
            else if txtConfirmPassword.text! == ""{
                iserror = true
                message = ConfirmPassnil
            }
            else if !(txtNewPassword.text! == txtConfirmPassword.text!) {
                iserror = true
                message = passNotSame
            }
        }
            
        
        if iserror == true{
            iserror = false
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        if comeFromForgot == false{
            let parameters = ["OldPassword" :txtOldPassword.text!,"UserName":UserDefaults.standard.object(forKey: "userName") as! String ,"NewPassword":txtNewPassword.text! ,"ConfirmPassword" : txtConfirmPassword.text!]
            
           self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().changePassword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true{
                   
                let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                    controller.message = response?.object(forKey: "message") as! String
                    controller.comeFromChangePass = true
                    self.addChildViewController(controller)
                    controller.view.frame = self.view.bounds
                    self.view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
//                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                   
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
            
        
        
        }else {
            
            let parameters = [ "MobileNo" :MobileNo,"Password":txtNewPassword.text! ,"ConfirmPassword":txtConfirmPassword.text! ]
            
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().Resetpassword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true{
                    
                     self.view.makeToast(message:passwordChanged)
                    let delayInSeconds = 2.0
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayInSeconds) {
                        self.navigationController!.viewControllers.insert(LoginViewController(), at: 0)
                        let dashboardVC = self.navigationController!.viewControllers.filter { $0 is LoginViewController }.first!
                        self.navigationController!.popToViewController(dashboardVC, animated: true)
                    }
                    
                }else {
                        self.view.makeToast(message: response?.object(forKey: "message") as! String)
                    }
                        
                    }
                    
        }
       
        
    }
    
    func yesClicked(notification:NSNotification){
         self.tabBarController?.tabBar.isHidden = false
    self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
