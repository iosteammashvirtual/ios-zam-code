//
//  OTPPoPUpViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/20/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class OTPPoPUpViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var alertview: UIView!
    @IBOutlet weak var btnVerify: UIButton!
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var lblTimer: UILabel!
    private var countDownTimer = Timer()
    var otpExpire:Bool = false
    private var timerValue = 300
    
    var OtpNumber = String()
    var mobileNo = String()
    var Password = String()
    var comeFromForgot:Bool = false
     var comeFromEditProfile:Bool = false
    
    @IBOutlet weak var lblnotreceiveEmail: UILabel!
    @IBOutlet weak var btnResend: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Timer Call
        
        
         ZamModel().viewCorner(Button: alertview, radius: Int(5.0))
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
        ZamModel().buttonCorner(Button: btnVerify,radius: 10)
        // Do any additional setup after loading the view.

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:- Timer Method
    private func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        // let hours: Int = totalSeconds / 3600
        return String(format: "%02d : %02d", minutes, seconds)
    }
    @objc private func countdown() {
        self.timerValue -= 1
        if self.timerValue < 0 {
            lblnotreceiveEmail.isHidden = false
            btnResend.isHidden = false
            self.countDownTimer.invalidate()
            lblnotreceiveEmail.text = "Your OTP has expired."
            otpExpire = true
        }
        else {
            if self.timerValue < 270 {
               lblnotreceiveEmail.text = "Didn't receive the SMS?"
                lblnotreceiveEmail.isHidden = false
                btnResend.isHidden = false
            }
            lblTimer.text = (self.timeFormatted(totalSeconds: self.timerValue))
        }
    }
    
    //MARK:- TextField Delegate
    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //        if textField.tag == 1{
    //            if string.characters.count == 1{
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
    //                   self.txtField2.becomeFirstResponder()
    //                }
    //            }
    //        }else if textField.tag == 2{
    //            if string.characters.count == 1{
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
    //                    self.txtField3.becomeFirstResponder()
    //                }
    //            }
    //        }else if textField.tag == 3{
    //                if string.characters.count == 1{
    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
    //                        self.txtField4.becomeFirstResponder()
    //                    }
    //            }
    //        }else if textField.tag == 4{
    //            if string.characters.count == 1{
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
    //                    textField.resignFirstResponder()
    //                }
    //        }
    //        }
    //        return true
    //    }
    //MARK:- Actions
    @IBAction func btnResendClick(_ sender: Any) {
        
        if comeFromEditProfile == true{
        getOtpValueForEdit()
        }else {
        getOtpValue()
        }
        
        otpExpire = false
        timerValue = 300
    }
   
    
    @IBAction func btnVerifyClickedClick(_ sender: Any) {
        
       txtField.resignFirstResponder()
        if txtField.text == ""{
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = OTPNil
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        if otpExpire == true{
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = OTPExpire
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        if OtpNumber == txtField.text{
           if comeFromForgot == true{
                let vc = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
                vc.MobileNo = self.mobileNo
                vc.comeFromForgot = true
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else {
            
            NotificationCenter.default.post(name: Notification.Name("ComeFromOtpPoPUp"), object: nil)
            self.willMove(toParentViewController: nil)
                self.willMove(toParentViewController: nil)
                self.view.removeFromSuperview()
                self.removeFromParentViewController()
            }
        }else {
            self.view.makeToast(message: WrongOtp)
        }
    }
    
    func getOtpValue() {
        let parameters = [ "MobileNo" :mobileNo]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().ForgotPassword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                 self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.lblnotreceiveEmail.isHidden = true
                self.btnResend.isHidden = true
                self.OtpNumber = response?.object(forKey: "otp") as! String
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    
    
    
    func getOtpValueForEdit() {
        let parameters = [ "MobileNo" :mobileNo]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().Register(parameter: parameters as NSDictionary,url: generateOtp) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                 self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.lblnotreceiveEmail.isHidden = true
                self.btnResend.isHidden = true
                self.OtpNumber = response?.object(forKey: "otp") as! String
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    
    
    
    
    
    
    
    @IBAction func btnCloseClick(_ sender: Any) {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if newLength <= 12{
            return true
        }else {
            return false
        }
    }
    
}
