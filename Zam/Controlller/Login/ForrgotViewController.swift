//
//  ForrgotViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/10/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class ForrgotViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var alertview: UIView!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var txtMobileNumber: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
ZamModel().buttonCorner(Button: btnSubmit, radius: Int(10))
         ZamModel().viewCorner(Button: alertview, radius: Int(5.0))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

    @IBAction func btnSubmitClicked(_ sender: Any) {
        
        var iserror:Bool = false
        var message = String()
       
        if ZamModel().isConnectedToNetwork() == false {
            iserror = true
            message = networkError
        }else if txtMobileNumber.text == ""{
            iserror = true
            message = MobilenoNil
            
        }
        else if ZamModel().regexPhoneNotCorrect(phoneNo: txtMobileNumber.text!) == false
        {
            iserror = true
            message = MobilenoNotValid
        }
        if iserror == true{
            iserror = false
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = [ "MobileNo" :txtMobileNumber.text!]
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().ForgotPassword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
               // debugPrint(response?.object(forKey: "otp") as! String)
                 self.view.makeToast(message: response?.object(forKey: "message") as! String)
                let controller:OTPPoPUpViewController = OTPPoPUpViewController(nibName: "OTPPoPUpViewController", bundle: nil)
                controller.OtpNumber = response?.object(forKey: "otp") as! String
                controller.mobileNo = self.txtMobileNumber.text!
                controller.comeFromForgot = true
                self.addChildViewController(controller)
                controller.view.frame = self.view.bounds
                self.view.addSubview(controller.view)
                controller.didMove(toParentViewController: self)
                
              //  let vc = OTPViewController(nibName: "OTPViewController", bundle: nil)
               
              //  self.navigationController?.pushViewController(vc, animated: false)
            } else {
                self.txtMobileNumber.resignFirstResponder()
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
       // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        if newLength <= 10{
            return true
        }else {
            return false
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
