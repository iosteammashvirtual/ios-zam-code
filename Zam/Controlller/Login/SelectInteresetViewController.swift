//
//  SelectInteresetViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/11/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class SelectInteresetViewController: UIViewController {

    @IBOutlet weak var btnSeracNow: UIButton!
    var PrimaryInfoId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
ZamModel().buttonCorner(Button: btnSeracNow, radius: 10)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSearchNowClicked(_ sender: Any) {
        
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = [ "PrimaryInfoID" :PrimaryInfoId]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().createExaminterest(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
              //  self.navigationController!.viewControllers.removeAll()
                if  self.navigationController!.viewControllers.contains(LoginViewController()){
                }else {
                 self.navigationController!.viewControllers.insert(LoginViewController(), at: 0)
                }
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is LoginViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)

            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
