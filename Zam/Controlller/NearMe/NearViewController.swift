
//  NearViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 12/30/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit
import GoogleMaps
import MobileCoreServices
import CoreLocation
import Alamofire
class NearViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var btnMap: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var btnUseLocation: UIButton!
    @IBOutlet weak var btnZoomIn: UIButton!
    @IBOutlet weak var btnZoomOut: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var btnListView: UIButton!
   //  var listView = MapListViewController()
    var marker = GMSMarker()
    var isListViewOpen:Bool = false
    var currnetLatLong = CLLocation()
     let defaults = UserDefaults.standard
    var locationManager = CLLocationManager()
    var dataArray = NSMutableArray()
    var Searchparameter = NSDictionary()
    var isFirsttimeCallService:Bool = true
    var ScrollEnable:Bool = true
    var gmaps: GMSMapView?
    var comeFromSearch:Bool = false
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = UINib(nibName: "MapListCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "MapListCell")
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 150;

         ZamModel().buttonCornerWithThemeBorder(Button: btnListView, radius: 1, borderWidth: 0)
        ZamModel().buttonCornerWithThemeBorder(Button: btnMap, radius: 1, borderWidth: 0)
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
      //  self.tabBarController?.tabBar.selectedItem = self.tabBarController?.tabBar.items![0]
         self.tabBarController?.tabBar.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(NearViewController.closeMapView(notification:)), name: Notification.Name("CloseMapView"), object: nil)
    // Map
        let target: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 151.6, longitude: 217.2)
        let camera: GMSCameraPosition = GMSCameraPosition(target: target, zoom: 12.0, bearing: 0, viewingAngle: 0)
        gmaps = GMSMapView(frame: CGRect(x:0, y:0, width:self.mapView.bounds.width, height:self.mapView.bounds.height))
        if gmaps != nil {
            mapView.camera = camera
            mapView.delegate = self
        }
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        // for custom compass
       // locationManager.startUpdatingHeading()
        // Do any additional setup after loading the view.
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(NearViewController.SwipeRightGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
      //  colseSliderView()
//        self.mapView.clear()
         self.tabBarController?.tabBar.isUserInteractionEnabled = true
         NotificationCenter.default.post(name: Notification.Name("SliderClosed"), object: nil)
         self.tabBarController?.tabBar.isHidden = false
        
        if appDelegate.comeFromSearchOnMap == true{
        comeFromSearch = true
        Searchparameter = appDelegate.searchparameter
        }else {
        comeFromSearch = false

        }
        if comeFromSearch == true{
            if  appDelegate.addToCompareClicked == true {
        getSearchedData()
            }
        }else {
            if  appDelegate.addToCompareClicked == true {
          getdata()
            }
        }
    }
  
        override func viewWillDisappear(_ animated: Bool) {
            appDelegate.addToCompareClicked = true
    }
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sliderclicked(sender: AnyObject) {
       SliderOpen()
    }
    
     //MARK:- Delagate Of Google map
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            if gmaps != nil {
            }
        }
    }
    
  // For Custom Compass
    
//    func locationManager(_ manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
//        // This will print out the direction the device is heading
//        print(heading.magneticHeading)
//    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        locationManager.startUpdatingLocation()
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = false
        mapView.settings.compassButton = false
        
               if let location = locations.first as CLLocation! {
                if isFirsttimeCallService{
                    isFirsttimeCallService = false
                    currnetLatLong = location
                    appDelegate.userLocation = currnetLatLong
                    
                    if ZamModel().isConnectedToNetwork() == true
                    {
                    getCityName()
                    }
                    if comeFromSearch == false{
                     getdata()
                    }
                }
                if comeFromSearch == false{
                    if gmaps != nil {
                        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 12.0, bearing: 0, viewingAngle: 0)
                        marker.position = location.coordinate
                        marker.icon = GMSMarker.markerImage(with: ZamModel().themecolor)
                        marker.map = mapView
                        locationManager.stopUpdatingLocation()
                }
                }
        }
    }
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        
          let markerValue: CLLocationCoordinate2D = marker.position
       // print(markerValue.latitude)
        let predicate = NSPredicate(format: "latitude == '\(markerValue.latitude)'")
        let list:NSMutableArray = (self.dataArray.filtered(using: predicate) as NSArray).mutableCopy() as! NSMutableArray
        if list.count>0{
            let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
            vc.CentreID = String(describing: (list[0] as AnyObject).value(forKey: "centreID") as! NSNumber)
            self.navigationController?.pushViewController(vc, animated: false)
        }
     //   print(marker.title!)
    }

    func getdata(){
        
    appDelegate.comeFromSearchOnMap = false
    comeFromSearch = false
    if ZamModel().isConnectedToNetwork() == false {
    let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
    controller.message = networkError
    addChildViewController(controller)
    controller.view.frame = view.bounds
    view.addSubview(controller.view)
    controller.didMove(toParentViewController: self)
    return
    }
    let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String,"latitude" :  (currnetLatLong.coordinate.latitude) , "longitude" :  (currnetLatLong.coordinate.longitude)] as [String : Any]
    self.view.makeToastActivity(message: "Loading", view: self.view)
    AppEngine().getCenterList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
          self.view.hideToastActivity(view: self.view)
    if response == nil{
    self.view.makeToast(message: ServerError)
    return
    }
    if response?.object(forKey: "success") as! Bool  == true{
//print(response?.object(forKey: "data") as! NSArray)
    self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
       self.tblView.reloadData()
       self.setMarker()
    } else {
    self.view.makeToast(message: response?.object(forKey: "message") as! String)
    }
    }
}
    func getSearchedData(){
            if ZamModel().isConnectedToNetwork() == false {
                let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                controller.message = networkError
                addChildViewController(controller)
                controller.view.frame = view.bounds
                view.addSubview(controller.view)
                controller.didMove(toParentViewController: self)
                return
            }
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().getSearchCenterByKeyword(parameter: Searchparameter as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true{
                    self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    let position = CLLocationCoordinate2D(latitude: CLLocationDegrees((self.dataArray[0] as AnyObject).value(forKey: "latitude") as! String)!, longitude: CLLocationDegrees((self.dataArray[0] as AnyObject).value(forKey: "longitude") as! String)!)
//                    self.currnetLatLong = CLLocation(latitude: position.latitude, longitude: position.longitude)
                    self.mapView.camera = GMSCameraPosition(target: position, zoom: 10.0, bearing: 0, viewingAngle: 0)
                    self.mapView.clear()
                    self.tblView.reloadData()
                    self.setMarker()
                   // print(response?.object(forKey: "data") as! NSArray)
                 } else {
                    self.dataArray.removeAllObjects()
                    self.tblView.reloadData()
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
    }
    //MARK:- Set Marker
    func setMarker (){
        for i in 0..<dataArray.count{
//        print((dataArray[i] as AnyObject).value(forKey: "latitude") as! String)
//        print((dataArray[i] as AnyObject).value(forKey: "longitude") as! String)
        let position = CLLocationCoordinate2D(latitude: CLLocationDegrees((dataArray[i] as AnyObject).value(forKey: "latitude") as! String)!, longitude: CLLocationDegrees((dataArray[i] as AnyObject).value(forKey: "longitude") as! String)!)
        let marker1 = GMSMarker(position: position)
        marker1.position = position
        marker1.icon = GMSMarker.markerImage(with: UIColor.red)
        marker1.title = (dataArray[i] as AnyObject).value(forKey: "centreName") as? String
        marker1.snippet = (dataArray[i] as AnyObject).value(forKey: "address1") as? String
        marker1.map = mapView
        }
    }
    @IBAction func btnListViewClicked(_ sender: Any) {
        isListViewOpen = true
        btnZoomIn.isHidden = true
        btnZoomOut.isHidden = true
         btnUseLocation.isHidden = true
        listView.isHidden = false
//            let controller:MapListViewController = MapListViewController(nibName: "MapListViewController", bundle: nil)
//            addChildViewController(controller)
//        controller.comFromSearch = comeFromSearch
//        controller.Searchparameter = Searchparameter
//            controller.currnetLatLong = currnetLatLong
//            controller.view.frame = mapView.bounds
//            btnListView.isHidden = true
//            mapView.addSubview(controller.view)
//            controller.didMove(toParentViewController: self)
    }
    @IBAction func btnMapViewClicked(_ sender: Any) {
        
        listView.isHidden = true
        btnListView.isHidden = false
        btnZoomIn.isHidden = false
        btnZoomOut.isHidden = false
        btnUseLocation.isHidden = false
        
    }
    
    
    
    
    // Delegate
    func closeMapView(notification: Notification) {
        btnListView.isHidden = false
        btnZoomIn.isHidden = false
        btnZoomOut.isHidden = false
        btnUseLocation.isHidden = false
         // getdata()
    }
    @IBAction func btnSearchClicked(_ sender: Any) {
    
        if btnListView.isHidden == true {
            NotificationCenter.default.post(name: Notification.Name("CloseMapViewWhenSearchOpen"), object: nil)
        }
        let vc = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func getCityName(){
        let geoCoder = CLGeocoder()
       // let location = CLLocation(latitude: curr, longitude: touchCoordinate.longitude)
        geoCoder.reverseGeocodeLocation(currnetLatLong, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            // Address dictionary
          //  print(placeMark.addressDictionary)
            // Location name
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                if city == "Gurgaon"{
                 self.appDelegate.currentState = "Gurugram"
                }else {
               self.appDelegate.currentState = city as String
                }
            }
         })
    }
    
    //MARK:- Zoom In Zoom Out
    
    @IBAction func btnZoomOutClicked(_ sender: Any) {
        let zoomCamera = GMSCameraUpdate.zoomOut()
        mapView.animate(with: zoomCamera)
           }
    @IBAction func btnZoomIn(_ sender: Any) {
        let zoomCamera = GMSCameraUpdate.zoomIn()
        mapView.animate(with: zoomCamera)
    }
    @IBAction func btnUseLocationClicked(_ sender: Any) {
        comeFromSearch = false
        getdata()
        mapView.animate(toZoom: 18)
        
        if ZamModel().isConnectedToNetwork() == true
        {
            getCityName()
        }
       
          mapView.animate(toLocation: CLLocationCoordinate2D(latitude: appDelegate.userLocation.coordinate.latitude, longitude: appDelegate.userLocation.coordinate.longitude))
    }
    func SwipeRightGesture(gesture: UIGestureRecognizer) {
        
       
        
      if appDelegate.SwipeRightOpen == false
      {
        SliderOpen()
        }
    }
    
    
    func SliderOpen(){
        
        let menuVC : SliderViewController = SliderViewController(nibName: "SliderViewController", bundle: nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        appDelegate.SwipeRightOpen = true
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.frame = CGRect(x:0 - UIScreen.main.bounds.size.width, y:0, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame = CGRect(x:0 , y:0, width:UIScreen.main.bounds.size.width , height:UIScreen.main.bounds.size.height);
        }, completion:nil)
    
    }
    
    //MARK:- Table View Delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataArray.count == 0{
        return 0
        }else{
        return dataArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
        // return 170
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:MapListCell = tableView.dequeueReusableCell(withIdentifier: "MapListCell") as! MapListCell
        let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "photoPath") as! String
        cell.imgView.sd_setImage(with: NSURL(string: imageurl) as URL!)
        cell.btnCompare.tag = indexPath.row
        ZamModel().roundImageView(image: cell.imgView)
        cell.lblName.text = (dataArray[indexPath.row] as AnyObject).value(forKey: "centreName") as? String
        
        let data = ((dataArray[indexPath.row] as AnyObject).object(forKey: "exams") as? NSArray)?.mutableCopy() as! NSMutableArray
        
        //  print(data)
        var examName = String()
        for i in 0..<data.count{
            examName =  examName + ((data[i] as AnyObject).value(forKey: "examName") as? String)! + ","
        }
        cell.lblExamName.text =  String(examName.characters.dropLast())
        
        cell.lblRateCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber)) + "/5"
        
        ZamModel().setSmileImageThemeColor(btn: cell.btnRate, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber))
        cell.btnRate.addTarget(self, action: #selector(NearViewController.RateClicked), for: .touchUpInside)
        cell.btnreview.addTarget(self, action: #selector(NearViewController.ReviewClicked), for: .touchUpInside)
        cell.btnPhone.addTarget(self, action: #selector(NearViewController.btnContactClicked), for: .touchUpInside)
        
        cell.lblReviewCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "noReviews") as! NSNumber))
        if (dataArray[indexPath.row] as AnyObject).value(forKey: "isFav") as! Bool == true{
            cell.btnCompare.setImage(UIImage(named:"compare50_red.png"), for: UIControlState.normal)
        }else {
            cell.btnCompare.setImage(UIImage(named:"compare50.png"), for: UIControlState.normal)
        }
        cell.btnCompare.addTarget(self, action: #selector(NearViewController.addTocompareClicked), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let centreID = Int((dataArray[indexPath.row] as AnyObject).value(forKey: "centreID") as! NSNumber)
        
        let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
        vc.CentreID =  String(describing: centreID)
        //  print(response?.object(forKey: "data") as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: false)
        //getDetail(centerId: String(describing: centreID))
    }
    
    @IBAction func btnCloseView(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("CloseMapView"), object: nil)
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    func addTocompareClicked(sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID)]
        let cell = tblView.cellForRow(at: indexPath!) as! MapListCell
        if  cell.btnCompare.currentImage!.isEqual(UIImage(named: "compare50.png"))
        {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().addToCompare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true{
                    //  self.getdata()
                    let dic:NSDictionary = self.dataArray[(indexPath?.row)!] as! NSDictionary
                    let value = dic.mutableCopy() as! NSMutableDictionary
                    value.setValue(true, forKey: "isFav")
                    print(value)
                    self.dataArray.replaceObject(at: (indexPath?.row)!, with: value)
                    self.tblView.reloadData()
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }else {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().removeFromComare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "success") as! Bool  == true{
                    //  self.getdata()
                    let dic:NSDictionary = self.dataArray[(indexPath?.row)!] as! NSDictionary
                    let value = dic.mutableCopy() as! NSMutableDictionary
                    value.setValue(false, forKey: "isFav")
                    self.dataArray.replaceObject(at: (indexPath?.row)!, with: value)
                    print(value)
                    self.tblView.reloadData()
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }
    }
    func RateClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID),"userID":UserDefaults.standard.object(forKey: "userId") as! String,]
        let vc = CenterRateViewController(nibName: "CenterRateViewController", bundle: nil)
        appDelegate.comeFromCenterRate = "nearme"
        vc.parameter = parameters as NSDictionary
        vc.centerID = String(describing: centreID)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
        /*
         self.view.makeToastActivity(message: "Loading")
         AppEngine().removeFromComare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
         if response == nil{
         self.view.makeToast(message: ServerError)
         return
         }
         self.view.hideToastActivity()
         if response?.object(forKey: "success") as! Bool  == true{
         } else {
         self.view.makeToast(message: response?.object(forKey: "message") as! String)
         }
         }
         */
    }
    func btnContactClicked(sender:UIButton){
        let controller:ContactAlertViewController = ContactAlertViewController(nibName: "ContactAlertViewController", bundle: nil)
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        print(dataArray[(indexPath?.row)!])
        controller.phoneNo = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "mobileNo") as! String
        controller.email = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "emailID") as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    func ReviewClicked(sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
        let vc = CenterReviewViewController(nibName: "CenterReviewViewController", bundle: nil)
        vc.cenetrID = String(describing: centreID)
        appDelegate.comeFromCenterReview = "nearme"
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    
    
    
}
