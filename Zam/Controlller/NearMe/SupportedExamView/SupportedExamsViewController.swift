//
//  SupportedExamsViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/16/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class SupportedExamsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNilMessage: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    let defaults = UserDefaults.standard
    var dataArray = NSMutableArray()
    var rateValue = String()
    var nameTitle = String()
    var parameter = NSDictionary()
    var message = ""
    @IBOutlet weak var lblRateCount: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let nibName = UINib(nibName: "SupportedExamTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "SupportedExamTableViewCell")
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 100;
    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        getdata()
        
        self.tabBarController?.tabBar.isHidden = true
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - TableViewDelegate & DataSource
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:SupportedExamTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SupportedExamTableViewCell") as! SupportedExamTableViewCell
        
        cell.btnRate.addTarget(self, action: #selector(switchToRate), for: UIControlEvents.touchUpInside)
        cell.btnReview.addTarget(self, action: #selector(switchToReview), for: UIControlEvents.touchUpInside)
        
        cell.lblCourseName.text =  (dataArray[indexPath.row] as AnyObject).value(forKey: "courseName") as? String
        
        cell.lblReviewCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "noReviews") as! NSNumber))
        
        
        cell.lblRateCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "rateValue") as! NSNumber)) + "/5"
        
        ZamModel().setSmileImageThemeColor(btn: cell.btnRate, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "rateValue") as! NSNumber))
        
        return cell
        
    }
    
    func switchToRate(sender:UIButton)
    {
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let courseId = (self.dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "cenCourseID") as! NSNumber
        let parameters = ["CenCourseID":courseId,"UserId":defaults.object(forKey: "userId")as! String,"PrimaryInfoID":defaults.object(forKey: "primaryInfoId") as! String]  as [String : Any]
        vc.navigateCourse = true
        vc.courseID = courseId
        vc.parameter = parameters as NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    func switchToReview(sender:UIButton)
    {
        //Nitika Commit
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        
        let courseId = (self.dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "cenCourseID") as! NSNumber
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        vc.courseID = courseId
        vc.navigateCourse = true
        vc.comeFromRateSuccess = false
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = SupportedExamDetailVC(nibName: "SupportedExamDetailVC", bundle: nil)
        vc.detailDic = dataArray[indexPath.row] as! NSDictionary
        self.navigationController?.pushViewController(vc, animated: false)
        
        //
        //            let trainerID =  (centerTrainer[indexPath.row - 1 ] as AnyObject).value(forKey: "trainerID") as! NSNumber
        //            getTrainerDetail(trainerId: String(describing: trainerID))
        
    }
    
    func getdata(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCenterCoursesList(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                let data = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                print(data)
                self.dataArray = data
                
                //                    let vc = SupportedExamsViewController(nibName: "SupportedExamsViewController", bundle: nil)
                //                    vc.dataArray = data
                //                    vc.nameTitle = name
                //                    vc.rateValue = String(describing: response?.object(forKey: "overallRate") as! NSNumber)
                //                    self.navigationController?.pushViewController(vc, animated: false)
                
                let rateVal = String(describing: response?.object(forKey: "overallRate") as! NSNumber)
                self.lblTitle.text = self.nameTitle
                self.lblNilMessage.text = ""
                self.lblRateCount.text = rateVal + "/5"
                ZamModel().setSmileImageWhite(btn: self.btnRate, rateCount:Int(rateVal)!)
                
                
                self.tblView.reloadData()
            } else
            {
                //  self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.lblNilMessage.text = response?.object(forKey: "message") as! String
                self.lblTitle.text = self.nameTitle
                self.lblRateCount.text = "0/5"
                ZamModel().setSmileImageWhite(btn: self.btnRate, rateCount: Int(0))
                
                //   vc.rateValue = String(describing: self.dataDic.object(forKey: "ratevalue") as! NSNumber)
            }
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
