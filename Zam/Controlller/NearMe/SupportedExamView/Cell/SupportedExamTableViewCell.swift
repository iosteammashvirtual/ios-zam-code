//
//  SupportedExamTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/16/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class SupportedExamTableViewCell: UITableViewCell {

    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var lblCourseName: UILabel!
    @IBOutlet weak var lblReviewCount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
