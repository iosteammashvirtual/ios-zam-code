//
//  SupportedExamDetailVC.swift
//  Zam
//
//  Created by Admin on 02/02/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import MessageUI

class SupportedExamDetailVC: UIViewController,UINavigationControllerDelegate {

    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDescTitle: UILabel!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var lblExamName: UILabel!
    @IBOutlet weak var lblExamFees: UILabel!
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet var headerView: UIView!
     @IBOutlet weak var btnRateClicked: UIButton!
 
    var detailDic = NSDictionary()
    let defaults = UserDefaults.standard

    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        print(self.detailDic)
        self.tbleView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.tbleView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0 , width: self.tbleView.bounds.size.width, height: 0.1))
        if self.detailDic["courseDesc"] as? String != ""{
            lblDesc.attributedText = ZamModel().stringFromHtml(string: (self.detailDic["courseDesc"] as? String)!)
            lblDescTitle.text = "Description"
            lblDesc.textColor = ZamModel().themecolor
        }else {
        lblDesc.text = ""
        lblDescTitle.text = ""
        }
        lblExamName.text = self.detailDic["courseName"] as? String
        lblExamFees.text = String(describing:self.detailDic["fee"] as! NSNumber)
        lblRateCount.text = String(describing:self.detailDic["rateValue"] as! NSNumber) + "/5"
        lblReviewCount.text = String(describing:self.detailDic["noReviews"] as! NSNumber)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        //return UITableViewAutomaticDimension
        return 500
    }

    
    //MARK: IB Action
    
    @IBAction func btnCloseVC(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnReferClicked(_ sender: Any)
    {
        let text = "Course Name: \(lblExamName.text!)\n Course Rate: \(lblRateCount.text!)"
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
        activityViewController.excludedActivityTypes = [ UIActivityType.postToFacebook, UIActivityType.postToTwitter, UIActivityType.postToWeibo, UIActivityType.message, UIActivityType.print, UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.postToVimeo,UIActivityType.postToTencentWeibo,UIActivityType.airDrop]
        
        activityViewController.setValue("Your email Subject" , forKey: "subject")
        // activityViewController.setValue("You" , forKey: "to");
        activityViewController.popoverPresentationController?.sourceView = self.view // so that
        self.present(activityViewController, animated: true, completion: nil)
    
        
        
    }
    
    @IBAction func btnReviewClicked(_ sender: Any)
    {
       
        
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        if let courseId = self.detailDic["cenCourseID"]
        {
            vc.courseID = courseId as! NSNumber
            vc.navigateCourse = true
            vc.navigateShop = false
            vc.comeFromRateSuccess = false
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
   
    @IBAction func btnRateClicked(_ sender: Any)
    {
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
        vc.navigateCourse = true
        vc.navigateShop = false
        let parameters = ["CenCourseID":self.detailDic["cenCourseID"] as! NSNumber,"UserId":defaults.object(forKey: "userId")as! String,"PrimaryInfoID":defaults.object(forKey: "primaryInfoId") as! String]  as [String : Any]
        vc.parameter = parameters as NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)

    }
  
}
