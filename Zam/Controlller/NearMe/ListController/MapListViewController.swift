//
//  MapListViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/4/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import CoreLocation

class MapListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var btnmap: UIButton!
    @IBOutlet weak var tblView: UITableView!
    var dataArray = NSMutableArray()
    var Searchparameter = NSDictionary()
    var comFromSearch : Bool = false
  var currnetLatLong = CLLocation()
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
         ZamModel().buttonCornerWithThemeBorder(Button: btnmap, radius: 1, borderWidth: 0)
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let nibName = UINib(nibName: "MapListCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "MapListCell")
        tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 144.0;
          NotificationCenter.default.addObserver(self, selector: #selector(MapListViewController.CloseMapViewWhenSearchOpen(notification:)), name: Notification.Name("CloseMapViewWhenSearchOpen"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if comFromSearch == true{
        getSearchedData()
        }else {
          getdata()
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:- Table View Delegate & Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
       // return 170
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:MapListCell = tableView.dequeueReusableCell(withIdentifier: "MapListCell") as! MapListCell
        let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "photoPath") as! String
        cell.imgView.sd_setImage(with: NSURL(string: imageurl) as URL!)
        cell.btnCompare.tag = indexPath.row
        ZamModel().roundImageView(image: cell.imgView)
        cell.lblName.text = (dataArray[indexPath.row] as AnyObject).value(forKey: "centreName") as? String
      
        let data = ((dataArray[indexPath.row] as AnyObject).object(forKey: "exams") as? NSArray)?.mutableCopy() as! NSMutableArray
       
      //  print(data)
        var examName = String()
        for i in 0..<data.count{
            examName =  examName + ((data[i] as AnyObject).value(forKey: "examName") as? String)! + ","
        }
        cell.lblExamName.text =  String(examName.characters.dropLast())
        
        cell.lblRateCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber)) + "/5"
        
          ZamModel().setSmileImageThemeColor(btn: cell.btnRate, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber))
         cell.btnRate.addTarget(self, action: #selector(MapListViewController.RateClicked), for: .touchUpInside)
         cell.btnreview.addTarget(self, action: #selector(MapListViewController.ReviewClicked), for: .touchUpInside)
        cell.btnPhone.addTarget(self, action: #selector(MapListViewController.btnContactClicked), for: .touchUpInside)

        cell.lblReviewCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "noReviews") as! NSNumber))
        if (dataArray[indexPath.row] as AnyObject).value(forKey: "isFav") as! Bool == true{
            cell.btnCompare.setImage(UIImage(named:"compare50_red.png"), for: UIControlState.normal)
        }else {
         cell.btnCompare.setImage(UIImage(named:"compare50.png"), for: UIControlState.normal)
        }
        cell.btnCompare.addTarget(self, action: #selector(MapListViewController.addTocompareClicked), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let centreID = Int((dataArray[indexPath.row] as AnyObject).value(forKey: "centreID") as! NSNumber)
        
        let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
        vc.CentreID =  String(describing: centreID)
        //  print(response?.object(forKey: "data") as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: false)
        //getDetail(centerId: String(describing: centreID))
    }

    @IBAction func btnCloseView(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("CloseMapView"), object: nil)
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    func addTocompareClicked(sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
         let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID)]
        let cell = tblView.cellForRow(at: indexPath!) as! MapListCell
        if  cell.btnCompare.currentImage!.isEqual(UIImage(named: "compare50.png"))
        {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().addToCompare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                 self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true{
                  //  self.getdata()
                    let dic:NSDictionary = self.dataArray[(indexPath?.row)!] as! NSDictionary
                    let value = dic.mutableCopy() as! NSMutableDictionary
                    value.setValue(true, forKey: "isFav")
                    print(value)
                    self.dataArray.replaceObject(at: (indexPath?.row)!, with: value)
                    self.tblView.reloadData()
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }else {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().removeFromComare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "success") as! Bool  == true{
                  //  self.getdata()
                    let dic:NSDictionary = self.dataArray[(indexPath?.row)!] as! NSDictionary
                    let value = dic.mutableCopy() as! NSMutableDictionary
                    value.setValue(false, forKey: "isFav")
                    self.dataArray.replaceObject(at: (indexPath?.row)!, with: value)
                    print(value)
                    self.tblView.reloadData()
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }
    }
    func RateClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID),"userID":UserDefaults.standard.object(forKey: "userId") as! String,]
        let vc = CenterRateViewController(nibName: "CenterRateViewController", bundle: nil)
        appDelegate.comeFromCenterRate = "nearme"
        vc.parameter = parameters as NSDictionary
        vc.centerID = String(describing: centreID)
         self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
        /*
        self.view.makeToastActivity(message: "Loading")
            AppEngine().removeFromComare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                self.view.hideToastActivity()
                if response?.object(forKey: "success") as! Bool  == true{
                                   } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
 */
    }
    func btnContactClicked(sender:UIButton){
        let controller:ContactAlertViewController = ContactAlertViewController(nibName: "ContactAlertViewController", bundle: nil)
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        print(dataArray[(indexPath?.row)!])
        controller.phoneNo = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "mobileNo") as! String
        controller.email = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "emailID") as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    func ReviewClicked(sender:UIButton){
        
    let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
    let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
    let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
    let vc = CenterReviewViewController(nibName: "CenterReviewViewController", bundle: nil)
    vc.cenetrID = String(describing: centreID)
    appDelegate.comeFromCenterReview = "nearme"
    self.tabBarController?.tabBar.isHidden = true
    self.navigationController?.pushViewController(vc, animated: false)
    }
    func getdata(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
       let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"latitude" :  (currnetLatLong.coordinate.latitude) , "longitude" :  (currnetLatLong.coordinate.longitude)] as [String : Any]
       self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCenterList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           
            if response == nil{
                 self.view.hideToastActivity(view: self.view)
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblView.reloadData()
                self.view.hideToastActivity(view: self.view)
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
                 self.view.hideToastActivity(view: self.view)
            }
        }
    }
    func getSearchedData(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getSearchCenterByKeyword(parameter: Searchparameter as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.tblView.reloadData()
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    
    func CloseMapViewWhenSearchOpen(notification: Notification) {
       
    NotificationCenter.default.post(name: Notification.Name("CloseMapView"), object: nil)
    self.willMove(toParentViewController: nil)
    self.view.removeFromSuperview()
    self.removeFromParentViewController()
    }
    
    
 
 }
