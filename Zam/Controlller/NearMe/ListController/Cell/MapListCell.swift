//
//  MapListCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/4/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class MapListCell: UITableViewCell {

    @IBOutlet weak var btnreview: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var lblExamName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var btnCompare: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
