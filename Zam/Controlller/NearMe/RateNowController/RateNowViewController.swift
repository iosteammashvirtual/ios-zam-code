//
//  RateNowViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/5/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class RateNowViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var btnFinish: UIButton!
    var comeFromMyRate:Bool = false
    @IBOutlet weak var tblView: UITableView!
    var CountArray = NSMutableArray()
    var SelecctedRateArrayCount = NSMutableArray()
    var parameter = NSDictionary()
    var cenetrID = String()
    var comeFromReviewSuccess:Bool = false
        var dataArray = NSMutableArray()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let nibName = UINib(nibName: "RateNowTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "RateNowTableViewCell")
        
         NotificationCenter.default.addObserver(self, selector: #selector(RateNowViewController.PopYesClicked(notification:)), name: Notification.Name("RateNowYesClicked"), object: nil)
        
        // Do any additional setup after loading the view.
        
        ZamModel().buttonCorner(Button: btnFinish, radius: Int(5))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        getdata()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:RateNowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RateNowTableViewCell") as! RateNowTableViewCell
       cell.lblTitle.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "cTitle") as? String
    
         cell.btn1.addTarget(self, action: #selector(RateNowViewController.RateClicked), for: .touchUpInside)
        cell.btn2.addTarget(self, action: #selector(RateNowViewController.RateClicked), for: .touchUpInside)
        cell.btn3.addTarget(self, action: #selector(RateNowViewController.RateClicked), for: .touchUpInside)
        cell.btn4.addTarget(self, action: #selector(RateNowViewController.RateClicked), for: .touchUpInside)
        cell.btn5.addTarget(self, action: #selector(RateNowViewController.RateClicked), for: .touchUpInside)
        
        
        if SelecctedRateArrayCount[indexPath.row] as! NSNumber == 0{
          cell.btn1.setImage(UIImage(named:"happy50B.png"), for: .normal)
          cell.btn2.setImage(UIImage(named:"happy50B.png"), for: .normal)
          cell.btn3.setImage(UIImage(named:"happy50B.png"), for: .normal)
          cell.btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
          cell.btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if SelecctedRateArrayCount[indexPath.row] as! NSNumber == 1{
            
            cell.btn1.setImage(UIImage(named:"angry50.png"), for: .normal)
            cell.btn2.setImage(UIImage(named:"happy50B.png"), for: .normal)
            cell.btn3.setImage(UIImage(named:"happy50B.png"), for: .normal)
            cell.btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            cell.btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if SelecctedRateArrayCount[indexPath.row] as! NSNumber == 2{
            
            cell.btn1.setImage(UIImage(named:"sad50.png"), for: .normal)
            cell.btn2.setImage(UIImage(named:"sad50.png"), for: .normal)
            cell.btn3.setImage(UIImage(named:"happy50B.png"), for: .normal)
            cell.btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            cell.btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)

        
        }else if SelecctedRateArrayCount[indexPath.row] as! NSNumber == 3{
            
            cell.btn1.setImage(UIImage(named:"natural50.png"), for: .normal)
            cell.btn2.setImage(UIImage(named:"natural50.png"), for: .normal)
            cell.btn3.setImage(UIImage(named:"natural50.png"), for: .normal)
            cell.btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            cell.btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if SelecctedRateArrayCount[indexPath.row] as! NSNumber == 4{
            cell.btn1.setImage(UIImage(named:"happy50.png"), for: .normal)
            cell.btn2.setImage(UIImage(named:"happy50.png"), for: .normal)
            cell.btn3.setImage(UIImage(named:"happy50.png"), for: .normal)
            cell.btn4.setImage(UIImage(named:"happy50.png"), for: .normal)
            cell.btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if SelecctedRateArrayCount[indexPath.row] as! NSNumber == 5{
            cell.btn1.setImage(UIImage(named:"awesome50.png"), for: .normal)
            cell.btn2.setImage(UIImage(named:"awesome50.png"), for: .normal)
            cell.btn3.setImage(UIImage(named:"awesome50.png"), for: .normal)
            cell.btn4.setImage(UIImage(named:"awesome50.png"), for: .normal)
            cell.btn5.setImage(UIImage(named:"awesome50.png"), for: .normal)
            
        }
        
        // cell.lblName.text = dataArray[indexPath.row]
        return cell
    }
    func getdata(){
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCenterratingCriteria(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
           self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                debugPrint(response)
              self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                if self.comeFromMyRate == true{
                    for i in 0..<self.dataArray.count{
                        let criteriaOption =  (self.dataArray.object(at: i) as AnyObject).object(forKey: "criteriaOption") as? NSArray
                        self.SelecctedRateArrayCount.add((criteriaOption?[0] as AnyObject).value(forKey:"optionValue") as! NSNumber)
                        self.btnFinish.isHidden = false
                    }
                }else {
                    for i in 0..<self.dataArray.count{
                        self.SelecctedRateArrayCount.add(0)
                         self.btnFinish.isHidden = true
                    }
                }
                
               
                
            //  self.dataArray = (data.object(forKey: "criteriaRates") as! NSArray).mutableCopy() as! NSMutableArray
               
             self.tblView.reloadData()
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    func RateClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        
        if !(CountArray.contains(indexPath?.row as Any)){
         CountArray.add(indexPath?.row as Any)
        }
        SelecctedRateArrayCount.replaceObject(at: (indexPath?.row)!, with: sender.tag)
        tblView.reloadData()
        if CountArray.count == dataArray.count{
        btnFinish.isHidden = false
        }
    }
    @IBAction func btnFinishClicked(_ sender: Any) {
        
        let criterialArray = NSMutableArray()
        for i in 0..<self.dataArray.count{
            
            var dataStr = String()
         let criteriaOption =  (dataArray.object(at: i) as AnyObject).object(forKey: "criteriaOption") as? NSArray
            
            dataStr = dataStr + String(describing: ((dataArray.object(at: i) as AnyObject).value(forKey: "criteriaID") as! NSNumber)) + ","
            
             dataStr = dataStr + String(describing: SelecctedRateArrayCount[i] as! NSNumber) + ","
            
             dataStr = dataStr + String(describing: ((criteriaOption?[0] as AnyObject).value(forKey: "criteriaOptID") as! NSNumber)) + ","
            
//            dic.add((dataArray.object(at: i) as AnyObject).value(forKey: "criteriaID") as! NSNumber)
//            dic.add(SelecctedRateArrayCount[i])
//            dic.add((criteriaOption?[0] as AnyObject).value(forKey: "criteriaOptID") as! NSNumber)
            
//            dic.setValue((dataArray.object(at: i) as AnyObject).value(forKey: "criteriaID") as? NSNumber, forKey: "CriteriaID")
//            dic.setValue(SelecctedRateArrayCount[i], forKey: "rate")
//             dic.setValue((criteriaOption?[0] as AnyObject).value(forKey: "criteriaOptID") as! NSNumber, forKey: "criteriaOptID")
            criterialArray.add(dataStr)
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : cenetrID,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"CriteriaAnsNew[]":criterialArray] as [String : Any]
        print(parameters as! NSDictionary)
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().addCentrerating(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "status") as! Bool  == true{
                // self.getdata()
                
                if self.comeFromReviewSuccess{
                    
                    let vc = CentreRateReviewSuccessViewController(nibName: "CentreRateReviewSuccessViewController", bundle: nil)
                   vc.comeFromReviewSuccess = self.comeFromReviewSuccess
                    self.navigationController?.pushViewController(vc, animated: false)
                
                }else {
                    let vc = RateRecommendViewController(nibName: "RateRecommendViewController", bundle: nil)
                    vc.cenetrID = self.cenetrID
                    vc.rateID = String(describing: response?.object(forKey: "rateID") as! NSNumber)
                    self.navigationController?.pushViewController(vc, animated: false)
                }
                
                
              
                
                            
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        let controller: AlertConfirmViewController =  AlertConfirmViewController(nibName: "AlertConfirmViewController", bundle: nil)
        controller.message = cancelRate
        controller.comFromCenterRate = true
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
       // self.navigationController?.popViewController(animated: true)
    }
    func PopYesClicked(notification : Notification){
        if self.comeFromReviewSuccess{
            if  appDelegate.comeFromCenterReview == "nearme"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterReview == "compare"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterReview == "centre"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterReview == "myreview"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyReviewViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
        }else {
        self.navigationController?.popViewController(animated: true)
        }
        
        
        
      
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
