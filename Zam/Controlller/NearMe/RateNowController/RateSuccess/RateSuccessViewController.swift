//
//  RateSuccessViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/24/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class RateSuccessViewController: UIViewController {

    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    var cenetrID = String()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
ZamModel().buttonCorner(Button: btnNo, radius: 5)
   ZamModel().buttonCorner(Button: btnYes, radius: 5)
        // Do any additional setup after loading the view.
    }

    @IBAction func btnNoClicked(_ sender: Any) {
        
        if  appDelegate.comeFromCenterRate == "nearme"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
        }else if appDelegate.comeFromCenterRate == "compare"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
        }else if appDelegate.comeFromCenterRate == "centre"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
        }else if appDelegate.comeFromCenterRate == "myrate"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyRateViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
        
        }
        
    }
    @IBAction func btnYesClicked(_ sender: Any) {
        
        let vc = CenterReviewViewController(nibName: "CenterReviewViewController", bundle: nil)
        vc.cenetrID = cenetrID
        vc.comeFromRateSuccess = true
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
