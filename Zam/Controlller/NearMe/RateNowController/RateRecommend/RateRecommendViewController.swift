//
//  RateRecommendViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/24/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class RateRecommendViewController: UIViewController {

    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    var rateID = String()
    var parameters = NSDictionary()
    var cenetrID = String()
    override func viewDidLoad() {
        super.viewDidLoad()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        ZamModel().buttonCorner(Button: btnYes, radius: 5)
        ZamModel().buttonCorner(Button: btnNo, radius: 5)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnNoClickked(_ sender: Any) {
        
    parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : cenetrID,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"RateID":rateID,"IsRecomand": "false"] as [String : Any] as NSDictionary
        callapi()
    }
    @IBAction func btnYesClicked(_ sender: Any) {
        
     parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : cenetrID,"RateID":rateID,"IsRecomand": "true"] as [String : Any] as NSDictionary
        callapi()
    }
    
    func callapi(){
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().setUserRecommenndation(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
           self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                let vc = RateSuccessViewController(nibName: "RateSuccessViewController", bundle: nil)
                vc.cenetrID = self.cenetrID
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
