//
//  CentreRateReviewSuccessViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CentreRateReviewSuccessViewController: UIViewController {

    @IBOutlet weak var lblTopTitle: UILabel!
    @IBOutlet weak var btngreta: UIButton!
    @IBOutlet weak var lblReviewText: UILabel!
    var comeFromReviewSuccess :Bool = false
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        
         ZamModel().buttonCornerWithThemeBorder(Button: btngreta, radius: 5, borderWidth: 1)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if comeFromReviewSuccess {
            lblTopTitle.text = "Rate"
            lblReviewText.text = "Your rating has successfully been submitted"
           
               }else {
            lblTopTitle.text = "Review"
            lblReviewText.text = "Your review will be published after approval"
           
        }
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btngreatClicked(_ sender: Any) {
        
        if comeFromReviewSuccess{
                if  appDelegate.comeFromCenterReview == "nearme"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                }else if appDelegate.comeFromCenterReview == "compare"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                }else if appDelegate.comeFromCenterReview == "centre"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                }else if appDelegate.comeFromCenterReview == "myreview"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyReviewViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            self.tabBarController?.tabBar.isHidden = false
        }
        else {
    
            if  appDelegate.comeFromCenterRate == "nearme"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "compare"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "centre"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "myrate"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyRateViewController }.first!
                 self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            
        }
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
