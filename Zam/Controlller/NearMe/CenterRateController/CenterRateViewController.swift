//
//  CenterRateViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/5/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CenterRateViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblRateCountNil: UILabel!
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var lblNoOfRate: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblOverAllRate: UILabel!
    @IBOutlet weak var btnOverAllRate: UIButton!
    var comeFromReviewSuccess:Bool = false
       var ParameterNil:Bool = false
    var parameter = NSDictionary()
    var centerID = String()
    var dataArray = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = UINib(nibName: "CentreRateTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "CentreRateTableViewCell")
        // Do any additional setup after loading the view.
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewWillAppear(_ animated: Bool) {
        getdata()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
       // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:CentreRateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CentreRateTableViewCell") as! CentreRateTableViewCell
        cell.lblName.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "criteriaName") as? String
         cell.lblRateCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "criteriaRateValue") as! NSNumber)) + "/5"
         ZamModel().setSmileImageThemeColor(btn: cell.btnImageView, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "criteriaRateValue") as! NSNumber))

        return cell
    }
    
    
    func getdata(){
         self.view.makeToastActivity(message: "Loading", view: self.view)
        
        if parameter.count ==  0 {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            centerID = appDelegate.centerIdForMyRate
           parameter = appDelegate.parameterForMyRate
            ParameterNil = true
        }
        
        AppEngine().getCenterrateData(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
           self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                 self.topView.isHidden = false
                self.lblRateCountNil.text = ""
                let data = response?.object(forKey: "data") as! NSDictionary
               self.dataArray = (data.object(forKey: "criteriaRates") as! NSArray).mutableCopy() as! NSMutableArray
                 ZamModel().setSmileImageThemeColor(btn: self.btnOverAllRate, rateCount: Int((data).value(forKey: "overAllRateValue") as! NSNumber))
                self.lblOverAllRate.text = String(describing: (data).value(forKey: "overAllRateValue") as! NSNumber) + "/5"
                self.lblNoOfRate.text = String(describing: (data).value(forKey: "noOfRate") as! NSNumber)
                self.tblView.reloadData()
                    } else {
                self.lblRateCountNil.text = response?.object(forKey: "message") as! String
                self.topView.isHidden = true
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }

    @IBAction func btnrateNowClicked(_ sender: Any) {
        let vc = RateNowViewController(nibName: "RateNowViewController", bundle: nil)
       let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : centerID,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"RateingID":"0"]
        vc.parameter = parameters as NSDictionary
         vc.cenetrID = centerID
        vc.comeFromReviewSuccess = comeFromReviewSuccess
        self.navigationController?.pushViewController(vc, animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btnBackClicked(_ sender: Any) {
        
        if ParameterNil ==  true {
            let vc = NearViewController(nibName: "NearViewController", bundle: nil)
            // self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.pushViewController(vc, animated: false)

        }else{
            
           if comeFromReviewSuccess == true{
            if  appDelegate.comeFromCenterReview == "nearme"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterReview == "compare"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterReview == "centre"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if appDelegate.comeFromCenterReview == "myreview"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyReviewViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            
            
            
            }else {
            
            if  appDelegate.comeFromCenterRate == "nearme"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "compare"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "centre"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if appDelegate.comeFromCenterRate == "myrate"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyRateViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            
            }

        }
        
          }
}
