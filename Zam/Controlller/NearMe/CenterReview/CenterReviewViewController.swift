//
//  CenterReviewViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/24/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CenterReviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {

    @IBOutlet weak var lblReviewNil: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblCharacterRemaining: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var tblView: UITableView!
    var textValue = String()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = NSMutableArray()
    var cenetrID = String()
    var comeFromRateSuccess:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
      //  IQKeyboardManager.sharedManager().enable = false
//        NotificationCenter.default.addObserver(self, selector: #selector(CenterReviewViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(CenterReviewViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        let nibName = UINib(nibName: "CenterReviewTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "CenterReviewTableViewCell")
        // Do any additional setup after loading the view.
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 60;
        txtView.delegate = self
        ZamModel().textViewCorner(txtview: txtView, radius: 10)
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewWillAppear(_ animated: Bool) {
        getdata()
        txtView.text = textValue
       lblCharacterRemaining.text = "Characters remaining : " +  String( 200 - textValue.characters.count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    // MARK: - table View Delegate & Datasource
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return dataArray.count
     }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return UITableViewAutomaticDimension
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     // create a new cell if needed or reuse an old one
        
     let cell:CenterReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CenterReviewTableViewCell") as! CenterReviewTableViewCell
    ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImg, radius: Int(cell.btnImg.frame.size.height / 2), borderWidth: 2)
    cell.lblName.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewBy") as? String
    cell.lblDate.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewDate") as? String
    cell.lblDesc.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewComment") as? String
        let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "photoPath") as! String
    cell.btnImg.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)     // cell.lblName.text = dataArray[indexPath.row]
     return cell
     }

    func getdata(){
        
   self.view.makeToastActivity(message: "Loading", view: self.view)
       let  parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : cenetrID,"reviewID":"0"] as [String : Any] as NSDictionary
        AppEngine().getCenterReviewList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
               // debugPrint(response)
               
                if response?.object(forKey: "data") != nil  {
                    self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblView.reloadData()
                    self.lblReviewNil.text = ""
                }
                
      
            } else {
                self.lblReviewNil.text = response?.object(forKey: "message") as! String
            //    self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    //MARK:- TextView Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newLength = textView.text.characters.count + text.characters.count - range.length
        if newLength <= 200{
            lblCharacterRemaining.text = "Characters remaining : " +  String( 200 - newLength)
            
            return true
        }else {
            return false
        }
    
    
    }
    
    @IBAction func btnPostClicked(_ sender: Any) {
        
        txtView.resignFirstResponder()
        if ZamModel().condenseWhitespace(str: txtView.text) == ""{
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = ReviewNil
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
        return
        }
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let  parameters = ["userId" : UserDefaults.standard.object(forKey: "userId") as! String,"CentreID" : cenetrID,"ReviewType":"Centre","ReviewComment":txtView.text!] as [String : Any] as NSDictionary
        
        AppEngine().saveCenterReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            
            if response?.object(forKey: "success") as! Bool  == true{
                
                if self.comeFromRateSuccess{
                    let vc = CentreRateReviewSuccessViewController(nibName: "CentreRateReviewSuccessViewController", bundle: nil)
                  //  vc.comeFromRateSuccess = self.comeFromRateSuccess
                   // self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(vc, animated: false)
                }else {
                    let vc = CenterReviewSuccessViewController(nibName: "CenterReviewSuccessViewController", bundle: nil)
                    vc.centreID = String(describing: self.cenetrID)
                   // self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(vc, animated: false)
                }
               
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    
//    func keyboardWillShow(notification: NSNotification) {
//        var userInfo = notification.userInfo!
//        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
//        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
//                var contentInset:UIEdgeInsets = self.scrollView.contentInset
//                contentInset.bottom = keyboardFrame.size.height
//    
//    }
//    func keyboardWillHide(notification: NSNotification) {
//        tblView.isHidden = true
//                let contentInset:UIEdgeInsets = .zero
//                self.scrollView.contentInset = contentInset
//    }
    // MARK: - TableViewDelegate
    

    @IBAction func btnBackClicked(_ sender: Any) {
        if comeFromRateSuccess{
            if  appDelegate.comeFromCenterRate == "nearme"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "compare"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "centre"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }else if appDelegate.comeFromCenterRate == "myrate"{
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyRateViewController }.first!
                self.navigationController!.popToViewController(dashboardVC, animated: true)
                
            }
        }else {
            self.tabBarController?.tabBar.isHidden = false
         self.navigationController?.popViewController(animated: true)
        }
       
    }
}
