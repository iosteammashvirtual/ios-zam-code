//
//  CenterReviewSuccessViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CenterReviewSuccessViewController: UIViewController {

    @IBOutlet weak var btnYes: UIButton!
    var centreID = String()
    @IBOutlet weak var btnNo: UIButton!
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
        ZamModel().buttonCorner(Button: btnNo, radius: 5)
        ZamModel().buttonCorner(Button: btnYes, radius: 5)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnNoClicked(_ sender: Any) {
        
        if  appDelegate.comeFromCenterReview == "nearme"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
              self.tabBarController?.tabBar.isHidden = false
        }else if appDelegate.comeFromCenterReview == "compare"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CompareViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
              self.tabBarController?.tabBar.isHidden = false
        }else if appDelegate.comeFromCenterReview == "centre"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterDetailViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
              self.tabBarController?.tabBar.isHidden = false
        }
        else if appDelegate.comeFromCenterReview == "myreview"{
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is MyReviewViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
             // self.tabBarController?.tabBar.isHidden = false
        }
       
      
       
        
//        let vc = CompareViewController(nibName: "CompareViewController", bundle: nil)
//       
//        self.navigationController?.pushViewController(vc, animated: false)
      
        
        
    }
    @IBAction func btnYesClicked(_ sender: Any) {
  
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID),"userID":UserDefaults.standard.object(forKey: "userId") as! String,]
        
        let vc = CenterRateViewController(nibName: "CenterRateViewController", bundle: nil)
        vc.parameter = parameters as NSDictionary
        vc.comeFromReviewSuccess = true
        vc.centerID = String(describing: centreID)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
