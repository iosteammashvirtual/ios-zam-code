//
//  CenterDetailViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/5/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import MessageUI
import CoreLocation

class CenterDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource ,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate{
    @IBOutlet weak var lblAboutTitle: UILabel!

    @IBOutlet weak var lblCenterVideoTitle: UILabel!
    @IBOutlet weak var imgLocationIcon: UIImageView!
    @IBOutlet weak var lblCentreImageTitle: UILabel!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnAddToCompare: UIButton!
    @IBOutlet weak var btnProfileBackGroumd: UIButton!
    @IBOutlet weak var lblUSPSTitle: UILabel!
    @IBOutlet weak var btnViewMore: UIButton!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var btnViewMoreDescription: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var colllView: UICollectionView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var btnPhoneNo: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collViewSupportedExamHeight: NSLayoutConstraint!
    @IBOutlet weak var VideoViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblUsps: UILabel!
    @IBOutlet weak var collViewSupportedExam: UICollectionView!
   
    
    @IBOutlet weak var aboutTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var aboutTopConstrainr: NSLayoutConstraint!
    
    @IBOutlet weak var uspsTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var uspsTopConstraint: NSLayoutConstraint!
    
    var comparedClicked:Bool = false
    
    var examsArray = NSMutableArray()
    var CentreID = String()
    
    var dataDic = NSDictionary()
    var workingDays = NSMutableArray()
    var centerTrainer = NSMutableArray()
    var centerImages = NSMutableArray()
    var centerVideo = String()
    var model = ZamModel()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var moviePlayer:MPMoviePlayerController!
    var comeFromComparision:Bool = false
    var comeFromCompare:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.doneButtonClick), name: NSNotification.Name(rawValue: "MPMoviePlayerDidExitFullscreenNotification"), object: nil)

        
        
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        ZamModel().buttonCornerWithThemeBorder(Button: btnProfileBackGroumd, radius:Int(btnProfileBackGroumd.frame.size.width/2.0),borderWidth: 6) 
        ZamModel().buttonCorner(Button: btnProfileImage, radius: Int(btnProfileImage.frame.size.height/2))
        
              
        
        
        let nibName = UINib(nibName: "WorkingHoursTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "WorkingHoursTableViewCell")
        
        let nibName1 = UINib(nibName: "WorkingHoursHeaderTableCell", bundle:nil)
        tblView.register(nibName1, forCellReuseIdentifier: "WorkingHoursHeaderTableCell")
        
        let nibName2 = UINib(nibName: "CenterTrainerTableCell", bundle:nil)
        tblView.register(nibName2, forCellReuseIdentifier: "CenterTrainerTableCell")
        
        
        let nibName3 = UINib(nibName: "CenterimagesCollectionCell", bundle: nil)
        colllView.register(nibName3, forCellWithReuseIdentifier: "CenterimagesCollectionCell")

      
        
        let nib = UINib(nibName: "CenterimagesCollectionCell", bundle: nil)
        colllView.register(nib, forCellWithReuseIdentifier: "CenterimagesCollectionCell")
        colllView.backgroundColor  = UIColor.clear
        
        
        
        let nib1 = UINib(nibName: "ExamsCollectionViewCell", bundle: nil)
        collViewSupportedExam.register(nib1, forCellWithReuseIdentifier: "ExamsCollectionViewCell")
        collViewSupportedExam.backgroundColor  = UIColor.clear
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
    {
         comparedClicked = false
        getCenterDetail(centerId:CentreID )
         self.tabBarController?.tabBar.isHidden = true
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnCloseClicked(_ sender: Any) {
        
//        if comeFromComparision == true {
//         NotificationCenter.default.post(name: Notification.Name("CenterComparisionOpenFromCenterDetail"), object: nil)
//           self.navigationController?.viewControllers.removeLast()
//        }else {
//          self.navigationController?.popViewController(animated: true)
//        }
//    }
        
        if comeFromComparision == true {
            NotificationCenter.default.post(name: Notification.Name("CenterComparisionOpenFromCenterDetail"), object: nil)
            self.navigationController?.viewControllers.removeLast()
        }else if comeFromCompare == true{
            if comparedClicked == true{
                appDelegate.addToCompareClicked = true
            }else {
                appDelegate.addToCompareClicked = false
            }
            self.navigationController?.popViewController(animated: false)
        }else {
            if comparedClicked == true{
                appDelegate.addToCompareClicked = true
            }else {
                appDelegate.addToCompareClicked = false
            }
            self.navigationController?.popViewController(animated: false)
        }
   
        
    }
    
    // MARK: - TableViewDelegate & DataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            if workingDays.count > 0{
             return workingDays.count + 1
            }else {
             return 0
            }
        }else {
            
            if centerTrainer.count > 0{
                return centerTrainer.count + 1
            }else {
                return 0
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if indexPath.section == 0 {
        return 30
        }else {
        return 80
        }
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:WorkingHoursTableViewCell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursTableViewCell") as! WorkingHoursTableViewCell
        
        let cellHeader:WorkingHoursHeaderTableCell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursHeaderTableCell") as! WorkingHoursHeaderTableCell
        
          let cellTrainer:CenterTrainerTableCell = tableView.dequeueReusableCell(withIdentifier: "CenterTrainerTableCell") as! CenterTrainerTableCell
       
        if indexPath.section == 0
        {
            if indexPath.row == 0{
              cellHeader.lblTitle.text = " Working Hours"
            return cellHeader
            }else {
            
             cell.lblDaysName.text = (workingDays[indexPath.row - 1] as AnyObject).value(forKey: "workingDay") as? String
           
                let str = ":      " + String((workingDays[indexPath.row - 1] as AnyObject).value(forKey: "startTime") as! String) + " - " + String((workingDays[indexPath.row - 1] as AnyObject).value(forKey: "endTime") as! String)
             cell.lblTime.text = str
             return cell
            }
          //  startTime  endTime
        }else {
            if indexPath.row == 0{
                 cellHeader.lblTitle.text = " Centre Trainers"
                return cellHeader
            }else {
                
                ZamModel().buttonCornerWithThemeBorder(Button: cellTrainer.btnImageView, radius: Int(cellTrainer.btnImageView.frame.height/2), borderWidth: 2)
                cellTrainer.lblName.text = (centerTrainer[indexPath.row - 1 ] as AnyObject).value(forKey: "trainerName") as? String
                let imageurl = (centerTrainer[indexPath.row - 1 ] as AnyObject).value(forKey: "tPhotoPath") as! String
                cellTrainer.btnImageView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
                return cellTrainer
        
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            
            if indexPath.row != 0 {
                let trainerID =  (centerTrainer[indexPath.row - 1 ] as AnyObject).value(forKey: "trainerID") as! NSNumber
                // getTrainerDetail(trainerId: String(describing: trainerID))
                let vc = CenterTrainerProfileViewController(nibName: "CenterTrainerProfileViewController", bundle: nil)
                // vc.dataDic = data
                vc.trainerId = String(describing: trainerID)
                self.navigationController?.pushViewController(vc, animated: false)
            }

            
        }
    }
    
    

    @IBAction func btnPlayVideoClicked(_ sender: Any) {
        
        if  dataDic.object(forKey: "isEmbeded") as? Bool == true{
            let vc = YoutubeVideoViewController(nibName: "YoutubeVideoViewController", bundle: nil)
              let url =  centerVideo.components(separatedBy: "=")
            vc.embedurl = url.last!
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else
        {
            
            let videoURL = NSURL(string: centerVideo)
            let player = AVPlayer(url: videoURL! as URL)
            let playerViewController = LandscapeAVPlayerController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
            
            
            
            
//            let url:NSURL = NSURL(string: centerVideo)!
//            moviePlayer = MPMoviePlayerController(contentURL: url as URL!)
//            self.view.addSubview(moviePlayer.view)
//            moviePlayer.isFullscreen = true
//            appDelegate.isFullScreen = true
            
        }
        
    }

    // MARK: - CollectionView Delegate & DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100
        {
             return examsArray.count
        }else
        {
         return centerImages.count
        }
        
       
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if collectionView.tag == 100 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExamsCollectionViewCell", for: indexPath as IndexPath) as! ExamsCollectionViewCell
            
            let imageurl = (examsArray[indexPath.row] as AnyObject).value(forKey: "examLogo") as! String
//            ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImageView, radius: Int(5),borderWidth: 1)
             ZamModel().CollCellCorner(collcell: cell, radius: 5)
            cell.btnImageView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
            cell.btnImageView.tag = indexPath.row
              cell.btnImageView.addTarget(self, action: #selector(CenterDetailViewController.btnImageViewClicked), for: .touchUpInside)
          return cell
        }else {
            
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CenterimagesCollectionCell", for: indexPath as IndexPath) as! CenterimagesCollectionCell
            
            let imageurl = (centerImages[indexPath.row] as AnyObject).value(forKey: "imagePath") as! String
            cell.btnImageView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
             cell.btnImageView.tag = indexPath.row
            cell.btnImageView.addTarget(self, action: #selector(CenterDetailViewController.btnImageViewClickedImages), for: .touchUpInside)
              return cell
        }
        
      
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 100
        {
          return CGSize(width:collectionView.frame.size.width / 2 - 10, height:60)
        }
        else
        {
          return CGSize(width:collectionView.frame.size.width / 2 - 2, height:140)
        }
      
    }
    
    
    @IBAction func btnDexcViewMore(_ sender: Any) {
    btnViewMoreDescription.isHidden = true
   lblDescription.numberOfLines = 0
    }

    @IBAction func btnViewMoreClicked(_ sender: Any) {
        btnViewMore.isHidden = true
        if centerTrainer.count > 0 && workingDays.count > 0 {
            
                tblViewHeight.constant = (CGFloat((centerTrainer.count + 1) * 80)) + (CGFloat((workingDays.count + 1) * 30))
        }else if centerTrainer.count > 0{
                tblViewHeight.constant =  (CGFloat((centerTrainer.count + 1) * 80))
        }
    }
    
    
    func getTrainerDetail(trainerId :String){
//        if ZamModel().isConnectedToNetwork() == false {
//            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
//            controller.message = networkError
//            addChildViewController(controller)
//            controller.view.frame = view.bounds
//            view.addSubview(controller.view)
//            controller.didMove(toParentViewController: self)
//            return
//        }
//        let parameters = ["TrainerID" : trainerId]
//        self.view.makeToastActivity(message: "Loading")
//        AppEngine().getTrainerDetail(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
//            self.view.hideToastActivity()
//            if response == nil{
//                self.view.makeToast(message: ServerError)
//                return
//            }
//            if response?.object(forKey: "success") as! Bool  == true{
//                let data = response?.object(forKey: "data") as! NSDictionary
//                print(data)
//                let vc = CenterTrainerProfileViewController(nibName: "CenterTrainerProfileViewController", bundle: nil)
//                vc.dataDic = data
//                vc.trainerId = trainerId
//                self.navigationController?.pushViewController(vc, animated: false)
//                
//                  } else {
//                self.view.makeToast(message: response?.object(forKey: "message") as! String)
//            }
//        }
    }
    
    
    
//    func getAssociatedExamList(examId :Int,name :String){
//        if ZamModel().isConnectedToNetwork() == false {
//            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
//            controller.message = networkError
//            addChildViewController(controller)
//            controller.view.frame = view.bounds
//            view.addSubview(controller.view)
//            controller.didMove(toParentViewController: self)
//            return
//        }
//         let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : CentreID ,"ExamID":examId] as [String : Any]
//       self.view.makeToastActivity(message: "Loading", view: self.view)
//        AppEngine().getCenterCoursesList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
//            self.view.hideToastActivity(view: self.view)
//            if response == nil{
//                self.view.makeToast(message: ServerError)
//                return
//            }
//            if response?.object(forKey: "success") as! Bool  == true{
//                let data = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
//                print(data)
//                let vc = SupportedExamsViewController(nibName: "SupportedExamsViewController", bundle: nil)
//                vc.dataArray = data
//                vc.nameTitle = name
//                vc.rateValue = String(describing: response?.object(forKey: "overallRate") as! NSNumber)
//                self.navigationController?.pushViewController(vc, animated: false)
//                
//            } else {
//               
//                self.view.makeToast(message: response?.object(forKey: "message") as! String)
//                let vc = SupportedExamsViewController(nibName: "SupportedExamsViewController", bundle: nil)
//                vc.dataArray = NSMutableArray()
//                vc.nameTitle = name
//                vc.message = response?.object(forKey: "message") as! String
//                  self.navigationController?.pushViewController(vc, animated: false)
//             //   vc.rateValue = String(describing: self.dataDic.object(forKey: "ratevalue") as! NSNumber)
//            }
//        }
//    }
    func btnImageViewClicked (sender :UIButton){
        
//        let examID = (examsArray[sender.tag] as AnyObject).value(forKey: "examID") as! NSNumber
//        let examName = (examsArray[sender.tag] as AnyObject).value(forKey: "examName") as! String
//        getAssociatedExamList(examId: Int(examID),name: examName)
        let examID = (examsArray[sender.tag] as AnyObject).value(forKey: "examID") as! NSNumber
        let examName = (examsArray[sender.tag] as AnyObject).value(forKey: "examName") as! String
        let vc = SupportedExamsViewController(nibName: "SupportedExamsViewController", bundle: nil)
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : CentreID ,"ExamID":examID] as [String : Any]
        vc.parameter = parameters as NSDictionary
        vc.nameTitle = examName
        self.navigationController?.pushViewController(vc, animated: false)
    
    }
    func btnImageViewClickedImages (sender :UIButton){
        
        let imageurl = (centerImages[sender.tag] as AnyObject).value(forKey: "imagePath") as! String
        let controller:ImagePopViewController = ImagePopViewController(nibName: "ImagePopViewController", bundle: nil)
        controller.imageurl = imageurl
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
        
        
    }
    
    
    
    func addTocompareClicked(){
        comparedClicked  = true
        
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: CentreID)]
       
        if  btnAddToCompare.currentImage!.isEqual(UIImage(named: "compare50Small.png"))
        {
            self.view.makeToastActivity(message: "Loading...", view: self.view)
            AppEngine().addToCompare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil{
                    
                    self.view.makeToast(message: ServerError)
                    return
                }
               self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "success") as! Bool  == true{
                   
                        self.btnAddToCompare.setTitle("Remove from Compare", for: .normal)
                        self.btnAddToCompare.setTitleColor(UIColor.red, for: .normal)
                        self.btnAddToCompare.setImage(UIImage(named:"compare50_redSmall.png"), for: UIControlState.normal)
                    
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }else {
             self.view.makeToastActivity(message: "Loading...", view: self.view)
            AppEngine().removeFromComare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
               self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "success") as! Bool  == true{
                  
                        self.btnAddToCompare.setImage(UIImage(named:"compare50Small.png"), for: UIControlState.normal)
                        self.btnAddToCompare.setTitle("Add to Compare", for: .normal)
                        self.btnAddToCompare.setTitleColor(ZamModel().themecolor, for: .normal)
                
                } else {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
            
        }
    }
    
    
    @IBAction func btnAddCompareClicked(_ sender: Any) {
        addTocompareClicked()
        
    }
    
    
    @IBAction func btnRateClicked(_ sender: Any) {
   
//        
//        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
//        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataDic as AnyObject).value(forKey: "centreID") as! NSNumber
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID),"userID":UserDefaults.standard.object(forKey: "userId") as! String,]
        
        let vc = CenterRateViewController(nibName: "CenterRateViewController", bundle: nil)
         appDelegate.comeFromCenterRate = "centre"
        vc.parameter = parameters as NSDictionary
        vc.centerID = String(describing: centreID)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func btnReviewClicked(_ sender: Any) {
        
//        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
//        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataDic as AnyObject).value(forKey: "centreID") as! NSNumber
        let vc = CenterReviewViewController(nibName: "CenterReviewViewController", bundle: nil)
        vc.cenetrID = String(describing: centreID)
        appDelegate.comeFromCenterReview = "centre"
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    
    @IBAction func btnProfileClicked(_ sender: Any) {
        
        let controller:ImagePopViewController = ImagePopViewController(nibName: "ImagePopViewController", bundle: nil)
        controller.imageurl = dataDic.object(forKey: "photoPath") as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
        
    }
    
    @IBAction func btnPhioneClicked(_ sender: Any) {
        let no = self.dataDic.value(forKey: "mobileNo") as! String
        if let url = NSURL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    @IBAction func btnEmailClicked(_ sender: Any) {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.delegate = self
        mailVC.setToRecipients([(self.dataDic.value(forKey: "emailID") as! String)])
        mailVC.setSubject("Zam")
        mailVC.setMessageBody("Mail text", isHTML: false)
        if (MFMailComposeViewController.canSendMail()) {
            present(mailVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnLocationClicked(_ sender: Any) {
        
    let url = "http://maps.google.com/maps?saddr=\(self.appDelegate.userLocation.coordinate.latitude),\(self.appDelegate.userLocation.coordinate.longitude)&daddr=\(self.dataDic.value(forKey: "latitude") as! String ),\(self.dataDic.value(forKey: "longitude") as! String)"
        UIApplication.shared.openURL(URL(string: url)!)
    }
    
    
    
//    func getLatLongFromAddress(address: String) {
//        CLGeocoder().geocodeAddressString(address, completionHandler: { (placemarks, error) in
//            if error != nil {
//                print(error)
//                return
//            }
//            if (placemarks?.count)! > 0 {
//                let placemark = placemarks?[0]
//                let location = placemark?.location
//                let coordinate = location?.coordinate
//               
//                
//                print("\nlat: \(coordinate!.latitude), long: \(coordinate!.longitude)")
////                if (placemark?.areasOfInterest?.count)! > 0 {
////                    let areaOfInterest = placemark!.areasOfInterest![0]
////                    print(areaOfInterest)
////                } else {
////                    print("No area of interest found.")
////                }
//            }
//        })
//
//    }
    
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail sent")
        case MFMailComposeResult.failed.rawValue:
            print("Mail sent failure: %@", [error?.localizedDescription])
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnShareClicked(_ sender: Any) {
        
//      let text = "Centre Name: \(lblname.text!)\nMobile Number: \(btnPhoneNo.titleLabel!.text!)\nEmail ID: \(btnEmail.titleLabel!.text!)\nAddress: \(lblAddress.text!)\nTrainer Rating: \(lblRateCount.text!)"
//        
        let myMutableString = NSMutableAttributedString()
        
        myMutableString
            .bold("Centre Name:")
            .normal(lblname.text!).bold("\nMobile Number:").normal(btnPhoneNo.titleLabel!.text!).bold("\nEmail ID:").normal(btnEmail.titleLabel!.text!).bold("\nAddress:").normal(lblAddress.text!).bold("\nCentre Rating:").normal(lblRateCount.text!)
        
        let textToShare = [ myMutableString ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        
         activityViewController.excludedActivityTypes = [ UIActivityType.postToFacebook, UIActivityType.postToTwitter, UIActivityType.postToWeibo, UIActivityType.message, UIActivityType.print, UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.postToVimeo,UIActivityType.postToTencentWeibo,UIActivityType.airDrop]
        
        
        activityViewController.setValue("Your email Subject" , forKey: "subject")
       // activityViewController.setValue("You" , forKey: "to");
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
//        activityViewController.excludedActivityTypes = [UIActivityType.message,UIActivityType.addToReadingList,UIActivityType.airDrop,UIActivityType.assignToContact,UIActivityType.copyToPasteboard,UIActivityType.openInIBooks,UIActivityType.postToFacebook,UIActivityType.postToFlickr,UIActivityType.postToVimeo,UIActivityType.postToTwitter,UIActivityType.postToTencentWeibo,UIActivityType.copyToPasteboard,UIActivityType.print,UIActivityType.saveToCameraRoll]
        
        
       
        
        
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
        
    }
    
    
    
    func getCenterDetail(centerId :String){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : centerId]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCenterDetail(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
//                let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
//             vc.CentreID = centerId
               self.dataDic = response?.object(forKey: "data") as! NSDictionary
                self.setdata()
                //  print(response?.object(forKey: "data") as! NSDictionary)
               // self.navigationController?.pushViewController(vc, animated: false)
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    
    
    func setdata(){
        
        lblRateCount.text = String(describing: dataDic.object(forKey: "ratevalue") as! NSNumber) + "/5"
        model.setSmileImageWhite(btn: btnRate, rateCount: Int(dataDic.object(forKey: "ratevalue") as! NSNumber))
        lblReviewCount.text = String(describing: dataDic.object(forKey: "noReviews") as! NSNumber)
        lblDescription.text = ZamModel().condenseWhitespace(str: (dataDic.object(forKey: "centreDesc") as? String)!)
        
        if lblDescription.text == ""{
            aboutTitleTopConstraint.constant = 0
            aboutTopConstrainr.constant = 0
            btnViewMoreDescription.isHidden = true
            lblAboutTitle.text = ""
            lblDescription.text = ""
        }else {
            if model.getNoOfLines(label: lblDescription) > 3{
                lblDescription.numberOfLines = 3
                btnViewMoreDescription.isHidden = false
            }else {
                lblDescription.numberOfLines = 0
                btnViewMoreDescription.isHidden = true
            }
        }
        
        
        let usps = dataDic.object(forKey: "centreUSPS") as? String
        if usps == ""{
            uspsTopConstraint.constant = 0
            uspsTitleTopConstraint.constant = 0
            lblUSPSTitle.text = ""
            lblUsps.attributedText = nil
        }else {
            lblUsps.attributedText = ZamModel().stringFromHtml(string: usps!)
        }
        
        btnEmail.setTitle("     " + (dataDic.object(forKey: "emailID") as? String)!, for: .normal)
        
        btnPhoneNo.setTitle( "     " + String(describing: self.dataDic.value(forKey: "stdCode") as! String) + " " + (dataDic.object(forKey: "mobileNo") as? String)!, for: .normal)
        
        var address = String()
        
        if String(describing: self.dataDic.value(forKey: "address1") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "address1") as! String) + ", "
        }
        if String(describing: self.dataDic.value(forKey: "address2") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "address2") as! String) + ", "
        }
        if String(describing: self.dataDic.value(forKey: "postCode") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "postCode") as! String) + ", "
        }
        if String(describing: self.dataDic.value(forKey: "cityName") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "cityName") as! String) + ", "
        }
        if String(describing: self.dataDic.value(forKey: "stateName") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "stateName") as! String) + ", "
        }
       
        address = String(address.characters.dropLast())
        address = String(address.characters.dropLast())
        
        if ZamModel().condenseWhitespace(str: address) != ""{
            imgLocationIcon.isHidden = false
            self.lblAddress.text = ZamModel().condenseWhitespace(str: address)
        }else {
            self.lblAddress.text = ""
            imgLocationIcon.isHidden = true
        }
        
        lblname.text = dataDic.object(forKey: "centreName") as? String
        
        if  let city:String = dataDic["cityName"] as? String
        {
            lblCity.text =  city as String
        }
        if  let state:String = dataDic["stateName"] as? String
        {
            lblCity.text =  lblCity.text! + ", " + state as String
        }
        
        
        btnProfileImage.sd_setBackgroundImage(with: NSURL(string: (dataDic.object(forKey: "photoPath") as? String)!) as! URL, for: .normal)
        workingDays =  (dataDic.object(forKey: "workingHours") as? NSArray)?.mutableCopy() as! NSMutableArray
        centerImages =  (dataDic.object(forKey: "centreImages") as? NSArray)?.mutableCopy() as! NSMutableArray
        centerTrainer =  (dataDic.object(forKey: "centreTrainers") as? NSArray)?.mutableCopy() as! NSMutableArray
        examsArray = (dataDic.object(forKey: "exams") as? NSArray)?.mutableCopy() as! NSMutableArray
        centerVideo = (dataDic.object(forKey: "videoPath") as? String)!
        
        if dataDic.object(forKey: "isFav") as? Bool == false{
            btnAddToCompare.setImage(UIImage(named:"compare50Small.png"), for: UIControlState.normal)
            btnAddToCompare.setTitle("Add to Compare", for: .normal)
            btnAddToCompare.setTitleColor(ZamModel().themecolor, for: .normal)
        }else{
            btnAddToCompare.setTitle("Remove from Compare", for: .normal)
            btnAddToCompare.setTitleColor(UIColor.red, for: .normal)
            btnAddToCompare.setImage(UIImage(named:"compare50_redSmall.png"), for: UIControlState.normal)
        }
        if examsArray.count > 0{
            if examsArray.count % 2 == 0 {
                collViewSupportedExamHeight.constant = CGFloat(examsArray.count / 2 * 70)
            }else {
                collViewSupportedExamHeight.constant = CGFloat((examsArray.count + 1) / 2 * 70)
            }
        }else {
            collViewSupportedExamHeight.constant = 0
        }
        
        if centerImages.count > 0{
            if centerImages.count % 2 == 0 {
                collViewHeight.constant = CGFloat(centerImages.count / 2 * 144)
            }else {
                collViewHeight.constant = CGFloat((centerImages.count + 1) / 2 * 144)
            }
        }else {
            lblCentreImageTitle.text = ""
            collViewHeight.constant = 0
        }
        if centerVideo != "Novideo" {
            btnPlay.isHidden = false
            VideoViewHeight.constant = 240
            lblCenterVideoTitle.text = "Centre Video"
        }else {
            lblCenterVideoTitle.text = ""
            btnPlay.isHidden = true
            VideoViewHeight.constant = 0
        }
        tblView.reloadData()
        if self.workingDays.count > 0 || centerTrainer.count > 0 {
            
            if centerTrainer.count > 0 && workingDays.count > 0 {
                if centerTrainer.count > 1 {
                    btnViewMore.isHidden = false
                    tblViewHeight.constant = (CGFloat(180)) + (CGFloat((workingDays.count + 1) * 30))
                    
                }else {
                    btnViewMore.isHidden = true
                    tblViewHeight.constant = (CGFloat((centerTrainer.count + 1) * 80)) + (CGFloat((workingDays.count + 1) * 30))
                }
                
            }else if centerTrainer.count > 0{
                
                if centerTrainer.count > 1{
                    btnViewMore.isHidden = false
                    tblViewHeight.constant =  (CGFloat(180))
                }else {
                    btnViewMore.isHidden = true
                    tblViewHeight.constant =  (CGFloat((centerTrainer.count + 1) * 80))
                }
                
            }else if workingDays.count > 0{
                btnViewMore.isHidden = true
                tblViewHeight.constant =  (CGFloat((workingDays.count + 1) * 30))
            }
        }else {
            tblViewHeight.constant = 0
        }
       collViewSupportedExam.reloadData()
       colllView.reloadData()
    }
    
    
    
    
    func doneButtonClick(aNotification:NSNotification)
    {
        //  let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
        //moviePlayer.isFullscreen = false
        appDelegate.isFullScreen = false
        moviePlayer .setFullscreen(false, animated: true)
        moviePlayer.stop()
        moviePlayer.view.removeFromSuperview()
    }
}

class LandscapeAVPlayerController: AVPlayerViewController {
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .landscapeLeft
    }
}



