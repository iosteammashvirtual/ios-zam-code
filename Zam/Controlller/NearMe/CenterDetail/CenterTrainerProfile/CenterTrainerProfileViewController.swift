//
//  CenterTrainerProfileViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/16/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import MessageUI
import CoreLocation

class CenterTrainerProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UINavigationControllerDelegate,UIPopoverPresentationControllerDelegate ,MFMailComposeViewControllerDelegate{
    
    
    
    
    
    @IBOutlet weak var addressTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var aboutTitleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var aboutTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblAssociatedExamTitle: UILabel!
    @IBOutlet weak var collViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collView: UICollectionView!
    @IBOutlet weak var btnViewMoreDescription: UIButton!
    @IBOutlet weak var btnProfileBackGroumd: UIButton!
    @IBOutlet weak var imgLocationAddress: UIImageView!
    @IBOutlet weak var imgLocationHeight: NSLayoutConstraint!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var lblRateCount: UILabel!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnPhoneNo: UIButton!
    @IBOutlet weak var txtAddress: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnProfilePic: UIButton!
    @IBOutlet weak var tblViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDescTitle: UILabel!
    @IBOutlet weak var lblDescp: UILabel!
    var associatedExamArray = NSMutableArray()
    var centerAssociatedArray = NSMutableArray()
    var trainerId = String()
   // let mailVC = MFMailComposeViewController()
    
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
var dataDic = NSDictionary()
    override func viewDidLoad() {
        super.viewDidLoad()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        ZamModel().buttonCorner(Button: btnProfilePic, radius: Int(btnProfilePic.frame.size.width / 2))
        let nibName = UINib(nibName: "ProfileAssociatedCentresTableCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "ProfileAssociatedCentresTableCell")
        let nibName2 = UINib(nibName: "WorkingHoursHeaderTableCell", bundle:nil)
        tblView.register(nibName2, forCellReuseIdentifier: "WorkingHoursHeaderTableCell")
        ZamModel().buttonCornerWithThemeBorder(Button: btnProfileBackGroumd, radius:Int(btnProfileBackGroumd.frame.size.width/2.0),borderWidth: 6)
        let nib = UINib(nibName: "ExamsCollectionViewCell", bundle: nil)
        collView.register(nib, forCellWithReuseIdentifier: "ExamsCollectionViewCell")
        collView.backgroundColor  = UIColor.clear
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
       getTrainerDetail(trainerId: trainerId)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
      // MARK: - TableViewDelegate & DataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            if centerAssociatedArray.count > 0{
                return centerAssociatedArray.count + 1
            }else {
                return 0
            }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
         return 30
        }else{
         return 80
        }
      
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        if section == 0 {
//            let footerView = UIView(frame: CGRect(x:0, y:0,width: tableView.frame.size.width, height:10))
//            footerView.backgroundColor = UIColor.clear
//            return footerView
//        }else {
//        return nil
//        }
//    }
//    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if section == 0 {
//        return 10
//        }else {
//        return 0
//        }
//    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:ProfileAssociatedCentresTableCell = tableView.dequeueReusableCell(withIdentifier: "ProfileAssociatedCentresTableCell") as! ProfileAssociatedCentresTableCell
        
        let cellHeader:WorkingHoursHeaderTableCell = tableView.dequeueReusableCell(withIdentifier: "WorkingHoursHeaderTableCell") as! WorkingHoursHeaderTableCell
        cell.backgroundColor = UIColor.white
        
            if indexPath.row == 0{
                cellHeader.lblTitle.text = "Associated Centres"
                return cellHeader
            }else {
                cell.lblType.text = (centerAssociatedArray[indexPath.row - 1] as AnyObject).value(forKey: "associateType") as? String
                cell.lblName.text = (centerAssociatedArray[indexPath.row - 1] as AnyObject).value(forKey: "centreName") as? String
                ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImgView, radius: Int(cell.btnImgView.frame.size.height / 2),borderWidth: 2)
                let imageurl = (centerAssociatedArray[indexPath.row - 1] as AnyObject).value(forKey: "logoPath") as! String
                cell.btnImgView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
                return cell
            }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = centerAssociatedArray[indexPath.row - 1]
        let centreID = Int((centerAssociatedArray[indexPath.row-1] as AnyObject).value(forKey: "centreID") as! NSNumber)
        
        let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
        vc.CentreID = String(describing: centreID)
        //  print(response?.object(forKey: "data") as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    

    // MARK: - CollectionView Delegate & DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return associatedExamArray.count
        
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExamsCollectionViewCell", for: indexPath as IndexPath) as! ExamsCollectionViewCell
        let imageurl = (associatedExamArray[indexPath.row] as AnyObject).value(forKey: "examLogo") as! String
        
        ZamModel().CollCellCorner(collcell: cell, radius: 5)
        
        cell.btnImageView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:collectionView.frame.size.width / 2 - 10, height:60)
        
    }
    
    
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
   
    
    
    @IBAction func btnEmailClicked(_ sender: Any) {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.delegate = self
        mailVC.setToRecipients([(self.dataDic.value(forKey: "emailID") as! String)])
        mailVC.setSubject("Zam")
        mailVC.setMessageBody("Mail Text", isHTML: false)
        if (MFMailComposeViewController.canSendMail()) {
            present(mailVC, animated: true, completion: nil)
        }
    }
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail sent")
        case MFMailComposeResult.failed.rawValue:
            print("Mail sent failure: %@", [error?.localizedDescription])
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCallClicked(_ sender: Any) {
        
        let no = self.dataDic.value(forKey: "mobileNo") as! String
        if let url = NSURL(string: "tel://\(no)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    
    
    @IBAction func btnProfileClicked(_ sender: Any) {
        if dataDic.object(forKey: "photoPath") as! String != ""{
            let controller:ImagePopViewController = ImagePopViewController(nibName: "ImagePopViewController", bundle: nil)
            controller.imageurl = dataDic.object(forKey: "photoPath") as! String
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
        }
        
        
    }
    @IBAction func btnViewMoreClicked(_ sender: Any) {
       lblDescp.numberOfLines = 0
        btnViewMoreDescription.isHidden = true
    }

    @IBAction func btnReviewClicked(_ sender: Any) {
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        vc.trainerID =  trainerId
         appDelegate.comeFromTrainerReview = "trainerprofile"
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    @IBAction func btnrateClicked(_ sender: Any) {
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerID" :  trainerId,"UserId":UserDefaults.standard.object(forKey: "userId") as! String,]
        appDelegate.comeFromTrainerRate = "trainerprofile"
        vc.parameter = parameters as NSDictionary
        vc.trainerID =  trainerId
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
  
    
    func getTrainerDetail(trainerId :String){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["TrainerID" : trainerId]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getTrainerDetail(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
               // debugPrint(response)
                self.lblRateCount.text = String(Int(response?.object(forKey: "rateValue") as! NSNumber)) + "/5"
                self.lblReviewCount.text = String(describing: response?.object(forKey: "noReviews") as! NSNumber)

                let data = response?.object(forKey: "data") as! NSDictionary
             //   print(data)
//                let vc = CenterTrainerProfileViewController(nibName: "CenterTrainerProfileViewController", bundle: nil)
                self.dataDic = data
                self.setdata()
               // vc.trainerId = trainerId
               // self.navigationController?.pushViewController(vc, animated: false)
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    func setdata(){
        let imageurl = self.dataDic.value(forKey: "photoPath") as! String
        self.btnProfilePic.sd_setBackgroundImage(with: NSURL(string: imageurl) as URL!, for: .normal)
        self.btnPhoneNo.setTitle( "     " + String(describing: self.dataDic.value(forKey: "stdCode") as! String) + " " + String(describing: self.dataDic.value(forKey: "mobileNo") as! String), for: .normal)
        self.btnEmail.setTitle("     " + String(describing: self.dataDic.value(forKey: "emailID") as! String), for: .normal)
        self.lblName.text = String(describing: self.dataDic.value(forKey: "firstName") as! String) + " " + String(describing: self.dataDic.value(forKey: "lastName") as! String)
        
        var address = String()
        
        if String(describing: self.dataDic.value(forKey: "address1") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "address1") as! String) + ","
        }
        if String(describing: self.dataDic.value(forKey: "address2") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "address2") as! String) + ","
        }
        if String(describing: self.dataDic.value(forKey: "city") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "city") as! String) + ","
        }
        if String(describing: self.dataDic.value(forKey: "state") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "state") as! String) + ","
        }
        if String(describing: self.dataDic.value(forKey: "postCode") as! String) != ""{
            address = address + String(describing: self.dataDic.value(forKey: "postCode") as! String) + ","
        }
        address = String(address.characters.dropLast())
        
        if ZamModel().condenseWhitespace(str: address) != ""{
            self.imgLocationAddress.isHidden = false
            addressTopConstraint.constant = 20
            self.txtAddress.text = ZamModel().condenseWhitespace(str: address)
        }else {
            txtAddress.text = ""
            addressTopConstraint.constant = 0
            imgLocationAddress.isHidden = true
        }
        
        
        
     
        centerAssociatedArray =  (dataDic.object(forKey: "trainerAssoCentres") as? NSArray)?.mutableCopy() as! NSMutableArray
        //         associatedExamArray =  (dataDic.object(forKey: "trainerExams") as? NSArray)?.mutableCopy() as! NSMutableArray
        
        lblDescp.text = self.dataDic.value(forKey: "profileDesc") as? String
        
        if ZamModel().getNoOfLines(label: lblDescp) > 3{
            lblDescp.numberOfLines = 3
            btnViewMoreDescription.isHidden = false
        }else {
            lblDescp.numberOfLines = 0
            btnViewMoreDescription.isHidden = true
        }
        
        if lblDescp.text == ""{
            lblDescTitle.text = ""
            aboutTopConstraint.constant = 0
            aboutTitleTopConstraint.constant = 0
        }
        
        if self.dataDic.value(forKey: "trainerExams") != nil  && !(self.dataDic.value(forKey: "trainerExams") is NSNull) {
            self.associatedExamArray = (self.dataDic.value(forKey: "trainerExams") as! NSArray).mutableCopy() as! NSMutableArray
            if self.associatedExamArray.count > 0{
                self.lblAssociatedExamTitle.text = "Associated Exams"
                if self.associatedExamArray.count % 2 == 0 {
                    self.collViewHeight.constant = CGFloat(self.associatedExamArray.count / 2 * 60) + 20
                }else {
                    self.collViewHeight.constant = CGFloat((self.associatedExamArray.count + 1) / 2 * 60) + 20
                }
            }else {
                self.lblAssociatedExamTitle.text = ""
                self.collViewHeight.constant = 0
            }
        }else {
            self.lblAssociatedExamTitle.text = ""
            self.collViewHeight.constant = 0
        }
        
        if self.centerAssociatedArray.count > 0  {
            tblViewHeight.constant =  (CGFloat((centerAssociatedArray.count) * 80) + 30)
        }else {
            tblViewHeight.constant = 0
        }
        collView.reloadData()
        tblView.reloadData()
    
    }
    @IBAction func btnShareClicked(_ sender: Any) {
        
        let myMutableString = NSMutableAttributedString()
        
        myMutableString
            .bold("Trainer Name:")
            .normal(lblName.text!).bold("\nMobile Number:").normal(btnPhoneNo.titleLabel!.text!).bold("\nEmail ID:").normal(btnEmail.titleLabel!.text!).bold("\nAddress:").normal(txtAddress.text!).bold("\nTrainer Rating:").normal(lblRateCount.text!)
        
        
        let textToShare = [ myMutableString ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [UIActivityType.message,UIActivityType.addToReadingList,UIActivityType.airDrop,UIActivityType.assignToContact,UIActivityType.copyToPasteboard,UIActivityType.openInIBooks,UIActivityType.postToFacebook,UIActivityType.postToFlickr,UIActivityType.postToVimeo,UIActivityType.postToTwitter,UIActivityType.postToTencentWeibo,UIActivityType.copyToPasteboard,UIActivityType.print,UIActivityType.saveToCameraRoll]
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnLocationClicked(_ sender: Any) {
   
   // getLatLongFromAddress(address: txtAddress.text!)
    }
    
    
//    func getLatLongFromAddress(address: String) {
//        CLGeocoder().geocodeAddressString(address, completionHandler: { (placemarks, error) in
//            if error != nil {
//                print(error)
//                return
//            }
//            if (placemarks?.count)! > 0 {
//                let placemark = placemarks?[0]
//                let location = placemark?.location
//                let coordinate = location?.coordinate
//                let url = "http://maps.google.com/maps?saddr=\(self.appDelegate.userLocation.coordinate.latitude),\(self.appDelegate.userLocation.coordinate.longitude)&daddr=\(coordinate!.latitude),\(coordinate!.longitude)"
//                UIApplication.shared.openURL(URL(string: url)!)
//                
//                print("\nlat: \(coordinate!.latitude), long: \(coordinate!.longitude)")
////                if (placemark?.areasOfInterest?.count)! > 0 {
////                    let areaOfInterest = placemark!.areasOfInterest![0]
////                    print(areaOfInterest)
////                } else {
////                    print("No area of interest found.")
////                }
//            }
//        })
//    }
    
}


extension NSMutableAttributedString {
    func bold(_ text:String) -> NSMutableAttributedString {
//         cell.lblProdDesc.font = UIFont (name: "HelveticaNeueLTStd-CnBold ", size: 20)
//        let labelFont = UIFont(name: "HelveticaNeue-Bold", size: 18)
        
        let attrs:[String:AnyObject] = [NSFontAttributeName : UIFont(name: "HelveticaNeue-Bold", size: 13)!]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}
