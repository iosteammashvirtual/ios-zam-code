//
//  WorkingHoursTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/14/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class WorkingHoursTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNameTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDaysName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
