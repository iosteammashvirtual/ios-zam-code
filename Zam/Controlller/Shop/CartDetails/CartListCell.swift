//
//  CartListCell.swift
//  Zam
//
//  Created by Admin on 17/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CartListCell: UITableViewCell {

    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgV_Content: UIImageView!
    @IBOutlet weak var lblSubHeading: MDHTMLLabel?
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblReviewCount: UILabel!
   
    @IBOutlet weak var lblQty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
