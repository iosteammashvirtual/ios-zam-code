//
//  CartListVC.swift
//  Zam
//
//  Created by Admin on 17/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import Alamofire

class CartListVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource
{
    
    @IBOutlet weak var btnContinueShopping: UIButton!
    @IBOutlet weak var lblCartNil: UILabel!
    @IBOutlet weak var viewCartNil: UIView!
    @IBOutlet weak var lblSummary: UILabel!
    @IBOutlet weak var viewWithPicker: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var tbleView: UITableView!
    var alertConfirm = AlertConfirmViewController()
    let defaults = UserDefaults.standard
    var dataArray = NSMutableArray()
    var selectedCartSender = Int()
    var total = String()
    var items = String()
    var  pickerData = ["1","2","3","4","5"]
    var selectedIndex = Int()
    var updateCartParam = [String:Any]()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tbleView.rowHeight = UITableViewAutomaticDimension;
        self.tbleView.estimatedRowHeight = 180;
       ZamModel().buttonCorner(Button: btnContinueShopping, radius: 5)
        
        self.Configure()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func Configure()
    {
        self.tbleView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
         self.tabBarController?.tabBar.isHidden = true
         self.tbleView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tbleView.bounds.size.width, height: 0.1))
        pickerView.dataSource = self
        pickerView.delegate = self
        self.viewWithPicker.isHidden = true
        pickerView.reloadAllComponents()
        self.getCartList()
        
    }
    
    func getCartList()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String] as [String : Any]
        //print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCartList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response?.object(forKey: "data") as! NSArray)
                let arr = response?.object(forKey: "data") as! NSArray
                self.dataArray = NSMutableArray(array: arr)
                self.tbleView .reloadData()
                //print(self.dataArray.count)
                
                self.items = String(describing :response?.object(forKey: "noOfitems") as! NSNumber)
                self.total = String(describing :response?.object(forKey: "grandTotal") as! NSNumber)
                self.lblSummary.text = String("Summary: " +  (self.items) + " items | ₹ " + self.total + ".0")
                self.viewCartNil.isHidden = true
                self.lblCartNil.text = ""
               
            } else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
                 self.lblSummary.text = String("Summary: " +  "0" + " item | ₹ " + "0")
                self.dataArray.removeAllObjects()
                self.viewCartNil.isHidden = false
                self.lblCartNil.text = response?.object(forKey: "message") as! String
                
                self.tbleView.reloadData()
            }
        }
        
    }

    //MARK: Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:CartListCell = tableView.dequeueReusableCell(withIdentifier: "CartListCell") as! CartListCell
        
        let dic = (self.dataArray[indexPath.row] as AnyObject).value(forKey: "centreMarketPlace") as! NSDictionary
        cell.lblHeading.text = dic["pcsName"]as? String
        //let longDesc = ZamModel().stringFromHtml(string: dic["longDesc"]as! String)
    
        cell.lblSubHeading?.htmlText = dic["longDesc"]as! String
        cell.lblSubHeading?.textColor = ZamModel().themecolor
        cell.lblRating.text = String(describing: dic["rates"] as! NSNumber) + "/5"
        cell.lblReviewCount.text = String(describing: dic["noReviews"] as! NSNumber)
        
        //Set Image
        cell.imgV_Content?.layer.cornerRadius = cell.imgV_Content.frame.height/2
        cell.imgV_Content.layer.masksToBounds = true
        cell.imgV_Content.layer.borderColor = ZamModel().themecolor.cgColor
        cell.imgV_Content.layer.borderWidth = 2
        let url = NSURL(string:dic["imagePath"] as! String)
        cell.imgV_Content.sd_setImage(with: url as URL!)
        
        //Smiley
        let rateVal = (dic["rates"] as! NSNumber)
        ZamModel().setSmileImageThemeColor(btn: cell.btnRating, rateCount: Int(rateVal))
        cell.lblQty.text = String(describing:(self.dataArray[indexPath.row] as AnyObject).value(forKey: "quantity") as! NSNumber)
        
        let orignalprice = String(describing: (self.dataArray[indexPath.row] as AnyObject)["totalFeesAfterDis"] as! NSNumber)
        var pricestr = "₹" + orignalprice
        let discount = String(describing :(self.dataArray[indexPath.row] as AnyObject)["discount"] as! String)
        
        if discount == ""
        {
            cell.lblPrice.text = pricestr
        }
        else
        {
             pricestr =  "₹" + String(describing :(self.dataArray[indexPath.row] as AnyObject)["totalFees"] as! NSNumber) + pricestr 
             pricestr = pricestr + "\n" + String(describing :(self.dataArray[indexPath.row] as AnyObject)["discount"] as! String)
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string:pricestr)
            let range = (pricestr as NSString).range(of: String(describing :(self.dataArray[indexPath.row] as AnyObject)["discount"] as! String))
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(1, orignalprice.characters.count))
            attributeString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
            //print(attributeString)
            cell.lblPrice.attributedText = attributeString

        }        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let story = UIStoryboard(name: "ShopStory", bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetails
        VC.marketPlaceID = ((self.dataArray[indexPath.row] as AnyObject).value(forKey: "marketPlaceID") as? NSNumber)!
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    @IBAction func btnRemovefromCart(_ sender: Any)
    {
            // Post notification
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        selectedCartSender = (indexPath?.row)!
        
        NotificationCenter.default.addObserver(self, selector: #selector(CartListVC.YesClicked), name: Notification.Name("PopToProfile"), object: nil)

        alertConfirm.message = "Are you sure you want to remove this item from Cart?"
        addChildViewController(alertConfirm)
        alertConfirm.view.frame = view.bounds
        view.addSubview(alertConfirm.view)
        alertConfirm.didMove(toParentViewController: self)
       
        
    }
    
    func YesClicked(note:NSNotification)
    {
       
        let dic = self.dataArray[selectedCartSender] as AnyObject
        let param = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String,"ShoppingCartID" : dic["shoppingCartID"] as! NSNumber,"userID" : defaults.object(forKey: "userId") as! String] as [String : Any]
        self.removeFromCart(parameters: param)
          NotificationCenter.default.removeObserver(self, name: Notification.Name("PopToProfile"), object: nil);
    }

    
    func removeFromCart(parameters:[String:Any])
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().removefromCart(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                 self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.getCartList()

            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    
    
    @IBAction func btnChooseQty(_ sender: Any)
    {
        self.viewWithPicker.isHidden = false
        //self.tbleView.addSubview(viewWithPicker)
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        selectedIndex = (indexPath?.row)!
        let dic = self.dataArray[(indexPath?.row)!] as AnyObject
        
        updateCartParam = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String,"ShoppingCartID" : dic["shoppingCartID"] as! NSNumber,"userID" : defaults.object(forKey: "userId") as! String] as [String : Any]
    }
    
    
    
    // DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // Delegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
         return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
         //print(pickerView.selectedRow(inComponent: component))
    }
    
    @IBAction func btnSelectPicker(_ sender: Any)
    {
        //pickerView.selectedRow(inComponent: Int)
        let row = pickerView.selectedRow(inComponent: 0)
        //print(self.pickerData[row])
        
        updateCartParam["itemCount"] = self.pickerData[row] as String
        //print("Update ", updateCartParam)
        self.updateCart(count: row)
        self.viewWithPicker.isHidden = true

    }
    
    func updateCart(count:Int)
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        //print(updateCartParam)
       self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().updateCart(parameter: updateCartParam as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response)
                
               let data:NSDictionary = (self.dataArray[self.selectedIndex] as AnyObject) as! NSDictionary
               print(data)
                
                let dic = NSMutableDictionary(dictionary: data)

                dic.setValue(response?.object(forKey: "discountItemAmount"), forKey: "totalFeesAfterDis")
                dic.setValue(count+1, forKey: "quantity")
                dic.setValue(response?.object(forKey: "discount"), forKey: "discount")
                 dic.setValue(response?.object(forKey: "itemAmount"), forKey: "totalFees")
                
                self.dataArray.replaceObject(at: self.selectedIndex, with: dic)
                
                self.tbleView.reloadData()
              self.view.makeToast(message: QuantityUpdated + String(count+1))
                
                let item = String(describing :response?.object(forKey: "noCartItem") as! NSNumber)
                let total = String(describing :response?.object(forKey: "totalamount") as! NSNumber)
                self.lblSummary.text = String("Summary: " +  (item) + " Items | ₹" + total + ".0")
                self.total = total
                
             //   self.getCartList()
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    @IBAction func btnClosePicker(_ sender: Any)
    {
        self.viewWithPicker.isHidden = true
        //self.viewWithPicker .removeFromSuperview()
    }
    
    
    @IBAction func btnReviewClicked(_ sender: Any)
    {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        let marketPlaceId = (self.dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "marketPlaceID") as! NSNumber
        
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        vc.marketplaceID = marketPlaceId
        vc.navigateShop = true
        vc.comeFromRateSuccess = false
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btnRateClicked(_ sender: Any)
    {
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
        vc.navigateShop = true
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tbleView)
        let indexPath = self.tbleView.indexPathForRow(at: buttonPosition)
        let marketPlaceId = (self.dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "marketPlaceID") as! NSNumber
        let parameters = ["MarketplaceID":marketPlaceId ]  as [String : Any]
        vc.parameter = parameters as NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnCloseVC(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCheckOutClicked(_ sender: Any)
    {
        if self.dataArray.count>0
        {
            let story = UIStoryboard(name: "ShopStory", bundle: nil)
            let VC = story.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
            VC.total = self.total
            VC.item = self.items
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }

    @IBAction func btnContinueClicked(_ sender: Any) {
        
        let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
        self.navigationController!.popToViewController(dashboardVC, animated: true)
        self.tabBarController?.tabBar.isHidden = false
    }

}
