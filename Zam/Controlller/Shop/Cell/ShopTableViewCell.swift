//
//  ShopTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/2/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell
{
    //MARK: IB OUTLET
    
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var btnRating: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgV_Content: UIImageView!
    @IBOutlet weak var lblSubHeading: MDHTMLLabel!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblAddCart: UILabel!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var imgRate: UIButton!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
