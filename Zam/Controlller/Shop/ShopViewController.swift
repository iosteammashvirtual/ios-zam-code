//
//  ShopViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 12/31/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit
import Alamofire

class ShopViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout
{
    var ScrollEnable:Bool = true
   
    @IBOutlet weak var btnViewCart: UIButton!
    @IBOutlet weak var tblView: UITableView!
    let defaults = UserDefaults.standard
    var dataArray = NSArray()
    var addArray = NSArray()
    var winnerlistScrolltimer : Timer!
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
       override func viewDidLoad()
    {
        super.viewDidLoad()
        
         ZamModel().buttonCornerWithThemeBorder(Button: btnViewCart, radius: 1, borderWidth: 0)
        // Swipe
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ShopViewController.SwipeRightGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
          NotificationCenter.default.post(name: Notification.Name("SliderClosed"), object: nil)
    }
    override func viewDidAppear(_ animated: Bool)
    {  // colseSliderView()
        self.Configure()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        if(winnerlistScrolltimer != nil)
        {
            winnerlistScrolltimer.invalidate()
        }
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        defaults.setValue(false, forKey: "navigate_Search")
       defaults.setValue(nil, forKey: "dataList")
    }
    func Configure()
    {self.tabBarController?.tabBar.isHidden = false
        self.tblView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tblView.bounds.size.width, height: 0.1))
//        pageControl.pageIndicatorTintColor = UIColor.gray
//        pageControl.currentPageIndicatorTintColor = UIColor.red
        if(winnerlistScrolltimer != nil)
        {
            winnerlistScrolltimer.invalidate()
        }
        let nibName = UINib(nibName: "ShopTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "ShopTableViewCell")
        let nibCellName = UINib(nibName: "ShopTableViewHeaderCell", bundle:nil)
        tblView.register(nibCellName, forCellReuseIdentifier: "ShopTableViewHeaderCell")
     
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 144.0;
        self.tblView.setNeedsLayout()
        self.tblView.layoutIfNeeded()
        //self.tblView.tableHeaderView = viewHeader
        
        self.getAddList()
        let  navigate_Search = (defaults.value(forKey: "navigate_Search")) as! Bool
        print(navigate_Search)
        if navigate_Search == false
        {
             self.getProductList()
        }
        else
        {
            let productList = UserDefaults.standard.object(forKey: "dataList") as? NSData
            if let productList = productList
            {
                 self.dataArray = NSKeyedUnarchiver.unarchiveObject(with: productList as Data) as! NSArray
                //print(self.dataArray)
            }
           
            if(self.dataArray.count>0)
            {
                
                self.tblView.dataSource = self
                    self.tblView.delegate = self
                    self.tblView .reloadData()
                    let predicate = NSPredicate(format: "isCart == 1")
                    let list = self.dataArray.filtered(using: predicate)
                    if list.count>0
                    {
                        self.btnViewCart .setTitle("\(list.count)" as String, for: UIControlState.normal)
                    }
                    else
                    {
                        self.btnViewCart .setTitle("0", for: UIControlState.normal)
                    }
            }
        }
    }
    func getAddList()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = [String : Any]()
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getAddList(parameter:parameters as NSDictionary ) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response?.object(forKey: "data") as! NSArray)
                self.addArray = response?.object(forKey: "data") as! NSArray
                print(self.addArray)
//                self.pageControl.numberOfPages = self.addArray.count
                self.tblView.reloadData()
            } else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    func getProductList()
    {
            if ZamModel().isConnectedToNetwork() == false
            {
                let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                controller.message = networkError
                addChildViewController(controller)
                controller.view.frame = view.bounds
                view.addSubview(controller.view)
                controller.didMove(toParentViewController: self)
                return
            }
            let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String] as [String : Any]
            print(parameters)
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().getProductList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true
                {
                    print(response?.object(forKey: "data") as! NSArray)
                    self.dataArray = response?.object(forKey: "data") as! NSArray
                    if self.dataArray.count > 0
                    {
                        self.tblView.dataSource = self
                        self.tblView.delegate = self
                        self.tblView .reloadData()
                        let predicate = NSPredicate(format: "isCart == 1")
                        let list = self.dataArray.filtered(using: predicate)
                        if list.count>0
                        {
                            self.btnViewCart .setTitle("\(list.count)" as String, for: UIControlState.normal)
                        }
                        else
                        {
                            self.btnViewCart .setTitle("0", for: UIControlState.normal)
                        }
                    }
                    else
                    {
                        self.view.makeToast(message: response?.object(forKey: "No Data Found") as! String)
                    }
                }
                else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
    }
   
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    //MARK: Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        print("data array ",self.dataArray.count)
        return self.dataArray.count + 1
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // create a new cell if needed or reuse an old one
        if indexPath.row == 0
        {
            let headerCell:ShopTableViewHeaderCell = tableView.dequeueReusableCell(withIdentifier: "ShopTableViewHeaderCell") as! ShopTableViewHeaderCell
            let nibCollCellName = UINib(nibName: "showAddCell", bundle:nil)
            headerCell.pageContrl.numberOfPages = self.addArray.count
            headerCell.pageContrl.pageIndicatorTintColor = UIColor.gray
            //headerCell.pageContrl.pageIndicatorTintColor = UIColor.gray
          //  pageControl.currentPageIndicatorTintColor = UIColor.red

            headerCell.collecView.register(nibCollCellName, forCellWithReuseIdentifier: "showAddCell"  )
            headerCell.collecView.delegate = self
            headerCell.collecView.dataSource = self
            headerCell.collecView.reloadData()
            if(self.addArray.count > 1)
            {
                if(winnerlistScrolltimer != nil)
                {
                    winnerlistScrolltimer.invalidate()
                }
                winnerlistScrolltimer = Timer.scheduledTimer(timeInterval: 5, target:self, selector: #selector(ShopViewController.ScrollTimer), userInfo: nil, repeats: true)
                
            }
            return  headerCell
        }
        else
        {
            let cell:ShopTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ShopTableViewCell") as! ShopTableViewCell
        
            cell.btnCart.addTarget(self, action: #selector(addToCart), for: .touchUpInside)
            cell.imgRate.addTarget(self, action: #selector(checkRating), for: .touchUpInside)
            cell.btnReview.addTarget(self, action: #selector(checkReview), for: .touchUpInside)

            let dic_user = (self.dataArray[indexPath.row - 1] as AnyObject)
            if let pcsName = dic_user.value(forKey: "pcsName") as? String
            {
                cell.lblHeading.text = pcsName
            }
//            cell.lblHeading.text = (self.dataArray[indexPath.row] as AnyObject).value(forKey: "pcsName") as? String
        
           // let str = ZamModel().stringFromHtml(string: dic_user.value(forKey: "longDesc") as! String)
            if dic_user.value(forKey: "longDesc") != nil{
            cell.lblSubHeading.htmlText = dic_user.value(forKey: "longDesc") as! String
            }
            cell.lblSubHeading.textColor = ZamModel().themecolor
        
            if let fee = dic_user["fees"]
            {
                var orignalprice = String(describing: fee as! NSNumber)
                var pricestr = "₹" + orignalprice
                if let discountfee = dic_user["discountFees"]
                {
                    if (discountfee as! NSNumber) == 0
                    {
                           cell.lblPrice.text = pricestr
                    }
                    else
                    {
                        pricestr = pricestr + "₹" + String(describing:discountfee as! NSNumber)
                        if let discountper = (self.dataArray[indexPath.row-1] as AnyObject)["discountFees"]
                        {
                            
                            pricestr = pricestr + "\n" + String(describing :dic_user["discount"] as! String)
                            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string:pricestr)
                            let range = (pricestr as NSString).range(of: String(describing :dic_user["discount"] as! String))
                            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(1, orignalprice.characters.count))
                            attributeString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
                            cell.lblPrice.attributedText = attributeString
                        }

                    }
                }
                
            }
            cell.imgV_Content?.image = UIImage (named: "download")
            cell.imgV_Content?.layer.cornerRadius = cell.imgV_Content.frame.height/2
            cell.imgV_Content.layer.masksToBounds = true
            cell.lblReviewCount.text = String(describing:(self.dataArray[indexPath.row-1] as AnyObject).value(forKey: "noReview")as! NSNumber)
        
            //SET IMAGE
        
            cell.imgV_Content.layer.borderColor = ZamModel().themecolor.cgColor
            cell.imgV_Content.layer.borderWidth = 2
            let url = NSURL(string:(self.dataArray[indexPath.row-1] as AnyObject).value(forKey: "imagePath") as! String)
            cell.imgV_Content.sd_setImage(with: url as URL!)
        
            //Smiley
            let rateVal = ((self.dataArray[indexPath.row-1] as AnyObject).value(forKey: "rateValue") as! NSNumber)
            cell.lblRating.text =  String(describing: rateVal) + "/5"
            switch rateVal
            {
            case 0:
                cell.imgRate.setImage(UIImage(named: "awesome50"), for: UIControlState.normal)
                break;
            case 1:
                cell.imgRate.setImage(UIImage(named: "angry50"), for: UIControlState.normal)
                break;
            case 2:
                cell.imgRate.setImage(UIImage(named: "sad50"), for: UIControlState.normal)
                break;
            case 3:
                cell.imgRate.setImage(UIImage(named: "natural50"), for: UIControlState.normal)
                break;
            case 4:
                cell.imgRate.setImage(UIImage(named: "happy50"), for: UIControlState.normal)
                break;
            case 5:
                cell.imgRate.setImage(UIImage(named: "awesome50"), for: UIControlState.normal)
                break;
            default:
                break;
            }
            //CART
            if  (self.dataArray[indexPath.row-1] as AnyObject).value(forKey: "isCart") as? NSNumber == 1
            {
                cell.btnCart.isSelected = true
                cell.lblAddCart.text = "GO TO CART"
                cell.lblAddCart.textColor = UIColor.black
            }
            else
            {
                cell.btnCart.isSelected = false
                cell.lblAddCart.text = "ADD"
                 cell.lblAddCart.textColor = ZamModel().themecolor
            }
            return cell
        }
    }
    
    func addToCart(sender:UIButton)
    {
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        
        let dic = self.dataArray[((indexPath?.row)!-1)] as AnyObject
        if  dic["isCart"] as? NSNumber == 0
        {
            //Hit service
            let param = ["MarketPlaceID" : dic["marketPlaceID"] as! NSNumber,"PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String,"ProductSKU" : dic["prodSKU"] as! String, "offerID": dic["offerID"]as! NSNumber,"userID" : defaults.object(forKey: "userId") as! String,"quantity" : "1"] as [String : Any]
            
            print(param)
            self.addtoCart(parameters: param)
            
        }
        else
        {
            //Navigate to cart list
            let story = UIStoryboard(name: "ShopStory", bundle: nil)
            let VC = story.instantiateViewController(withIdentifier: "CartListVC") as! CartListVC
           // VC.marketPlaceID = ((self.dataArray[indexPath.row] as AnyObject).value(forKey: "marketPlaceID") as? NSNumber)!
            self.navigationController?.pushViewController(VC, animated: false)
        }

    }
    
    func addtoCart(parameters:[String:Any])
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
       
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().addToCart(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
               self.view.makeToast(message: response?.object(forKey: "message") as! String)
               self.getProductList()
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }

    func checkReview(sender:UIButton)
    {
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let marketPlaceId = (self.dataArray[(indexPath?.row)!-1] as AnyObject).value(forKey: "marketPlaceID") as! NSNumber

        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        vc.marketplaceID = marketPlaceId
        vc.navigateShop = true
        vc.comeFromRateSuccess = false
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func checkRating(sender:UIButton)
    {
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
        vc.navigateShop = true
        
        let buttonPosition = (sender as AnyObject).convert(CGPoint.zero, to: self.tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let marketPlaceId = (self.dataArray[(indexPath?.row)!-1] as AnyObject).value(forKey: "marketPlaceID") as! NSNumber
        let parameters = ["MarketplaceID":marketPlaceId ]  as [String : Any]
        vc.parameter = parameters as NSDictionary
        vc.marketPlaceID = (self.dataArray[(indexPath?.row)!-1] as AnyObject).value(forKey: "marketPlaceID") as! NSNumber
        self.navigationController?.pushViewController(vc, animated: true)
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let story = UIStoryboard(name: "ShopStory", bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetails
        VC.marketPlaceID = ((self.dataArray[indexPath.row-1] as AnyObject).value(forKey: "marketPlaceID") as? NSNumber)!
        VC.navigate_Serach = false
        self.navigationController?.pushViewController(VC, animated: false)
        print("You tapped cell number \(indexPath.row).")
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            return 183
        }
        return UITableViewAutomaticDimension
    }
    
    //MARK: Collect Delegates
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
        {
////            
            if(self.addArray.count > 1)
            {
                if(winnerlistScrolltimer != nil)
                {
                    winnerlistScrolltimer.invalidate()
                }
                winnerlistScrolltimer = Timer.scheduledTimer(timeInterval: 5, target:self, selector: #selector(ShopViewController.ScrollTimer), userInfo: nil, repeats: true)
                
            }
            print(self.addArray.count)
            return self.addArray.count
        }
    
    func ScrollTimer()
    {
        let visibleitm_tbl = tblView.indexPathsForVisibleRows
        if let firstindex = visibleitm_tbl?[0]
        {
            let headercell0 = tblView.cellForRow(at: firstindex)
            if(headercell0 is ShopTableViewHeaderCell)
            {
                let headercell = headercell0 as! ShopTableViewHeaderCell
                var visibleItems = headercell.collecView!.indexPathsForVisibleItems
                let currentItem = visibleItems[0] as NSIndexPath
                var nextItem = IndexPath(row: currentItem.item + 1 , section: currentItem.section)
                
                if(nextItem.row < self.addArray.count)
                {
                    headercell.collecView.scrollToItem(at: nextItem, at: UICollectionViewScrollPosition.left, animated: true)
                    let pageWidth = headercell.collecView.frame.size.width
                    headercell.pageContrl.currentPage = Int(headercell.collecView.contentOffset.x / pageWidth)
                    
                }
                else
                {
                    let pageWidth = headercell.collecView.frame.size.width
                    headercell.pageContrl.currentPage = Int(headercell.collecView.contentOffset.x / pageWidth)
                    headercell.collecView.scrollToItem(at: IndexPath(row: 0, section: currentItem.section), at: UICollectionViewScrollPosition.left, animated: true)
                }


            }
        }
     
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        let headerCell:ShopTableViewHeaderCell = self.tblView.dequeueReusableCell(withIdentifier: "ShopTableViewHeaderCell") as! ShopTableViewHeaderCell
        let pageWidth = headerCell.collecView.frame.size.width
        headerCell.pageContrl.currentPage = Int(headerCell.collecView.contentOffset.x / pageWidth)
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell :showAddCell = collectionView.dequeueReusableCell(withReuseIdentifier: "showAddCell", for: indexPath) as! showAddCell
        
        cell.btnShowAddImg.tag = indexPath.row
        cell.btnShowAddImg.addTarget(self, action: #selector(ShopViewController.btnAddClicked), for: .touchUpInside)
    
        let url = NSURL(string:(self.addArray[indexPath.row] as AnyObject).value(forKey: "adMediaResource") as! String)
        cell.btnShowAddImg.sd_setImage(with: url as URL!, for: UIControlState.normal)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: collectionView.frame.width, height: 183)
    }
 
    
    @IBAction func btnViewCartClicked(_ sender: Any)
    {
        let story = UIStoryboard(name: "ShopStory", bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: "CartListVC") as! CartListVC
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    
   //MARK:- Left Slider
    
    
    @IBAction func sliderclicked(_ sender: Any) {
   SliderOpen()
    }
   
    
    
    @IBAction func btnSearchClicked(_ sender: Any) {
        let vc = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    func colseSliderView(){
        if(ScrollEnable == false){
            ScrollEnable = true
            let viewMenuBack : UIView = view.subviews.last!
            viewMenuBack.removeFromSuperview()
        }
    }
    
    func btnAddClicked(sender:UIButton){
        
        let data = self.addArray[sender.tag]
        let vc = WebViewController(nibName: "WebViewController", bundle: nil)
        vc.comeFromShopAdd = true
        vc.strUrl = (data as AnyObject).value(forKey: "linkURL") as! String
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    
    func SliderOpen(){
        
        let menuVC : SliderViewController = SliderViewController(nibName: "SliderViewController", bundle: nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        appDelegate.SwipeRightOpen = true
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.frame = CGRect(x:0 - UIScreen.main.bounds.size.width, y:0, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame = CGRect(x:0 , y:0, width:UIScreen.main.bounds.size.width , height:UIScreen.main.bounds.size.height);
        }, completion:nil)
        
    }

    func SwipeRightGesture(gesture: UIGestureRecognizer) {
        
        if appDelegate.SwipeRightOpen == false
        {
            SliderOpen()
        }
    }
    
}
