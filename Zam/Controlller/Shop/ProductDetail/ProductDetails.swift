//
//  ProductDetails.swift
//  Zam
//
//  Created by Admin on 16/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import Alamofire
import AssetsLibrary
import MobileCoreServices
import MessageUI

class ProductDetails: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate
{
    
   
    @IBOutlet weak var openView: UIView!
    @IBOutlet weak var imgFullImgView: UIImageView!
    @IBOutlet weak var viewFullimg: UIView!
    @IBOutlet weak var lblBeforeDisConst: NSLayoutConstraint!
    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    @IBOutlet weak var lblBeforeDis: UILabel!
    @IBOutlet weak var bgimgView: UIButton!
    @IBOutlet weak var lblDisValue: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var htProdDesc: NSLayoutConstraint!
    @IBOutlet weak var txtProdDesc: UITextField!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblProductPrice: UILabel!
    @IBOutlet weak var txtProdUsp: UITextField!
    @IBOutlet weak var collecView: UICollectionView!
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet var headerView: UIView!
    var navigate_Serach = false
    var marketPlaceID :NSNumber = 0.0
    let defaults = UserDefaults.standard
    var productDetail = NSMutableDictionary()
    var imgList = NSArray()
    var rowCount = 0
    var htforHeader = 480
    override func viewDidLoad()
    {
        super.viewDidLoad()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = true
        self.openView.isHidden = true
        print(self.marketPlaceID)
         self.getProductDetail()
    }
    func getProductDetail()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String,"marketPlaceID": self.marketPlaceID] as [String : Any]
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getProductDetails(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response?.object(forKey: "data") as! NSDictionary)
                self.productDetail = (response?.object(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
              
                self.Configure()
                
            } else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }

    }
    
    func Configure()
    {
        self.tbleView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        self.tbleView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0 , width: self.tbleView.bounds.size.width, height: 0.1))
        self.automaticallyAdjustsScrollViewInsets = false
        self.tbleView.rowHeight = UITableViewAutomaticDimension;
        self.tbleView.estimatedRowHeight = 144.0;
        self.tbleView.setNeedsLayout()
        self.tbleView.layoutIfNeeded()
        //Set Image
        bgimgView.layer.cornerRadius = bgimgView.frame.size.height/2
        bgimgView.layer.masksToBounds = true
        bgimgView.layer.borderWidth = 6
        bgimgView.layer.borderColor = ZamModel().themecolor.cgColor
        imgProduct.layer.borderColor = UIColor.white.cgColor
        imgProduct.layer.cornerRadius = imgProduct.frame.size.height/2
        imgProduct.layer.masksToBounds = true
        imgProduct.layer.borderWidth = 2
        
        let url = NSURL(string:(self.productDetail["photoPath"]) as! String)
        imgProduct.sd_setImage(with: url as URL!)
    
        
        if (self.productDetail["discountFees"] as! NSNumber) == 0
        {
            lblBeforeDisConst.constant = 0
            htforHeader = 480-21
            lblBeforeDis.isHidden = true
            lblDisValue.isHidden = true
            lblProductPrice.text = "₹" + String(describing: self.productDetail["fees"]as! NSNumber)
        }
            
        else
        {
            lblBeforeDisConst.constant = 21
            htforHeader = 480
            lblDisValue.text =  self.productDetail["discount"] as? String
            let attributeString: NSMutableAttributedString = NSMutableAttributedString(string: "₹" + String(describing: self.productDetail["fees"]as! NSNumber))
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            lblBeforeDis.attributedText =  attributeString
            lblProductPrice.text = "₹" + String(describing: self.productDetail["discountFees"]as! NSNumber)
            //lblProductPrice.backgroundColor = UIColor.brown
            lblBeforeDis.isHidden = false
            lblDisValue.isHidden = false
        }
        
        //PRODUCT NAME
        lblProductName.text = self.productDetail["pcsName"] as? String
        
        //PhNo
        btnCall .setTitle(self.productDetail["phoneNumber"] as? String, for: UIControlState.normal)
        
        //Email
        btnEmail.setTitle(self.productDetail["emailID"] as? String, for: UIControlState.normal)
        
        //Btn Rate
        let rateCount = "\(String(describing: self.productDetail["rateValue"] as! NSNumber))/5"
            //rbtnRate.setTitle(rateCount, for: UIControlState.normal)
        lblRate.text = rateCount
        ZamModel().setSmileImageWhite(btn: btnRate, rateCount: Int(self.productDetail["rateValue"] as! NSNumber))
        
        //Btn Review
        let reviewCount = String(describing: self.productDetail["noReviews"] as! NSNumber)
        //btnReview.setTitle(reviewCount, for: UIControlState.normal)
        lblReview.text = reviewCount
        
        //Check for Cart
        if self.productDetail["isCart"] as? NSNumber == 0
        {
            btnCart.setTitle("Add to Cart", for: UIControlState.normal)
            btnCart.setImage(UIImage (named: "cart78"), for: UIControlState.normal)
            btnCart .setTitleColor(UIColor.white, for: .normal)

        }
        else
        {
            btnCart.setTitle("Go to Cart", for: UIControlState.normal)
            btnCart.setImage(UIImage (named: "cartShopred"), for: UIControlState.normal)
            btnCart.titleLabel?.textColor = UIColor.black
            btnCart .setTitleColor(UIColor.black, for: .normal)
        }
        
        //self.productSkuArr = self.productDetail["ProductUsps"] as!  NSArray
        self.imgList = self.productDetail["imageList"] as!  NSArray
        
        //Manage Cell Count
        if imgList.count>=1
        {
            if imgList.count == 1
            {
                rowCount = 2
            }
            else if imgList.count>1
            {
                var cellImages = imgList.count / 2
                let rem = imgList.count % 2
                print(cellImages)
                cellImages = cellImages + 1 + rem
                rowCount = cellImages
            }
        }
        else
        {
            rowCount = 1
        }
        print("Count",rowCount)
        self.tbleView.reloadData()
        
    }
    

    //MARK: Table View Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return rowCount
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell:ProdDescCell = tableView.dequeueReusableCell(withIdentifier: "ProdDesc") as! ProdDescCell
//            cell.lblProdDesc.htmlText = self.productDetail["longDesc"] as! String
//            let uspList = ZamModel().stringFromHtml(string:  self.productDetail.value(forKey: "productUsps") as! String)
//            cell.lblProdUSP.attributedText = uspList
//            cell.lblProdUSP.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 18)

            
            if !(self.productDetail["longDesc"] is NSNull)
            {
                cell.lblDisplayDesc.text = "Product Description"
                
                cell.lblProdDesc.attributedText = ZamModel().stringFromHtml(string: self.productDetail["longDesc"] as! String)
                 cell.lblProdDesc.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 20)
                cell.lblProdDesc.textColor = ZamModel().themecolor
            }
            else
            {
                cell.lblProdDesc.attributedText = nil
                cell.lblDisplayDesc.text = ""
                
            }
            if !(self.productDetail.value(forKey: "productUsps") is NSNull)
            {
                cell.lblDispUSP.text = "Product USP"
                let uspList = ZamModel().stringFromHtml(string:  self.productDetail.value(forKey: "productUsps") as! String)
                cell.lblProdUSP.attributedText = uspList
                cell.lblProdUSP.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 20)
            }
            else
            {
               cell.lblDispUSP.text = nil
                cell.lblProdUSP.text = nil
            }
            return cell
        }
        else
        {
            let cell:ProdImgCell = tableView.dequeueReusableCell(withIdentifier: "ProdImg") as! ProdImgCell
            let index = ((indexPath.row-1)*2)/2
            if (self.imgList[index] as AnyObject).value(forKey: "imagePath") != nil
            {
                let url = NSURL(string:(self.imgList[index] as AnyObject).value(forKey: "imagePath") as! String)
                cell.btnShowImg1.sd_setImage(with: url as URL!, for: UIControlState.normal)

            }
            if index+1 < imgList.count
            {
             if(self.imgList[index+1] as AnyObject).value(forKey: "imagePath") != nil
                {
                    let url1 = NSURL(string:(self.imgList[index+1] as AnyObject).value(forKey: "imagePath") as! String)
                    cell.btnShowImg2.sd_setImage(with: url1 as URL!, for: UIControlState.normal)

                }
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        //return UITableViewAutomaticDimension
        return CGFloat(htforHeader)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row > 0
        {
            return 195
        }
        
        return UITableViewAutomaticDimension
    }
    //MARK: IB Action
    
    @IBAction func btnCartClicked(_ sender: Any)
    {
        if  self.productDetail["isCart"] as? NSNumber == 0
        {
            //Hit service
            let param = ["MarketPlaceID" : self.marketPlaceID,"PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String,"ProductSKU" : self.productDetail["prodSKU"] as! String, "offerID": self.productDetail["offerID"]as! NSNumber,"userID" : defaults.object(forKey: "userId") as! String,"quantity" : "1"] as [String : Any]
            
            print(param)
            self.addtoCart(parameters: param)
            
        }
        else
        {
            let story = UIStoryboard(name: "ShopStory", bundle: nil)
            let VC = story.instantiateViewController(withIdentifier: "CartListVC") as! CartListVC
            self.navigationController?.pushViewController(VC, animated: true)

        }

    }
    
    func addtoCart(parameters:[String:Any])
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().addToCart(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                //print(response?.object(forKey: "data") as! NSArray)
                self.btnCart.setTitle("Go to Cart", for: UIControlState.normal)
                self.btnCart.setImage(UIImage (named: "cartShopred"), for: UIControlState.normal)
                self.btnCart .setTitleColor(UIColor.black, for: .normal)
                self.productDetail["isCart"] = "1"
                 
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }

    @IBAction func btnCloseView(_ sender: Any)
    {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnReviewClicked(_ sender: Any)
    {
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        vc.marketplaceID = marketPlaceID
        vc.navigateShop = true
        vc.comeFromRateSuccess = false
        self.navigationController?.pushViewController(vc, animated: false)

    }
    
    @IBAction func btnRateClicked(_ sender: Any)
    {
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
        vc.navigateShop = true
        let parameters = ["MarketplaceID":self.marketPlaceID ]  as [String : Any]
        vc.parameter = parameters as NSDictionary
         vc.marketPlaceID = self.marketPlaceID
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnCallClicked(_ sender: Any)
    {
        
        let no = (self.productDetail["phoneNumber"] as! String).trimmingCharacters(in: .whitespaces)
        
        
       let phoneNo = no.replacingOccurrences(of: " ", with: "")
       
        if let url = NSURL(string: "tel://\(phoneNo)"), UIApplication.shared.canOpenURL(url as URL)
        {
            UIApplication.shared.openURL(url as URL)
        }
    }
   
    @IBAction func btnEmailClicked(_ sender: Any)
    {
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.delegate = self
        mailVC.setToRecipients([(self.productDetail["emailID"] as? String)!])
        mailVC.setSubject("Zam")
        mailVC.setMessageBody("Mail text", isHTML: false)
        if (MFMailComposeViewController.canSendMail()) {
            present(mailVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func showFullImg(_ sender: Any)
    {
        
        let controller:ImagePopViewController = ImagePopViewController(nibName: "ImagePopViewController", bundle: nil)
        controller.imageurl = self.productDetail["photoPath"] as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
        
        
//        self.openView.isHidden = false
//        self.viewFullimg.layer.borderColor = ZamModel().themecolor.cgColor
//        self.viewFullimg.layer.cornerRadius = 10
//        self.viewFullimg.layer.borderWidth = 2
//        let url = NSURL(string:(self.productDetail["photoPath"]) as! String)
//        self.imgFullImgView.sd_setImage(with: url as URL!)
        
    }
   
    
    @IBAction func closeFullImg(_ sender: Any)
    {
        self.openView.isHidden = true
    }
    
    //MARK: Email Composer

    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail sent")
        case MFMailComposeResult.failed.rawValue:
            print("Mail sent failure: %@", [error?.localizedDescription])
        default:
            break
        }
        self.dismiss(animated: true, completion: nil)
    }

}



