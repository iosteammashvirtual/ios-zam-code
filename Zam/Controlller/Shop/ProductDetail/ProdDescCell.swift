//
//  ProdDescCell.swift
//  Zam
//
//  Created by Admin on 17/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class ProdDescCell: UITableViewCell {

    @IBOutlet weak var lblProdUSP: UILabel!
  
    
    @IBOutlet weak var lblProdDesc: UILabel!
    @IBOutlet weak var lblDisplayDesc: UILabel!
    @IBOutlet weak var lblDispUSP: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
