//
//  TrainerSuccessRateViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerSuccessRateViewController: UIViewController {

    var trainerID = String()
    var marketplaceID = NSNumber()
    var courseID = NSNumber()
    var rateID = String()
    var navigate_Shop = false
    var navigarteCourse = false
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        ZamModel().buttonCorner(Button: btnNo, radius: 5)
        ZamModel().buttonCorner(Button: btnYes, radius: 5)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigate_Shop = false
        self.navigarteCourse = false
    }
    
    @IBAction func btnNoClicked(_ sender: Any)
    {
        if navigate_Shop
        {
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController!.popToViewController(dashboardVC, animated: true)
        }
        else if navigarteCourse
        {
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController!.popToViewController(dashboardVC, animated: true)
        }
        else
        {
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TrainerRateViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
 
        }
    
    }
    
    @IBAction func btnYesClicked(_ sender: Any)
    {
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        if navigate_Shop
        {
            vc.marketplaceID = self.marketplaceID
            vc.navigateShop = true
        }
        else if (navigarteCourse)
        {
            vc.courseID = self.courseID
            vc.navigateCourse = true
        }
        else
        {
            vc.trainerID = trainerID
            vc.navigateShop = false
        }
        vc.comeFromRateSuccess = true
        self.navigationController?.pushViewController(vc, animated: false)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
