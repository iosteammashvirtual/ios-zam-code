//
//  TrainerRateViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerRateViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    @IBOutlet weak var tblView: UITableView!

    @IBOutlet weak var lblRateNil: UILabel!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoOfRate: UILabel!
    @IBOutlet weak var lblOverAllRate: UILabel!
    @IBOutlet weak var btnOverAllRate: UIButton!
    var navigateShop = false
    var navigateCourse = false
    var comeFromReviewSuccess:Bool = false
    var parameter = NSDictionary()
    var trainerID = String()
    var marketPlaceID = NSNumber()
    var courseID = NSNumber()
    var dataArray = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
          self.tabBarController?.tabBar.isHidden = true
        let nibName = UINib(nibName: "CentreRateTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "CentreRateTableViewCell")
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        if navigateShop == true
        {
            self.getProductRating()
        }
        else if navigateCourse
        {
            
            //Nitika Commit
            //self.courseID = parameter["CenCourseID"]as! NSNumber
            self.getCourseRating()
        }
        else
        {
            getdata()// Trainer Rating
        }
    }
   
    override func viewDidDisappear(_ animated: Bool)
    {
//        self.navigateCourse = false
//        self.navigateShop = false
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:CentreRateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CentreRateTableViewCell") as! CentreRateTableViewCell
        cell.lblName.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "criteriaName") as? String
        cell.lblRateCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "criteriaRateValue") as! NSNumber)) + "/5"
        ZamModel().setSmileImageThemeColor(btn: cell.btnImageView, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "criteriaRateValue") as! NSNumber))
        
        return cell
    }
    
    
    func getdata()
    {
            AppEngine().getTrainerrrateData(parameter: self.parameter as NSDictionary)
            { (response : NSDictionary?) in
                if response == nil
                {
                    self.view.makeToast(message: ServerError)
                    return
                }
    
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                debugPrint(response)
                let data = response?.object(forKey: "data") as! NSDictionary
                self.dataArray = (data.object(forKey: "criteriaRates") as! NSArray).mutableCopy() as! NSMutableArray
                ZamModel().setSmileImageThemeColor(btn: self.btnOverAllRate, rateCount: Int((data).value(forKey: "overAllRateValue") as! NSNumber))
                self.lblOverAllRate.text = String(describing: (data).value(forKey: "overAllRateValue") as! NSNumber) + "/5"
                self.lblNoOfRate.text = String(describing: (data).value(forKey: "noOfRate") as! NSNumber)
                self.topView.isHidden = false
                self.lblRateNil.text = ""
                
                self.tblView.reloadData()
            }
            else
            {
                self.topView.isHidden = true
                self.lblRateNil.text = response?.object(forKey: "message") as! String
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    
    }
    
    func getProductRating()
    {
        print(parameter)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetProductRating(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true
            {
                // self.getdata()
                debugPrint(response)
                let data = response?.object(forKey: "data") as! NSDictionary
                self.marketPlaceID = data["marketPlaceID"] as! NSNumber
//                self.lblTitle.text = "Product Overview"
//                self.lblTitle.isHidden = true
                self.dataArray = (data.object(forKey: "criteriaRates") as! NSArray).mutableCopy() as! NSMutableArray
                ZamModel().setSmileImageThemeColor(btn: self.btnOverAllRate, rateCount: Int((data).value(forKey: "overAllRateValue") as! NSNumber))
                self.lblOverAllRate.text = String(describing: (data).value(forKey: "overAllRateValue") as! NSNumber) + "/5"
                self.lblNoOfRate.text = String(describing: (data).value(forKey: "noOfRate") as! NSNumber)
                self.topView.isHidden = false
                self.lblRateNil.text = ""
                self.tblView.reloadData()
            } else {
                self.topView.isHidden = true
                self.lblRateNil.text = response?.object(forKey: "message") as! String
                //  self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    
    func getCourseRating()
    {
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetCourseRating(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                debugPrint(response)
                let data = response?.object(forKey: "data") as! NSDictionary
                self.courseID = data["cenCourseID"] as! NSNumber
//                self.lblTitle.text = "Course Overview"
//                self.lblTitle.isHidden = true
                self.dataArray = (data.object(forKey: "criteriaRates") as! NSArray).mutableCopy() as! NSMutableArray
                ZamModel().setSmileImageThemeColor(btn: self.btnOverAllRate, rateCount: Int((data).value(forKey: "overAllRateValue") as! NSNumber))
                self.lblOverAllRate.text = String(describing: (data).value(forKey: "overAllRateValue") as! NSNumber) + "/5"
                self.lblNoOfRate.text = String(describing: (data).value(forKey: "noOfRate") as! NSNumber)
                self.topView.isHidden = false
                self.lblRateNil.text = ""
                self.tblView.reloadData()
            } else {
                self.topView.isHidden = true
                self.lblRateNil.text = response?.object(forKey: "message") as! String
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }

    }
    
    @IBAction func btnRateClicked(_ sender: Any) {
   
        let vc = TrainerRateNowViewController(nibName: "TrainerRateNowViewController", bundle: nil)
        if(navigateShop == true)
        {
            let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"marketPlaceID" : self.marketPlaceID] as [String : Any]
            vc.marketPlaceID = self.marketPlaceID
            vc.parameter = parameters as NSDictionary
            vc.navigate_Shop = true
        }
        else if (navigateCourse)
        {
            let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"cenCourseID" : self.courseID] as [String : Any]
            vc.courseId = self.courseID
            vc.parameter = parameters as NSDictionary
            vc.navigateCourse = true

        }
        else
        {
             let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerID" : trainerID]
             vc.trainerID = trainerID
             vc.parameter = parameters as NSDictionary
             vc.navigate_Shop = false
            
        }
        vc.comeFromReviewSuccess = comeFromReviewSuccess
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func btnBackClicked(_ sender: Any) {
        
        
        if comeFromReviewSuccess
        {
            if navigateShop
            {
                print(self.navigationController!.viewControllers.first)
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if navigateCourse
            {
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else
            {
                if  self.appDelegate.comeFromTrainerReview == "trainercontroller"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }else if  self.appDelegate.comeFromTrainerReview == "trainerprofile"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterTrainerProfileViewController }.first!
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }
            }
            
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
            
        }
        
    }
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
