//
//  RateRecommendTrainerViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class RateRecommendTrainerViewController: UIViewController {

    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
    var rateID = String()
    var navigateShop = false
    var navigateCourse = false
    var parameters = NSDictionary()
    var trainerID = String()
    var courseId = NSNumber()
    var marketplaceID = NSNumber()
    
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ZamModel().buttonCorner(Button: btnYes, radius: 5)
        ZamModel().buttonCorner(Button: btnNo, radius: 5)
        if navigateShop
        {
            self.lblTitle.text = "Would you recommend this Product to others"
        }
        else if (navigateCourse)
        {
            
            self.lblTitle.text = "Would you recommend this Course to others"
        }
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.navigateCourse = false
        self.navigateShop = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnNoClicked(_ sender: Any)
    {
      if self.navigateShop
      {
        
        parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"MarketplaceID" : marketplaceID,"RateID":rateID,"IsRecomand": "false"] as [String : Any] as NSDictionary
       self.updateProductRecomend()
      }
        else if self.navigateCourse
        {
            parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CenCourseID" : courseId,"RateID":rateID,"IsRecomand": "false"] as [String : Any] as NSDictionary
            self.updateCourseRecomend()
        }
      else
      {
        parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerID" : trainerID,"RateID":rateID,"IsRecomand": "false"] as [String : Any] as NSDictionary
        callapi()
      }

    }
    @IBAction func btnYesClicked(_ sender: Any)
    {
        if self.navigateShop
        {
            parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"MarketplaceID" : marketplaceID,"RateID":rateID,"IsRecomand": "true"] as [String : Any] as NSDictionary
            self.updateProductRecomend()
        }
        else if self.navigateCourse
        {
            parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CenCourseID" : courseId,"RateID":rateID,"IsRecomand": "true"] as [String : Any] as NSDictionary
            self.updateCourseRecomend()
        }
        else
        {
            parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as!   String,"TrainerID" : trainerID,"RateID":rateID,"IsRecomand": "true"] as [String : Any] as NSDictionary
            callapi()
        }
    }
    
    func callapi(){
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().setTrainerRecomandation(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true
            {
                
                    let vc = TrainerSuccessRateViewController(nibName: "TrainerSuccessRateViewController", bundle: nil)
                    vc.trainerID = self.trainerID
                    self.navigationController?.pushViewController(vc, animated: true)

                
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    func updateProductRecomend()
    {
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().UpdateProductRecomandation(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
           self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true
            {
                // self.getdata()
                let vc = TrainerSuccessRateViewController(nibName: "TrainerSuccessRateViewController", bundle: nil)
                if(self.navigateShop)
                {
                      vc.marketplaceID = self.marketplaceID
                        vc.navigate_Shop = true
                }
                self.navigationController?.pushViewController(vc, animated: true)
            } else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    func updateCourseRecomend()
    {
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().UpdateCourseRecomandation(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true
            {
                // self.getdata()
                let vc = TrainerSuccessRateViewController(nibName: "TrainerSuccessRateViewController", bundle: nil)
                
                    vc.courseID = self.courseId
                    vc.navigarteCourse = true
                self.navigationController?.pushViewController(vc, animated: true)
            } else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
