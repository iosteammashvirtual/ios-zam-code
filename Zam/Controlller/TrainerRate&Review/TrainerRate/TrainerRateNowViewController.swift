//
//  TrainerRateNowViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerRateNowViewController: UIViewController {
    
    @IBOutlet weak var lblrateTitle: UILabel!
    @IBOutlet weak var btn4: UIButton!
    @IBOutlet weak var btn5: UIButton!
    @IBOutlet weak var btn3: UIButton!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btnFinish: UIButton!
    var selectedValue = 0
    var trainerID = String()
    var marketPlaceID = NSNumber()
    var courseId = NSNumber()
    var parameter = NSDictionary()
    var comeFromReviewSuccess:Bool = false
    var navigate_Shop = false
    var navigateCourse = false
    var rateSelected : Bool = false
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var dataArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ZamModel().buttonCorner(Button: btnFinish, radius: Int(5))
        NotificationCenter.default.addObserver(self, selector: #selector(TrainerRateNowViewController.PopYesClicked(notification:)), name: Notification.Name("TrainerRateNowYesClicked"), object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool)
    {
        print(parameter)
        btnFinish.isHidden = true
        if navigate_Shop == true
        {
            self.getProductRateCriteria()
        }
        else if(navigateCourse)
        {
            self.getCourseRateCriteria()
        }
        else
        {
            getdata()
        }
        
    }
    override func viewDidDisappear(_ animated: Bool)
    {
        self.navigateCourse = false
        self.navigate_Shop = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btn5Clicked(_ sender: Any) {
        setImage(sender: sender as! UIButton)
    }
    @IBAction func btn1Clicked(_ sender: Any) {
        setImage(sender: sender as! UIButton)
    }
    @IBAction func btn2Clicked(_ sender: Any) {
        setImage(sender: sender as! UIButton)
    }
    @IBAction func btn3Clicked(_ sender: Any) {
        setImage(sender: sender as! UIButton)
    }
    @IBAction func btn4Clicked(_ sender: Any) {
        setImage(sender: sender as! UIButton)
    }
    
    func setImage(sender:UIButton){
        selectedValue = sender.tag
        btnFinish.isHidden = false
        if sender.tag == 0{
            btn1.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn2.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn3.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if sender.tag == 1{
            btn1.setImage(UIImage(named:"angry50.png"), for: .normal)
            btn2.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn3.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if sender.tag == 2{
            btn1.setImage(UIImage(named:"sad50.png"), for: .normal)
            btn2.setImage(UIImage(named:"sad50.png"), for: .normal)
            btn3.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if sender.tag == 3{
            
            btn1.setImage(UIImage(named:"natural50.png"), for: .normal)
            btn2.setImage(UIImage(named:"natural50.png"), for: .normal)
            btn3.setImage(UIImage(named:"natural50.png"), for: .normal)
            btn4.setImage(UIImage(named:"happy50B.png"), for: .normal)
            btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if sender.tag == 4{
            btn1.setImage(UIImage(named:"happy50.png"), for: .normal)
            btn2.setImage(UIImage(named:"happy50.png"), for: .normal)
            btn3.setImage(UIImage(named:"happy50.png"), for: .normal)
            btn4.setImage(UIImage(named:"happy50.png"), for: .normal)
            btn5.setImage(UIImage(named:"happy50B.png"), for: .normal)
            
        }else if sender.tag == 5{
            btn1.setImage(UIImage(named:"awesome50.png"), for: .normal)
            btn2.setImage(UIImage(named:"awesome50.png"), for: .normal)
            btn3.setImage(UIImage(named:"awesome50.png"), for: .normal)
            btn4.setImage(UIImage(named:"awesome50.png"), for: .normal)
            btn5.setImage(UIImage(named:"awesome50.png"), for: .normal)
            
        }
        
        
    }
    
    
    func getdata(){
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getTrainerRatingCriteria(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                // debugPrint(response)
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.lblrateTitle.text = (self.dataArray[0] as AnyObject).value(forKey: "cTitle") as? String
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    func getProductRateCriteria()
    {
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().GetProductRatingCriteria(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                print(self.dataArray)
                self.lblrateTitle.text = (self.dataArray[0] as AnyObject).value(forKey: "cTitle") as? String
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    func getCourseRateCriteria()
    {
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().CourseRatingCriteria(parameter: parameter as NSDictionary) { (response : NSDictionary?) in
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            self.view.hideToastActivity(view: self.view)
            if response?.object(forKey: "success") as! Bool  == true{
                
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                print(self.dataArray)
                self.lblrateTitle.text = (self.dataArray[0] as AnyObject).value(forKey: "cTitle") as? String
                
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any)
    {
        let controller: AlertConfirmViewController =  AlertConfirmViewController(nibName: "AlertConfirmViewController", bundle: nil)
        controller.message = cancelRate
        controller.comFromTrainerRate = true
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
        
        // self.navigationController?.popViewController(animated: true)
    }
    
    func PopYesClicked(notification : Notification){
        
        
        if comeFromReviewSuccess
        {
            if navigate_Shop
            {
                print(self.navigationController!.viewControllers.first)
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if navigateCourse
            {
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else
            {
                if  self.appDelegate.comeFromTrainerReview == "trainercontroller"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }else if  self.appDelegate.comeFromTrainerReview == "trainerprofile"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterTrainerProfileViewController }.first!
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }
            }
            
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        
        }
        
        
        
      
    }
    
    
    @IBAction func btnFinishClicked(_ sender: Any)
    {
        
        let criterialArray = NSMutableArray()
        if navigate_Shop == true
        {
            for i in 0..<self.dataArray.count
            {
                var dataStr = String()
                let criteriaOption =  (dataArray.object(at: i) as AnyObject).object(forKey: "criteriaOption") as? NSArray
                
                dataStr = dataStr + String(describing: ((dataArray.object(at: i) as AnyObject).value(forKey: "criteriaID") as! NSNumber)) + ","
                
                dataStr = dataStr + String(describing:selectedValue  as! NSNumber) + ","
                
                dataStr = dataStr + String(describing: ((criteriaOption?[0] as AnyObject).value(forKey: "criteriaOptID") as! NSNumber)) + ","
                
                criterialArray.add(dataStr)
            }
            
            let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"MarketplaceID":self.marketPlaceID,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"CriteriaAnsNew[]":criterialArray] as [String : Any]
            
            print(parameters)
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().AddProductRating(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil
                {
                    self.view.makeToast(message: ServerError)
                    return
                }
                self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "status") as! Bool  == true
                {
                    // self.getdata()
                    debugPrint(response)
                    if self.comeFromReviewSuccess
                    {
                        let vc = TrainerRateReviewSuccessViewController(nibName: "TrainerRateReviewSuccessViewController", bundle: nil)
                        vc.comeFromReviewSuccess = self.comeFromReviewSuccess
                        if self.navigate_Shop
                        {
                            vc.navigate_Shop = true
                        }
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    else
                    {
                        let vc = RateRecommendTrainerViewController(nibName: "RateRecommendTrainerViewController", bundle: nil)
                        vc.trainerID = self.trainerID
                        
                        if self.navigate_Shop
                        {
                            vc.navigateShop = true
                            vc.marketplaceID = self.marketPlaceID
                        }
                        vc.rateID = String(describing: response?.object(forKey: "rateID") as! NSNumber)
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                } else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }}
            
        }
        else if (navigateCourse)
        {
            for i in 0..<self.dataArray.count
            {
                
                var dataStr = String()
                let criteriaOption =  (dataArray.object(at: i) as AnyObject).object(forKey: "criteriaOptions") as? NSArray
                
                dataStr = dataStr + String(describing: ((dataArray.object(at: i) as AnyObject).value(forKey: "criteriaID") as! NSNumber)) + ","
                
                dataStr = dataStr + String(describing:selectedValue  as! NSNumber) + ","
                
                dataStr = dataStr + String(describing: ((criteriaOption?[0] as AnyObject).value(forKey: "criteriaOptID") as! NSNumber)) + ","
                
                criterialArray.add(dataStr)
                
                
                
            }
            let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CenCourseID":self.courseId,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"CriteriaAnsNew[]":criterialArray] as [String : Any]
            print (parameters)
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().CourseAddRating(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil
                {
                    self.view.makeToast(message: ServerError)
                    return
                }
                self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "status") as! Bool  == true
                {
                    // self.getdata()
                    debugPrint(response)
                    if self.comeFromReviewSuccess
                    {
                        let vc = TrainerRateReviewSuccessViewController(nibName: "TrainerRateReviewSuccessViewController", bundle: nil)
                        vc.comeFromReviewSuccess = self.comeFromReviewSuccess
                        if self.navigate_Shop
                        {
                            vc.navigate_Shop = true
                        }
                        else if self.navigateCourse
                        {
                            vc.navigateCourse = true
                        }
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    else
                    {
                        let vc = RateRecommendTrainerViewController(nibName: "RateRecommendTrainerViewController", bundle: nil)
                        vc.trainerID = self.trainerID
                        
                        if self.navigate_Shop
                        {
                            vc.navigateShop = true
                            vc.marketplaceID = self.marketPlaceID
                        }
                        else if(self.navigateCourse)
                        {
                            vc.navigateCourse = true
                            vc.courseId = self.courseId
                        }
                        vc.rateID = String(describing: response?.object(forKey: "rateID") as! NSNumber)
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                } else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }}
        }
        else
        {
            for i in 0..<self.dataArray.count
            {
                var dataStr = String()
                let criteriaOption =  (dataArray.object(at: i) as AnyObject).object(forKey: "criteriaOptions") as? NSArray
                dataStr = dataStr + String(describing: ((dataArray.object(at: i) as AnyObject).value(forKey: "criteriaID") as! NSNumber)) + ","
                dataStr = dataStr + String(describing:selectedValue  as! NSNumber) + ","
                dataStr = dataStr + String(describing: ((criteriaOption?[0] as AnyObject).value(forKey: "criteriaOptID") as! NSNumber)) + ","
                criterialArray.add(dataStr)
            }
            let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerID" : trainerID,"userID":UserDefaults.standard.object(forKey: "userId") as! String,"CriteriaAnsNew[]":criterialArray] as [String : Any]
            
            self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().addtrainerRating(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                if response == nil
                {
                    self.view.makeToast(message: ServerError)
                    return
                }
                self.view.hideToastActivity(view: self.view)
                if response?.object(forKey: "status") as! Bool  == true
                {
                    // self.getdata()
                    if self.comeFromReviewSuccess
                    {
                        let vc = TrainerRateReviewSuccessViewController(nibName: "TrainerRateReviewSuccessViewController", bundle: nil)
                        vc.comeFromReviewSuccess = self.comeFromReviewSuccess
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    else
                    {
                        let vc = RateRecommendTrainerViewController(nibName: "RateRecommendTrainerViewController", bundle: nil)
                        vc.trainerID = self.trainerID
                        vc.rateID = String(describing: response?.object(forKey: "rateID") as! NSNumber)
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                } else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }}
            
        }
        
        
    }
    
    
}


