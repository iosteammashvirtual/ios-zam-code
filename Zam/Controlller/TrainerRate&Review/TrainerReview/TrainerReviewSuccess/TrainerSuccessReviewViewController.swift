//
//  TrainerSuccessReviewViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerSuccessReviewViewController: UIViewController {

    @IBOutlet weak var btnYes: UIButton!
    var trainerID = String()
    var marketplaceID = NSNumber()
    var courseId = NSNumber()
    var navigateShop = false
    var navigateCourse = false
    @IBOutlet weak var btnNo: UIButton!
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        ZamModel().buttonCorner(Button: btnNo, radius: 5)
        ZamModel().buttonCorner(Button: btnYes, radius: 5)
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.navigateCourse = false
        self.navigateShop = false
    }
    @IBAction func btnNoClicked(_ sender: Any)
    {

        if self.navigationController!.viewControllers.contains(TutorViewController())
        {
            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
            self.navigationController!.popToViewController(dashboardVC, animated: true)
             self.tabBarController?.tabBar.isHidden = false
            
        }
        else
        {
            if navigateShop
            {
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if navigateCourse
            {
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else
            {
                
                if  appDelegate.comeFromTrainerReview == "trainercontroller"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                
                }else if  appDelegate.comeFromTrainerReview == "trainerprofile"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterTrainerProfileViewController }.first!
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                
                }
               
            }
            
        }
       
        
        
        //        let vc = CompareViewController(nibName: "CompareViewController", bundle: nil)
        //
        //        self.navigationController?.pushViewController(vc, animated: false)
        
        
        
    }
    @IBAction func btnYesClicked(_ sender: Any)
    {
       
        if navigateShop
        {
            let parameters = ["MarketplaceID" : self.marketplaceID]
            let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
            vc.parameter = parameters as NSDictionary
            vc.comeFromReviewSuccess = true
            vc.navigateShop = true
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: false)

        }
        else if navigateCourse
        {
            let parameters = ["cenCourseID" : self.courseId]
            let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
            vc.parameter = parameters as NSDictionary
            vc.comeFromReviewSuccess = true
            vc.navigateCourse = true
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: false)
            
        }
        else
        {
            let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerID" : String(describing: trainerID),"userID":UserDefaults.standard.object(forKey: "userId") as! String,]
            let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
            vc.parameter = parameters as NSDictionary
            vc.comeFromReviewSuccess = true
            vc.trainerID = String(describing: trainerID)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
   
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
