//
//  TrainerReviewViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerReviewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    var comeFromRateSuccess:Bool = false

    @IBOutlet weak var lblRateNil: UILabel!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtView: UITextView!
    let defaults = UserDefaults.standard
    var marketplaceID = NSNumber()
    var courseID = NSNumber()
    var navigateShop = false
    var navigateCourse = false
    var trainerID = String()
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var lblCharacterRemaining: UILabel!
   
    var dataArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
          self.tabBarController?.tabBar.isHidden = true
        let nibName = UINib(nibName: "CenterReviewTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "CenterReviewTableViewCell")
        // Do any additional setup after loading the view.
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 100
        txtView.delegate = self
        ZamModel().textViewCorner(txtview: txtView, radius: 10)
         navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewWillAppear(_ animated: Bool)
    {
        if navigateShop
        {
            self.getProductReview()
        }
        else if (navigateCourse)
        {
            self.getCourseReview()
        }
        else
        {
            getdata()
        }
        
    }
    override func viewDidDisappear(_ animated: Bool)
    {
//        self.navigateCourse = false
//        self.navigateShop = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - table View Delegate & Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        
        print(dataArray[indexPath.row])
        let cell:CenterReviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CenterReviewTableViewCell") as! CenterReviewTableViewCell
        ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImg, radius: Int(cell.btnImg.frame.size.height / 2), borderWidth: 2)
        cell.lblName.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewBy") as? String
        cell.lblDate.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewDate") as? String
        cell.lblDesc.text = (dataArray.object(at: indexPath.row) as AnyObject).value(forKey: "reviewComment") as? String
        if navigateShop || navigateCourse{
        }else{
            let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "photoPath") as! String
            cell.btnImg.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
        }
        
           // cell.lblName.text = dataArray[indexPath.row]
        return cell
    }
    
    func getdata(){
       
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let  parameters = ["userid" : UserDefaults.standard.object(forKey: "userId") as! String,"TrainerID" : trainerID] as [String : Any] as NSDictionary
        AppEngine().getTrainerReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                if response?.object(forKey: "data") != nil  {
                    self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    self.tblView.reloadData()
                     self.lblRateNil.text = ""
                }
            } else {
                 self.lblRateNil.text = response?.object(forKey: "message") as! String
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    func getProductReview(){
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let  parameters = ["userid" : UserDefaults.standard.object(forKey: "userId") as! String,"MarketplaceID" : self.marketplaceID] as [String : Any] as NSDictionary
        AppEngine().GetProductReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                //self.view.makeToast(message: "No review")
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                debugPrint(response)
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.tblView.reloadData()
                self.lblRateNil.text = ""
            } else {
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.lblRateNil.text = response?.object(forKey: "message") as! String
            }
        }
    }

    func getCourseReview(){
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let  parameters = ["PrimaryInfoID":defaults.object(forKey: "primaryInfoId") as! String,"CenCourseID" : self.courseID] as [String : Any] as NSDictionary
        AppEngine().GetCourseReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                //self.view.makeToast(message: "No review")
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                // self.getdata()
                debugPrint(response)
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.tblView.reloadData()
                 self.lblRateNil.text = ""
            } else {
                 self.lblRateNil.text = response?.object(forKey: "message") as! String
              //  self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }

    //MARK:- TextView Delegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        let newLength = textView.text.characters.count + text.characters.count - range.length
        if newLength <= 200{
            lblCharacterRemaining.text = "Characters remaining : " +  String( 200 - newLength)
            return true
        }else {
            return false
        }        
       
    }
    @IBAction func btnPostClicked(_ sender: Any) {
            txtView.resignFirstResponder()
        if ZamModel().condenseWhitespace(str: txtView.text) == ""{
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = ReviewNil
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        if navigateShop
        {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            let  parameters = ["userId" : UserDefaults.standard.object(forKey: "userId") as! String,"MarketplaceID" : self.marketplaceID,"ReviewType":"Product","ReviewComment":txtView.text!] as [String : Any] as NSDictionary
            
            AppEngine().AddProductReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                
                if response?.object(forKey: "success") as! Bool  == true
                {
                    
                    if self.comeFromRateSuccess
                    {
                        let vc = TrainerRateReviewSuccessViewController(nibName: "TrainerRateReviewSuccessViewController", bundle: nil)
                        if self.navigateShop
                        {
                            vc.navigate_Shop = true
                        }
                                               //  vc.comeFromRateSuccess = self.comeFromRateSuccess
                        // self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else
                    {
                        let vc = TrainerSuccessReviewViewController(nibName: "TrainerSuccessReviewViewController", bundle: nil)
                        
                        if self.navigateShop
                        {
                            vc.navigateShop = true
                            vc.marketplaceID = self.marketplaceID
                        }
                        // self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                } else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }
        else if(self.navigateCourse)
        {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            let  parameters = ["userId" : UserDefaults.standard.object(forKey: "userId") as! String,"CenCourseID" :self.courseID,"ReviewType":"Course","ReviewComment":txtView.text!] as [String : Any] as NSDictionary
            
            AppEngine().AddCourseReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                
                if response?.object(forKey: "success") as! Bool  == true{
                    
                    if self.comeFromRateSuccess{
                        let vc = TrainerRateReviewSuccessViewController(nibName: "TrainerRateReviewSuccessViewController", bundle: nil)
                        if self.navigateCourse
                        {
                            vc.navigateCourse = true
                        }

                        self.navigationController?.pushViewController(vc, animated: false)
                    }else
                    {
                        let vc = TrainerSuccessReviewViewController(nibName: "TrainerSuccessReviewViewController", bundle: nil)
                       vc.navigateCourse = true
                        vc.courseId = self.courseID
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                } else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }

        }
        else
        {
            self.view.makeToastActivity(message: "Loading", view: self.view)
            let  parameters = ["userId" : UserDefaults.standard.object(forKey: "userId") as! String,"TrainerID" : trainerID,"ReviewType":"Trainer","ReviewComment":txtView.text!] as [String : Any] as NSDictionary
            
            AppEngine().setTrainerReview(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                
                if response?.object(forKey: "success") as! Bool  == true{
                    
                    if self.comeFromRateSuccess{
                        let vc = TrainerRateReviewSuccessViewController(nibName: "TrainerRateReviewSuccessViewController", bundle: nil)
                        //  vc.comeFromRateSuccess = self.comeFromRateSuccess
                        // self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(vc, animated: false)
                    }else {
                        let vc = TrainerSuccessReviewViewController(nibName: "TrainerSuccessReviewViewController", bundle: nil)
                        vc.trainerID = String(describing: self.trainerID)
                        // self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                } else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
        }
        
    }
    
   
    @IBAction func btnBackClicked(_ sender: Any)
    {
        if comeFromRateSuccess
        {
            if navigateShop
                {
                   // print(self.navigationController!.viewControllers.first)
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                }
                else if navigateCourse
                {
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                }
                else
                {
                    if  self.appDelegate.comeFromTrainerRate == "trainercontroller"{
                        let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
                        self.navigationController!.popToViewController(dashboardVC, animated: true)
                        
                    }else if  self.appDelegate.comeFromTrainerRate == "trainerprofile"{
                        let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterTrainerProfileViewController }.first!
                        self.tabBarController?.tabBar.isHidden = false
                        self.navigationController!.popToViewController(dashboardVC, animated: true)
                        
                    }
                }
            
        }else {
            self.tabBarController?.tabBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
