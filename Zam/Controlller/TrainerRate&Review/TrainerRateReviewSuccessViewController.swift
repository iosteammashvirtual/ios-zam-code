//
//  TrainerRateReviewSuccessViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/25/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TrainerRateReviewSuccessViewController: UIViewController {

    @IBOutlet weak var btnGreat: UIButton!
    @IBOutlet weak var lblReviewText: UILabel!
    @IBOutlet weak var lblTopTitle: UILabel!
    var comeFromReviewSuccess : Bool = false
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var navigate_Shop = false
    var navigateCourse = false
    override func viewWillAppear(_ animated: Bool) {
        
        ZamModel().buttonCornerWithThemeBorder(Button: btnGreat, radius: 5, borderWidth: 1)
        if comeFromReviewSuccess {
            lblTopTitle.text = "Rate"
            lblReviewText.text = "Your rating has successfully been submitted"
            
        }else {
            lblTopTitle.text = "Review"
            lblReviewText.text = "Your review will be published after approval"
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnGreatClicked(_ sender: Any)
    {
        
          if comeFromReviewSuccess
          {
            if navigate_Shop
            {
                print(self.navigationController!.viewControllers.first)
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if navigateCourse
            {
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else
            {
                if  appDelegate.comeFromTrainerReview == "trainercontroller"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }else if  appDelegate.comeFromTrainerReview == "trainerprofile"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterTrainerProfileViewController }.first!
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }
            }
            
          }
        else
          {
            if navigate_Shop
            {
                 let dashboardVC = self.navigationController!.viewControllers.filter { $0 is ShopViewController }.first!
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else if(navigateCourse)
            {
                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is SupportedExamsViewController }.first!
                self.tabBarController?.tabBar.isHidden = false
                self.navigationController!.popToViewController(dashboardVC, animated: true)
            }
            else
            {
                if  appDelegate.comeFromTrainerRate == "trainercontroller"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is TutorViewController }.first!
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }else if  appDelegate.comeFromTrainerRate == "trainerprofile"{
                    let dashboardVC = self.navigationController!.viewControllers.filter { $0 is CenterTrainerProfileViewController }.first!
                    self.tabBarController?.tabBar.isHidden = false
                    self.navigationController!.popToViewController(dashboardVC, animated: true)
                    
                }
            }
                    }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
