//
//  AlertViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/2/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    
    @IBOutlet weak var alertview: UIView!
    @IBOutlet weak var btnOk: UIButton!
    @IBOutlet weak var lblMessage: UILabel!
    var message = String()
    var comeFromChangePass = false
    override func viewDidLoad() {
        super.viewDidLoad()
        lblMessage.sizeToFit()
        ZamModel().buttonCorner(Button: btnOk, radius: Int(5.0))
         ZamModel().viewCorner(Button: alertview, radius: Int(5.0))
       

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        let myMutableString = NSMutableAttributedString(
            string: message,
            attributes: [NSFontAttributeName:UIFont(
                name: "HelveticaNeueLTStd-Cn",
                size: 18.0)!])
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8 // Whatever line spacing you want in points
        myMutableString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, myMutableString.length))
         lblMessage.textColor = ZamModel().themecolor
        lblMessage.attributedText = myMutableString
        lblMessage.textAlignment = .center
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func okClicked(_ sender: Any) {
        
        if comeFromChangePass == true{
          NotificationCenter.default.post(name: Notification.Name("ChangePassword"), object: nil)
        }
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
   
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
