//
//  AlertConfirmViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/10/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit


class AlertConfirmViewController: UIViewController {

    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnYes: UIButton!
   
    @IBOutlet weak var alertview: UIView!
    var comFromCompare:Bool = false
    var comFromCenterRate = false
    var comFromTrainerRate = false

    @IBOutlet weak var lblMessage: UILabel!
    var message = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        ZamModel().buttonCorner(Button: btnYes, radius: Int(10.0))
         ZamModel().buttonCorner(Button: btnNo, radius: Int(10.0))
         ZamModel().viewCorner(Button: alertview, radius: Int(5.0))
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         lblMessage.text = message
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnNoClicked(_ sender: Any) {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    @IBAction func btnYesClicked(_ sender: Any) {
        
        if comFromTrainerRate == true{
          NotificationCenter.default.post(name: Notification.Name("TrainerRateNowYesClicked"), object: nil)
        }
        else if comFromCenterRate == true{
             NotificationCenter.default.post(name: Notification.Name("RateNowYesClicked"), object: nil)
        }
        else if comFromCompare == true{
         NotificationCenter.default.post(name: Notification.Name("CompareYesClicked"), object: nil)
        }else {
            NotificationCenter.default.post(name: Notification.Name("PopToProfile"), object: nil)
        }
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
