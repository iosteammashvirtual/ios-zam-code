//
//  ContactAlertViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 2/3/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import MessageUI

class ContactAlertViewController: UIViewController,UINavigationControllerDelegate,UIPopoverPresentationControllerDelegate ,MFMailComposeViewControllerDelegate {

    @IBOutlet weak var btnEmail: UIButton!
    var email = String()
    let mailVC = MFMailComposeViewController()

    var phoneNo = String()
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var alertview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ZamModel().buttonCorner(Button: btnCall, radius: Int(10.0))
        ZamModel().buttonCorner(Button: btnEmail, radius: Int(10.0))
        ZamModel().viewCorner(Button: alertview, radius: Int(5.0))
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func btnCallClicked(_ sender: Any) {
        
        if let url = NSURL(string: "tel://\(phoneNo)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.openURL(url as URL)
        }
    }
    @IBAction func btnEmailClicked(_ sender: Any) {
  
            mailVC.mailComposeDelegate = self
            mailVC.delegate = self
            mailVC.setToRecipients([email])
            mailVC.setSubject("Subject for email")
            mailVC.setMessageBody("Email message string", isHTML: false)
            if (MFMailComposeViewController.canSendMail()) {
                present(mailVC, animated: true, completion: nil)
            }
        }
        func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
            switch result.rawValue {
            case MFMailComposeResult.cancelled.rawValue:
                print("Mail cancelled")
            case MFMailComposeResult.saved.rawValue:
                print("Mail saved")
            case MFMailComposeResult.sent.rawValue:
                print("Mail sent")
            case MFMailComposeResult.failed.rawValue:
                print("Mail sent failure: %@", [error?.localizedDescription])
            default:
                break
            }
          
            self.dismiss(animated: true, completion: nil)
             closeView()
            
        }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
           closeView()
        }
        super.touchesBegan(touches, with: event)
    }
    
    func closeView(){
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    
}
