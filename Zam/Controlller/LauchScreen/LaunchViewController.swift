//
//  LaunchViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/2/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    @IBOutlet weak var ImageView: UIView!
 let defaults = UserDefaults.standard
    @IBOutlet weak var centerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
      navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if let name = defaults.string(forKey: "token") {
              DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = appDelegate.tabBarController
            }
        }else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let vc = RegistrationViewController(nibName: "RegistrationViewController", bundle: nil)
                self.navigationController?.pushViewController(vc, animated: false)
            }
        
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidLayoutSubviews() {
      //  ZamModel().GradiantColor(view:ImageView)
         //self.ImageView.bringSubview(toFront: centerView)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
