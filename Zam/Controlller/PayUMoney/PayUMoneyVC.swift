//
//  PayUMoneyVC.swift
//  Zam
//
//  Created by Admin on 24/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class PayUMoneyVC: UIViewController,UIWebViewDelegate
{
    
    var urlStr = String()
    @IBOutlet weak var webView: UIWebView!
    var paymentDict = [String:AnyObject]()
    
    var timer = Timer()
    //        Timer.scheduledTimer(timeInterval: 15.0, target: self, selector: #selector(closeView), userInfo: nil, repeats: true);
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if(timer != nil)
        {
            timer.invalidate()
        }
        self.setUpPayment()
        
        // webView.loadRequest(NSURLRequest(url: NSURL(string: urlStr)! as URL) as URLRequest)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpPayment()
    {
        self.webView.delegate = self
        let i = arc4random()
        print("Random val=",i)
        let merchantKey = self.paymentDict["merchantKey"]as! String
        let salt = self.paymentDict["salt"]as! String
        let PayUBaseUrl = self.paymentDict["payUURL"]as! String
        
        let amount = String(describing:self.paymentDict["totalAmount"]as! NSNumber)
        let productInfo = self.paymentDict["productName"]as! String
        let firstName = self.paymentDict["name"]as! String
        let email = self.paymentDict["email"]as! String
        let phone = String(describing:self.paymentDict["phone"]as! String)
        let sUrl = self.paymentDict["sucessURL"]as! String
        let fUrl = self.paymentDict["failURL"]as! String
        let service_provider = "payu_paisa"
        //let str_i = 505008977
        let str = String.localizedStringWithFormat("%d%@",i,NSDate())
        print(str )
        // let str1 = "505008977Wednesday, 25 January 2017 at 6:51:34 PM India Standard Time"
        let strHash:String = str.digest(length: CC_SHA512_DIGEST_LENGTH, gen: {(data, len, md) in CC_SHA512(data,len,md)})
        
        
        let txnid1: String? = (strHash as? NSString)?.substring(to: 20)
        
        let hashValue = String.localizedStringWithFormat("%@|%@|%@|%@|%@|%@|||||||||||%@",merchantKey,txnid1!,amount,productInfo,firstName,email,salt)
        
        let hash = hashValue.digest(length: CC_SHA512_DIGEST_LENGTH, gen: {(data, len, md) in CC_SHA512(data,len,md)})
        print("Hash val ",hash)
        let exp1 = String.localizedStringWithFormat("txnid=%@&key=%@&amount=%@&productinfo=%@&firstname=%@&email=%@&phone=%@&surl=%@&furl=%@&hash=%@&service_provider=%@",txnid1!,merchantKey,amount,productInfo,firstName,email,phone,sUrl,fUrl,hash,service_provider)
        
        let postStr = exp1 //"txnid=" + txnid1 + "&key=" + merchantKey + "&amount=" + amount + "&productinfo=" + productInfo + "&firstname=" + firstName + "&email=" + email + "&phone=" + phone + "&surl=" + sUrl + "&furl=" + fUrl + "&hash=" + hash + "&service_provider=" + service_provider
        
        print(postStr)
        
        
        //let postStr =  "txnid=" + txnid1 + "&key=" + merchantKey + "&amount=" + amount + "&productinfo=" + productInfo + "&firstname=" + firstName + "&email=" + email + "&phone=" + phone + "&surl=" + sUrl + "&furl=" + fUrl + "&hash=" + hash + "&service_provider=" + service_provider
        
        let url = NSURL(string: String.localizedStringWithFormat("%@/_payment", PayUBaseUrl))
        
        print("check my url", url, postStr)
        
        let request = NSMutableURLRequest(url: url! as URL)
        
        do {
            
            let postLength = String.localizedStringWithFormat("%lu",postStr.characters.count)
            request.httpMethod = "POST"
            request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Current-Type")
            request.setValue(postLength, forHTTPHeaderField: "Content-Length")
            request.httpBody = postStr.data(using: String.Encoding.utf8)
            webView.loadRequest(request as URLRequest)
        } catch
        {
            
        }
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
        let requestURL = self.webView.request
        let requestString = String(describing:requestURL!)
        //  let requestString =  String.localizedStringWithFormat("%@",requestURL as! URL as CVarArg)
        
        if requestString.contains("https://secure.payu.in//_payment")
        {
            print("success payment done")
        }
        else if requestString.contains("https://www.bing.com") {
            print("payment failure")
        }
        else if requestString.contains("PaymentStat.aspx")
        {
            timer = Timer.scheduledTimer(timeInterval: 5, target:self, selector: #selector(PayUMoneyVC.closeView), userInfo: nil, repeats: true)
            
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        print("double failure")
    }
    
    
    
    
    @IBAction func btnCloseVC(_ sender: Any)
    {
        timer.invalidate()
        self.closeView()
        
        
        //   timer = Timer.scheduledTimer(timeInterval: 5, target:self, selector: #selector(PayUMoneyVC.closeView), userInfo: nil, repeats: true)
        
        // self.navigationController?.popViewController(animated: true)
    }
    func closeView(){
        
        //        if self.navigationController!.viewControllers.contains(NearViewController()){
        //            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
        //            self.navigationController!.popToViewController(dashboardVC, animated: false)
        //
        //        }else {
        //            self.navigationController!.viewControllers.insert(NearViewController(), at: 0)
        //            let dashboardVC = self.navigationController!.viewControllers.filter { $0 is NearViewController }.first!
        //            self.navigationController!.popToViewController(dashboardVC, animated: false)
        //        }
        
        let vc = ShopViewController(nibName: "ShopViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        
    }
    
}

extension String {
    func sha1() -> String {
        let data = self.data(using: String.Encoding.utf8)!
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
    
    func digest(length:Int32, gen:(_ data: UnsafeRawPointer, _ len: CC_LONG, _ md: UnsafeMutablePointer<UInt8>) -> UnsafeMutablePointer<UInt8>) -> String {
        
        var cStr = [UInt8](self.utf8)
        
        var result = [UInt8](repeating:0, count:Int(length))
        
        gen(&cStr, CC_LONG(cStr.count), &result)
        
        let output = NSMutableString(capacity:Int(length))
        
        for r in result {
            
            output.appendFormat("%02x", r)
            
        }
        
        return String(output)
        
    }
    
}
