//
//  OrderSummaryVC.swift
//  Zam
//
//  Created by Admin on 23/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class OrderSummaryVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var btnAmt: UIButton!
    @IBOutlet var footerView: UIView!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblItem: UILabel!
    var  order_item = String()
    var order_total = String()
    var paymentParam = [String:AnyObject]()
    var order_shippingID = String()
    var order_couponCode = String()
    var summary_reference = String()
    var arr_orderList = NSMutableArray()
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbleView.rowHeight = UITableViewAutomaticDimension;
        self.tbleView.estimatedRowHeight = 80
        self.Configure()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Configure()
    {
        let amt = "₹ " + order_total + ".0" as String
        btnAmt.setTitle(amt, for: UIControlState.normal)
        ZamModel().buttonCorner(Button: btnPayNow, radius: 10)
        btnPayNow.layer.shadowRadius = 0.4
        btnPayNow.layer.shadowColor = UIColor.white.cgColor
        lblItem.text = self.order_item
        lblAmount.text = "₹ " + self.order_total + ".0" as String
        
        self.tbleView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tbleView.bounds.size.width, height: 0.1))
        self.buyOrder()
    }
    func buyOrder()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String,"TotalAmount":self.order_total , "ShippingID" :self.order_shippingID, "CouponCode": self.order_couponCode] as [String : Any]
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().buyNow(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                self.summary_reference = ""
                print(response?.object(forKey: "data") as AnyObject)
                let dic =  response?.object(forKey: "data") as AnyObject
                if let ref = dic["referenceNo"]
                {self.summary_reference = ref as! String}
                let arrData = dic["orderList"] as! NSArray
                self.arr_orderList = arrData.mutableCopy() as! NSMutableArray
                if self.arr_orderList.count > 0
                {
                    let dic = ["productName":"Product Name","quantity":"Quantity","totalFeesAfterDis":"Price"]
                    self.arr_orderList.insert(dic, at: 0)
                }
                self.tbleView.delegate = self
                self.tbleView.reloadData()
            } else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }

    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.arr_orderList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:OrderSummaryCell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell") as! OrderSummaryCell
        let dicInfo = self.arr_orderList[indexPath.row] as! [String:AnyObject]
        if(indexPath.row != 0)
        {
            cell.lblPro_Name.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 18)
            cell.lblPro_Price.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 18)
            cell.lblQty.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 18)
            
        }
        //cell.lblPro_Name.font = UIFont (name: "HelveticaNeueLTStd-Cn", size: 18)
        if let name = dicInfo["productName"]
        {
            cell.lblPro_Name.text = name as? String
        }
        if let qty = dicInfo["quantity"]
        {
            cell.lblQty.text = String(describing:qty)
        }
        
        if indexPath.row > 0
        {
            let orignalprice =  String(describing:(dicInfo["totalFeesAfterDis"] as! NSNumber))
            var pricestr = "₹" + orignalprice
            let discount = dicInfo["discount"] as! String
            print(pricestr)
            if discount == ""
            {
                cell.lblPro_Price.text = pricestr
            }
            else
            {
                pricestr =  "₹" + String(describing :dicInfo["totalFees"] as! NSNumber) + pricestr
                pricestr = pricestr + "\n" + String(describing :dicInfo["discount"] as! String)
                
                print("Price ",pricestr)
                let attributeString: NSMutableAttributedString = NSMutableAttributedString(string:pricestr)
                let range = (pricestr as NSString).range(of: String(describing :dicInfo["discount"] as! String))
                attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(1, orignalprice.characters.count))
                attributeString.addAttribute(NSForegroundColorAttributeName, value: UIColor.red , range: range)
                print(attributeString)
                cell.lblPro_Price.attributedText = attributeString
                
            }
        }
        else
        {
            cell.lblPro_Name.text = "Product Name"
        }
        cell.contentView.setNeedsLayout()
        cell.contentView.layoutIfNeeded()
        
        
//        if let add1 = dicInfo["shippingAdd1"]
//        {
//            addStr = add1 as! String
//            cell.lblAdd1.text = addStr
//        }
//        if let add2 = dicInfo["shippingAdd2"]
//        {
//            if add2 as! String == ""
//            {
//            }
//            else
//            {
//                addStr =  addStr + ", " + (add2 as! String)
//                cell.lblAdd1.text = addStr
//            }
//        }
//        if let landmark = dicInfo["shippingLandMark"]
//        {
//            cell.lblLandMark.text = landmark as? String
//        }
//        if let mobileNo = dicInfo["shippingMobileNo"]
//        {
//            cell.lblMobileNo.text = mobileNo as? String
//        }
//        if let city = dicInfo["shippingCity"]
//        {
//            cell.lblCity.text = city as? String
//        }
//        if let state = dicInfo["shippingState"]
//        {
//            cell.lblState.text = state as? String
//        }
//        if let pinCode = dicInfo["shippingPinCode"]
//        {
//            cell.lblPostalCode.text = pinCode as? String
//        }
        return cell
    }
    

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return footerView
    }
    

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        //return UITableViewAutomaticDimension
        return 127
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        print(self.tbleView.rowHeight)
//        tableView.rowHeight = UITableViewAutomaticDimension
//        return UITableViewAutomaticDimension
//    }
//    
 func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath)-> CGFloat
    {
//        print(self.tbleView.rowHeight)
//        tableView.rowHeight = UITableViewAutomaticDimension
        return UITableViewAutomaticDimension
    }
    
    
    @IBAction func btnPayNowClicked(_ sender: Any)
    {
        self.payNow()
    }
    func payNow()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String,"TotalAmount":self.order_total , "ShippingID" :self.order_shippingID, "CouponCode": self.order_couponCode,"ReferenceNO":self.summary_reference] as [String : Any]
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().PayNow(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response?.object(forKey: "data") as AnyObject)
                self.paymentParam = response?.object(forKey: "data") as! [String:AnyObject]
                let story = UIStoryboard(name: "ShopStory", bundle: nil)
                let PayUMoneyVC_obj = story.instantiateViewController(withIdentifier: "PayUMoneyVC") as! PayUMoneyVC
                PayUMoneyVC_obj.paymentDict = self.paymentParam
                print(PayUMoneyVC_obj.paymentDict)
                //PayUMoneyVC_obj.urlStr = dict["payUURL"]as! String
                self.navigationController?.pushViewController(PayUMoneyVC_obj, animated: true)
                
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }

    }
    @IBAction func btnCloseVC(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    

}
