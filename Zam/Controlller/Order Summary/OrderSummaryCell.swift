//
//  OrderSummaryCell.swift
//  Zam
//
//  Created by Admin on 23/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class OrderSummaryCell: UITableViewCell {

    @IBOutlet weak var lblPro_Price: UILabel!
    @IBOutlet weak var lblPro_Name: UILabel!
    @IBOutlet weak var lblQty: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
