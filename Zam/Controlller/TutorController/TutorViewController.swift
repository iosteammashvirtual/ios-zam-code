//
//  TutorViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/23/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class TutorViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var dataArray = NSMutableArray()
      let appDelegate = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var tblView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let nibName = UINib(nibName: "MapListCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "MapListCell")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        print(dataArray)
        self.tabBarController?.tabBar.isHidden = true
    }
    // MARK: - TableView Delegate  & Datasource
     
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 160
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:MapListCell = tableView.dequeueReusableCell(withIdentifier: "MapListCell") as! MapListCell
        let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "photoPath") as! String
        
        cell.imgView.sd_setImage(with: NSURL(string: imageurl) as URL!)
        ZamModel().roundImageView(image: cell.imgView)
        
        if (dataArray[indexPath.row] as AnyObject).value(forKey: "lastName") is NSNull{
            cell.lblName.text = String(describing: (dataArray[indexPath.row] as AnyObject).value(forKey: "firstName") as! String)
        }else {
            cell.lblName.text = String(describing: (dataArray[indexPath.row] as AnyObject).value(forKey: "firstName") as! String) + " " + String(describing: (dataArray[indexPath.row] as AnyObject).value(forKey: "lastName") as! String);
        
        }
        
    
        
        let examArray = ((dataArray[indexPath.row] as AnyObject).object(forKey: "trainerExams") as? NSArray)?.mutableCopy() as! NSMutableArray
        var str = String()
        for i in 0..<examArray.count{
         
            if (i == examArray.count - 1 ){
                str = str + ((examArray[i] as AnyObject).value(forKey: "examName") as? String)!
            }else {
                str = str + ((examArray[i] as AnyObject).value(forKey: "examName") as? String)! + ","

            }
        }
        cell.lblExamName.text = str
        cell.lblRateCount.text = String(((dataArray[indexPath.row] as AnyObject).value(forKey: "overAllRateValue") as! NSNumber).intValue) + "/5"
        
        ZamModel().setSmileImageThemeColor(btn: cell.btnRate, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "overAllRateValue") as! NSNumber))
        cell.lblReviewCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "noReview") as! NSNumber))
        cell.btnPhone.isHidden = true
         cell.btnCompare.isHidden = true
         cell.btnreview.tag = indexPath.row
        cell.btnRate.tag = indexPath.row
        cell.btnreview.addTarget(self, action: #selector(TutorViewController.btnReviewClicked), for: .touchUpInside)
         cell.btnRate.addTarget(self, action: #selector(TutorViewController.btnrateClicked), for: .touchUpInside)
        
        
//        if (dataArray[indexPath.row] as AnyObject).value(forKey: "isFav") as! Bool == true{
//            cell.btnCompare.setImage(UIImage(named:"compare50_red.png"), for: UIControlState.normal)
//        }else {
//            cell.btnCompare.setImage(UIImage(named:"compare50.png"), for: UIControlState.normal)
//        }
//        cell.btnCompare.addTarget(self, action: #selector(MapListViewController.addTocompareClicked), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let trainerID =  (dataArray[indexPath.row] as AnyObject).value(forKey: "trainerID") as! NSNumber
        
        let vc = CenterTrainerProfileViewController(nibName: "CenterTrainerProfileViewController", bundle: nil)
                      vc.trainerId = String(describing: trainerID)
                self.navigationController?.pushViewController(vc, animated: false)
       // getTrainerDetail(trainerId: String(describing: trainerID))
       // getDetail(centerId: String(describing: centreID))
    }
 
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
     func btnReviewClicked(sender: UIButton) {
        let data = dataArray[sender.tag]
      //  print(data)
        let vc = TrainerReviewViewController(nibName: "TrainerReviewViewController", bundle: nil)
        vc.trainerID = String(describing: ((data as AnyObject).value(forKey: "trainerID") as! NSNumber))
        appDelegate.comeFromTrainerReview = "trainercontroller"
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func btnrateClicked(sender: UIButton) {
         let data = dataArray[sender.tag]
        let vc = TrainerRateViewController(nibName: "TrainerRateViewController", bundle: nil)
         appDelegate.comeFromTrainerRate = "trainercontroller"
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"TrainerID" :  String(describing: ((data as AnyObject).value(forKey: "trainerID") as! NSNumber)),"UserId":UserDefaults.standard.object(forKey: "userId") as! String,]
        vc.parameter = parameters as NSDictionary
        vc.trainerID = String(describing: ((data as AnyObject).value(forKey: "trainerID") as! NSNumber))
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }

}
