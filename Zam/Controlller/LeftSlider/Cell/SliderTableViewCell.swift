//
//  SliderTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 12/30/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
