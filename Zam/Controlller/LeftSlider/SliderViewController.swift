//
//  SliderViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 12/30/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import UIKit
import MessageUI
//import GTMOAuth2

class SliderViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var tblView: UITableView!
      var window: UIWindow?
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let dataArray = ["My Profile","My Rate","My Review","About","Rate us","Contact us", "Change Password","Logout"]
    
    let imageArray = ["profile.png","star.png","review50w.png","information50w.png","happy50w.png","contact.png", "change-password.png","logout.png"]
    override func viewDidLoad() {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(SliderViewController.closeView(notification:)), name: Notification.Name("SliderClosed"), object: nil)
          let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(SliderViewController.SwipeLeftGesture(gesture:)))
          swipeLeft.direction = UISwipeGestureRecognizerDirection.left
         self.view.addGestureRecognizer(swipeLeft)
        
          navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        let nibName = UINib(nibName: "SliderTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "SliderTableViewCell")

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:SliderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell") as! SliderTableViewCell
       cell.lblName.text = dataArray[indexPath.row]
        cell.imgView.image = UIImage(named: imageArray[indexPath.row])
        return cell
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let vc = ProfileViewController(nibName: "ProfileViewController", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
              closeView()
        }
        else if indexPath.row == 1{
            let vc = MyRateViewController(nibName: "MyRateViewController", bundle: nil)
            //self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: true)
              closeView()
            
        }else if indexPath.row == 2{
            let vc = MyReviewViewController(nibName: "MyReviewViewController", bundle: nil)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: true)
              closeView()
        
        }
        else if indexPath.row == 3{
            let vc = AboutViewController(nibName: "AboutViewController", bundle: nil)
             self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: true)
              closeView()
        }
        else if indexPath.row==4{
            
           UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/app/id1206399957")! as URL)
//            UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")! as URL)
        }else if indexPath.row == 5{
            
//            var gtlMessage = GTLGmailMessage()
//            gtlMessage.raw = self.generateRawString()
//            let appd = UIApplication.shared.delegate as! AppDelegate
//            let query = GTLQueryGmail.queryForUsersMessagesSendWithUploadParameters(nil)
//            query.message = gtlMessage
//            
//            appd.service.executeQuery(query, completionHandler: { (ticket, response, error) -> Void in
//                println("ticket \(ticket)")
//                println("response \(response)")
//                println("error \(error)")
//            })
            let mailVC = MFMailComposeViewController()
            mailVC.mailComposeDelegate = self
             mailVC.delegate = self
            mailVC.setToRecipients(["hello@mashvirtual.com"])
            mailVC.setSubject("Zam")
            mailVC.setMessageBody("Contact us", isHTML: false)
            if (MFMailComposeViewController.canSendMail()) {
                present(mailVC, animated: true, completion: nil)
               // closeView()
            }
        }else if indexPath.row == 6{
            let vc = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
              self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(vc, animated: true)
              closeView()
        }else if indexPath.row == 7{
            UserDefaults.standard.set(nil, forKey: "token")
            UserDefaults.standard.synchronize()
             self.tabBarController?.tabBar.isHidden = true
            let viewObject:LoginViewController = LoginViewController(nibName: "LoginViewController", bundle: nil)
           // let navigation: UINavigationController = UINavigationController(rootViewController: viewObject)
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            navigation.navigationBar.isHidden = true
//            self.window!.rootViewController = navigation
//             self.window!.makeKeyAndVisible()
            self.navigationController?.pushViewController(viewObject, animated: false)
//            if self.navigationController!.viewControllers.contains(LoginViewController()){
//                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is LoginViewController }.first!
//                self.navigationController!.popToViewController(dashboardVC, animated: false)
//                closeView()
//            }else {
//                self.navigationController!.viewControllers.insert(LoginViewController(), at: 0)
//                let dashboardVC = self.navigationController!.viewControllers.filter { $0 is LoginViewController }.first!
//                self.navigationController!.popToViewController(dashboardVC, animated: false)
//                closeView()
//            }
           closeView()
        }
    }
    
    @IBAction func btnCloseClick(_ sender: Any) {
          closeView()
    }
    
    func SwipeLeftGesture(gesture: UIGestureRecognizer) {
      closeView()
       }
    
    func closeView(){
        appDelegate.SwipeRightOpen = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x:-UIScreen.main.bounds.size.width, y:0, width:UIScreen.main.bounds.size.width - 70 ,height:UIScreen.main.bounds.size.height-50)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    func mailComposeController(_ controller:MFMailComposeViewController, didFinishWith result:MFMailComposeResult, error:Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Mail cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Mail saved")
        case MFMailComposeResult.sent.rawValue:
            print("Mail sent")
        case MFMailComposeResult.failed.rawValue:
            print("Mail sent failure: %@", [error?.localizedDescription])
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCloseView(_ sender: Any) {
        closeView()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func closeView(notification: Notification) {
         appDelegate.SwipeRightOpen = false
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
//        UIView.animate(withDuration: 0.01, animations: { () -> Void in
//            self.view.frame = CGRect(x:-UIScreen.main.bounds.size.width, y:0, width:UIScreen.main.bounds.size.width - 70 ,height:UIScreen.main.bounds.size.height-50)
//            self.view.layoutIfNeeded()
//            self.view.backgroundColor = UIColor.clear
//        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
 //       })
    }
//    class func generateRawString() -> String {
//        
//        var dateFormatter:DateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss Z"; //RFC2822-Format
//        var todayString:String = dateFormatter.string(from: NSDate() as Date)
//        
//        var rawMessage = "" +
//            "Date: \(todayString)\r\n" +
//            "From: <mail>\r\n" +
//            "To: username <mail>\r\n" +
//            "Subject: Test send email\r\n\r\n" +
//        "Test body"
//        
//        print("message \(rawMessage)")
//        
//        return GTLEncodeWebSafeBase64(rawMessage.data(using: String.Encoding.utf8))
//    }
}
