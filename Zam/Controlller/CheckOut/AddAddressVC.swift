//
//  AddAddressVC.swift
//  Zam
//
//  Created by Admin on 19/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import Alamofire
class AddAddressVC: UIViewController,UITextFieldDelegate
{
    var dropDownState = DropDown()
    var dropDownCity = DropDown()
    var mobileNo = String()
    var name = String()
    let defaults = UserDefaults.standard
    var stateArray = NSArray()
    var filterStateArray = NSArray()
    var cityArray = NSArray()
    var selectedStateid = NSNumber()
    var selectedCityId = NSNumber()
    
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var btnAddAddress: UIButton!
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtPostalCode: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtLandmark: UITextField!
    @IBOutlet weak var txtAddressLine2: UITextField!
    
    @IBOutlet weak var txtAddressLine1: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.Configure()
        // Do any additional setup after loading the view.
    }
    func Configure()
    {
        self.tbleView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tbleView.bounds.size.width, height: 0.1))
        txtAddressLine1.becomeFirstResponder()
        txtName.textfieldUnderline(textField: txtName)
        txtAddressLine1.textfieldUnderline(textField: txtName)
        txtAddressLine2.textfieldUnderline(textField: txtName)
        txtCity.textfieldUnderline(textField: txtName)
        txtState.textfieldUnderline(textField: txtName)
        txtMobileNo.textfieldUnderline(textField: txtName)
        txtPostalCode.textfieldUnderline(textField: txtName)
        txtLandmark.textfieldUnderline(textField: txtName)
        txtName.textfieldUnderline(textField: txtName)
        btnAddAddress.layer.shadowColor = UIColor.gray.cgColor
        ZamModel().buttonCorner(Button: btnAddAddress, radius: 10)
        btnAddAddress.layer.shadowRadius = 0.5
        self.getStateList()
    }
    
    func setupStateMenu()
    {
        dropDownCity.dataSource.removeAll()
        for dic in (self.stateArray as? [[String:AnyObject]])!
        {
            if dic  != nil
            {
                let stateName = dic["stateName"]! as? String
                dropDownState.dataSource.append(stateName!)
            }
        }
        dropDownState.selectionAction = { [unowned self] (index, item) in
            self.txtState.text = item as String
            let dic = self.filterStateArray[index] as? [String:AnyObject]
            print(dic)
            let stateId = dic?["stateID"]! as? NSNumber
            if stateId != nil
            {
                self.txtCity.text = nil
                self.dropDownCity.dataSource.removeAll()
                self.selectedStateid = stateId!
                self.getCityList(id: stateId!)
            }
            
        }
        
    }
    
    
    func setupCityMenu()
    {
        dropDownCity.dataSource.removeAll()
        for dic in (self.cityArray as? [[String:AnyObject]])!
        {
            if dic != nil
            {
                let cityName = dic["cityName"]! as? String
                dropDownCity.dataSource.append(cityName!)
            }
        }
        
        dropDownCity.selectionAction = { [unowned self] (index, item) in
            self.txtCity.text = item as String
            let dic = self.cityArray[index] as? [String:AnyObject]
            let cityID = dic?["cityID"]! as? NSNumber
            self.selectedCityId = cityID!
            
        }
        
    }
    
    
    func getStateList()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String] as [String : Any]
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getStateList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                self.txtName.text = response?.object(forKey: "name") as? String
                if !(response?.object(forKey: "mobile") is NSNull){
                 self.txtMobileNo.text = response?.object(forKey: "mobile") as! String?
                }
               
                print(response?.object(forKey: "data") as! NSArray)
                self.stateArray = response?.object(forKey: "data") as! NSArray
                self.filterStateArray = self.stateArray
                if self.stateArray.count>0
                {
                    self.setupStateMenu()
                }
                
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    
    
    func getCityList(id:NSNumber)
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["stateID" : id]
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCityList_State(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response?.object(forKey: "data") as! NSArray)
                self.cityArray = response?.object(forKey: "data") as! NSArray
                if self.cityArray.count>0
                {
                    self.setupCityMenu()
                }
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: TEXT DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if textField == txtState
        {
            dropDownState.anchorView = txtLandmark
            print(txtCity.frame.origin.y)
            print(dropDownState.anchorView?.plainView.bounds.height)
            dropDownState.updateConstraints()
            // print(dropDownState.anchorView?.plainView.bounds.height)
            //dropDownState.bottomOffset = CGPoint(x: 0, y: 0)
            dropDownState.show()
        }
        else if textField == txtCity
        {
            dropDownCity.anchorView = txtCity
            print(txtCity.frame.origin.y)
            dropDownCity.bottomOffset = CGPoint(x: 0, y: txtCity.bounds.height)
            dropDownCity.updateConstraintsIfNeeded()
            dropDownCity.show()
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == txtPostalCode{
            guard let text = textField.text else { return true }
            let newLength = text.characters.count + string.characters.count - range.length
            if newLength <= 8{
                return true
            }else {
                return false
            }
        
        }
       else if textField == txtState
        {
            let searchPredicate = NSPredicate(format: "stateName CONTAINS[c] %@", textField.text!)
            self.filterStateArray  = self.stateArray.filtered(using: searchPredicate) as NSArray
            dropDownState.dataSource.removeAll()
            for dic in (self.filterStateArray as? [[String:AnyObject]])!
            {
                if dic != nil
                {
                    let stateName = dic["stateName"]! as? String
                    dropDownState.dataSource.append(stateName!)
                }
            }
            dropDownState.show()
        }
        else if textField == txtCity
        {
            print(self.cityArray)
            let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            let searchPredicate = NSPredicate(format: "cityName CONTAINS[c] %@", result)
            let array = (self.cityArray).filtered(using: searchPredicate)
            
            print(array)
            dropDownCity.dataSource.removeAll()
            for dic in (array as? [[String:AnyObject]])!
            {
                if dic != nil
                {
                    let cityName = dic["cityName"]! as? String
                    dropDownCity.dataSource.append(cityName!)
                }
            }
            
            dropDownCity.show()
        }
        
        return true
    }
    
    //MARK: TABLE DELEGATES
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        //return UITableViewAutomaticDimension
        return 623
    }
    
    //MARK: IB Action
    @IBAction func btnCloseVC(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func btnAddAddresssClicked(_ sender: Any)
    {
        
        var errorStr = ""
        if (self.txtName.text?.characters.count == 0)
        {
            errorStr = "Please enter Name."
        }
        if (self.txtAddressLine1.text?.characters.count == 0)
        {
            errorStr = "Please enter Shipping Address."
        }
            
        else if(self.txtState.text?.characters.count == 0)
        {
            errorStr = "Please enter State."
        }
        else if(self.txtCity.text?.characters.count == 0)
        {
            errorStr = "Please select City."
        }
        else if(self.txtPostalCode.text?.characters.count == 0)
        {
            errorStr = "Please enter Postal Code."
        }
        else if(self.txtMobileNo.text?.characters.count == 0)
        {
            errorStr = "Please enter Mobile Number."
        }else if ZamModel().regexPhoneNotCorrect(phoneNo: txtMobileNo.text!) == false{
            
            errorStr = MobilenoNotValid
        }
        if errorStr == ""
        {
            self.addAddress()
        }
        else
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = errorStr
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
    }
    func addAddress()
    {
        if ZamModel().isConnectedToNetwork() == false
        {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String, "ShippingAdd1": txtAddressLine1.text! as String,"ShippingAdd2": txtAddressLine2.text! as String ,"ShippingCity": txtCity
            .text! as String,"ShippingPinCode": txtPostalCode.text! as String,"ShippingCountry": "India","ShippingMobileNo": txtMobileNo.text! as String,"ShippingLandMark": txtLandmark.text! as String,"ShippingName": txtName.text! as String,"ShippingState": txtState.text! as String] as [String : Any]
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().createShipping(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true
            {
                print(response)//.object(forKey: "data") as! NSArray)
                self.navigationController?.popViewController(animated: true)
                
            }
            else
            {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
        
        
        
        
    }
    
}


extension UITextField
{
    func textfieldUnderline(textField:UITextField)
    {
        self.layoutIfNeeded()
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = ZamModel().themecolor.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
