//
//  CheckOutVC.swift
//  Zam
//
//  Created by Admin on 19/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import Alamofire

class CheckOutVC: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    
    @IBOutlet weak var btnAddBorder: UIButton!
    @IBOutlet weak var btnAddCouponHtConst: NSLayoutConstraint!
    @IBOutlet weak var btnAddCoupon: UIButton!
    @IBOutlet weak var htTable: NSLayoutConstraint!
    @IBOutlet weak var tbleView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var btnAddAddressw: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblsummary: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    @IBOutlet weak var txtCoupon: UITextField!
    var headerViewht = 140
    var dataArray = NSArray()
    var  item = String()
    var total = String()
    var shippingID = String()
    var couponCode = String()
    
     let defaults = UserDefaults.standard
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.Configure()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getShippingInfo()
    }
    func Configure()
    {
        btnAddCouponHtConst.constant = 0
        self.tbleView.estimatedRowHeight = 51.0;
        self.tbleView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.tbleView.bounds.size.width, height: 0.1))
        btnAddCoupon.isHidden = true
        ZamModel().buttonCorner(Button: btnAddCoupon, radius: 5)
        ZamModel().buttonCorner(Button: btnAddBorder, radius: 5)
        btnAddBorder.layer.borderWidth = 0.5
        btnAddBorder.layer.borderColor = ZamModel().themecolor.cgColor
        ZamModel().buttonCorner(Button: btnAddAddressw, radius: 10)
        self.lblsummary.text = String("Summary: " +  (self.item) + " items | ₹ " + self.total + ".0")
        self.tbleView.delegate = self
        self.tbleView.reloadData()
        //self.getShippingInfo()
    }

    func getShippingInfo()
    {
            if ZamModel().isConnectedToNetwork() == false
            {
                let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                controller.message = networkError
                addChildViewController(controller)
                controller.view.frame = view.bounds
                view.addSubview(controller.view)
                controller.didMove(toParentViewController: self)
                return
            }
            let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String] as [String : Any]
            print(parameters)
           self.view.makeToastActivity(message: "Loading", view: self.view)
            AppEngine().getShipping(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                self.view.hideToastActivity(view: self.view)
                if response == nil{
                    self.view.makeToast(message: ServerError)
                    return
                }
                if response?.object(forKey: "success") as! Bool  == true
                {
                    self.shippingID = ""
                    print(response?.object(forKey: "data") as! NSArray)
                    self.dataArray = response?.object(forKey: "data") as! NSArray
                    self.tbleView.reloadData()
                    if let email = response?.object(forKey: "email")
                    {
                        self.lblEmail.text = email as? String
                    }
                    if let mobileNo = response?.object(forKey: "mobileNo")
                    {
                        self.lblMobileNo.text = mobileNo as? String
                    }
                }
                else
                {
                    self.view.makeToast(message: response?.object(forKey: "message") as! String)
                }
            }
    }
    // MARK: - Table Delegates

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
     {
//        htTable.constant = 309
        return self.dataArray.count
        
     }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
     {
        let cell:CheckOutCell = tableView.dequeueReusableCell(withIdentifier: "CheckCell") as! CheckOutCell
        cell.imgBg.isHidden = false
        cell.viewBg.backgroundColor = UIColor.white
        cell.lblAdd1.textColor = ZamModel().themecolor
        cell.lblCity.textColor = ZamModel().themecolor
        cell.lblLandMark.textColor = ZamModel().themecolor
        cell.lblName.textColor = ZamModel().themecolor
        cell.lblMobileNo.textColor = ZamModel().themecolor
        cell.lblState.textColor = ZamModel().themecolor
        cell.lblPostalCode.textColor = ZamModel().themecolor
        let dicInfo = self.dataArray[indexPath.row] as! [String:AnyObject]
        var addStr = ""
            if let name = dicInfo["shippingName"]
            {
                cell.lblName.text = name as? String
            }
            if let add1 = dicInfo["shippingAdd1"]
            {
                addStr = add1 as! String
                cell.lblAdd1.text = addStr
            }
            if let add2 = dicInfo["shippingAdd2"]
            {
                if add2 as! String == ""
                {
                }
                else
                {
                    addStr =  addStr + ", " + (add2 as! String)
                    cell.lblAdd1.text = addStr
                }
            }
            if let landmark = dicInfo["shippingLandMark"]
            {
                cell.lblLandMark.text = landmark as? String
            }
            if let mobileNo = dicInfo["shippingMobileNo"]
            {
                cell.lblMobileNo.text = mobileNo as? String
            }
            if let city = dicInfo["shippingCity"]
            {
                cell.lblCity.text = city as? String
            }
            if let state = dicInfo["shippingState"]
            {
                cell.lblState.text = state as? String
            }
            if let pinCode = dicInfo["shippingPinCode"]
            {
                cell.lblPostalCode.text = pinCode as? String
            }
          return cell
     }
    
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
     {
        
        return headerView
     }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return footerView
    }

     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
     {
        return CGFloat(headerViewht)
     }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        //return UITableViewAutomaticDimension
        return 55
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
        return 185
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell:CheckOutCell = tableView.cellForRow(at: indexPath) as! CheckOutCell
        cell.viewBg.backgroundColor = ZamModel().themecolor
        cell.imgBg.isHidden = true
        cell.lblAdd1.textColor = UIColor.white
        cell.lblCity.textColor = UIColor.white
        cell.lblLandMark.textColor = UIColor.white
        cell.lblName.textColor = UIColor.white
        cell.lblMobileNo.textColor = UIColor.white
        cell.lblState.textColor = UIColor.white
        cell.lblPostalCode.textColor = UIColor.white
        let dic = self.dataArray[indexPath.row] as AnyObject
        if let shipID = dic["shippingID"]
        {
            shippingID = String(describing:shipID as! NSNumber)
        }
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath)
    {
        let cell:CheckOutCell = tableView.cellForRow(at: indexPath) as! CheckOutCell
        cell.imgBg.isHidden = false
        cell.viewBg.backgroundColor = UIColor.white
        cell.lblAdd1.textColor = ZamModel().themecolor
        cell.lblCity.textColor = ZamModel().themecolor
        cell.lblLandMark.textColor = ZamModel().themecolor
        cell.lblName.textColor = ZamModel().themecolor
        cell.lblMobileNo.textColor = ZamModel().themecolor
        cell.lblState.textColor = ZamModel().themecolor
        cell.lblPostalCode.textColor = ZamModel().themecolor
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        btnAddCouponHtConst.constant = 30
        headerViewht = 170
        self.tbleView.reloadData()
        if textField == txtCoupon
        {
            btnAddCoupon.isHidden = false
        }
    }
    @IBAction func btnCloseVC(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnAddCouponClicked(_ sender: Any)
    {
        txtCoupon.resignFirstResponder()
        if txtCoupon.text != ""
        {
            self.couponCode = txtCoupon.text!
           
                if ZamModel().isConnectedToNetwork() == false
                {
                    let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                    controller.message = networkError
                    addChildViewController(controller)
                    controller.view.frame = view.bounds
                    view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                    return
                }
                let parameters = ["PrimaryInfoID" : defaults.object(forKey: "primaryInfoId") as! String, "userID": defaults.object(forKey: "userId")as! String,"CouponCode": txtCoupon.text! as String ,"TotalAmount": total as String] as [String : Any]
                print(parameters)
                self.view.makeToastActivity(message: "Loading", view: self.view)
                AppEngine().GetCouponDiscount(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
                    self.view.hideToastActivity(view: self.view)
                    if response == nil{
                        self.view.makeToast(message: ServerError)
                        return
                    }
                    if response?.object(forKey: "success") as! Bool  == true
                    {
                        print(response)
                        self.view.makeToast(message: response?.object(forKey: "message") as! String)
                        self.btnAddCoupon.isHidden = true
                        self.btnAddCouponHtConst.constant = 140
                        self.headerViewht = 140
                        
                        self.tbleView .reloadData()
                        print(response?.object(forKey: "data") as! NSArray)
                    }
                    else
                    {
//                        self.view.makeToast(message: response?.object(forKey: "message") as! String)
                        //self.btnAddCoupon.isHidden = true
                         self.btnAddCoupon.isHidden = false
                        self.btnAddCouponHtConst.constant = 30
                        self.txtCoupon.text = nil
                        self.headerViewht = 170
                        let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                        controller.message = response?.object(forKey: "message") as! String
                        self.addChildViewController(controller)
                        controller.view.frame = self.view.bounds
                        self.view.addSubview(controller.view)
                        controller.didMove(toParentViewController: self)
                        self.tbleView .reloadData()

                    }
                }
            
            //btnAddCoupon.isHidden = true
        }else {
       self.view.makeToast(message: EnterValidCoupon)
        }
    }
    @IBAction func btnBuyClicked(_ sender: Any)
    {
        if shippingID != ""
        {
            let story = UIStoryboard(name: "ShopStory", bundle: nil)
            let OrderSummaryVC_obj = story.instantiateViewController(withIdentifier: "OrderSummaryVC") as! OrderSummaryVC
            OrderSummaryVC_obj.order_total = total
            OrderSummaryVC_obj.order_item = item
            OrderSummaryVC_obj.order_couponCode = couponCode
            OrderSummaryVC_obj.order_shippingID = shippingID
            self.navigationController?.pushViewController(OrderSummaryVC_obj, animated: true)
        }else {
        
        self.view.makeToast(message: ShippingAddressNil)
        }
    }
    
    @IBAction func btnAddAddress(_ sender: Any)
    {
        let story = UIStoryboard(name: "ShopStory", bundle: nil)
        let VC = story.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        VC.txtMobileNo.text = self.lblMobileNo.text
        VC.txtName.text = self.lblEmail.text
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    
}

extension UITextField {
    func setCursor(position: Int) {
        let position = self.position(from: beginningOfDocument, offset: position)!
        selectedTextRange = textRange(from: position, to: position)
    }
}
