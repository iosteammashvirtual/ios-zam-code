//
//  CheckOutCell.swift
//  Zam
//
//  Created by Admin on 23/01/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CheckOutCell: UITableViewCell
{
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var imgBg: UIImageView!
    @IBOutlet weak var lblLandMark: UILabel!
    @IBOutlet weak var lblAdd1: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblPostalCode: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
