//
//  WebViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/23/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class WebViewController: UIViewController,UIWebViewDelegate {

    let url = "https://www.ieltsidpindia.com/"

    @IBOutlet weak var webView: UIWebView!
    var comFromEditProfile:Bool = false
    var comeFromShopAdd = false
    var strUrl = String()
    override func viewDidLoad() {
        super.viewDidLoad()
  navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
      self.tabBarController?.tabBar.isHidden = true
        if comeFromShopAdd == true{
            let webUrl = URL(string : strUrl)
            webView.loadRequest(URLRequest(url: webUrl!))
        }
        else if  comFromEditProfile {
            let strUrl = weburl + "?token_type=" + "Bearer" + "&access_token=" + (UserDefaults.standard.object(forKey: "token") as! String)
            let webUrl = URL(string : strUrl)
          webView.loadRequest(URLRequest(url: webUrl!))
        }
    }
  
    
    //MARK:- WebView Delegate
    
    func webViewDidStartLoad(_ webView: UIWebView) {
      self.view.makeToastActivity(message: "Loading", view: self.view)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
       self.view.hideToastActivity(view: self.view)
        self.view.makeToast(message: error.localizedDescription)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.hideToastActivity(view: self.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackClicked(_ sender: Any) {
         self.tabBarController?.tabBar.isHidden = false
    self.navigationController?.popViewController(animated: true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
