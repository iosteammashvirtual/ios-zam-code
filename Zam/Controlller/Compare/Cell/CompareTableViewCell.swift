//
//  CompareTableViewCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/10/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CompareTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var lblExamName: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnInfo: UIButton!
   
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnReview: UIButton!
    @IBOutlet weak var lblReviewCount: UILabel!
    @IBOutlet weak var imgBackGround: UIImageView!
    @IBOutlet weak var btnImgView: UIButton!
    @IBOutlet weak var lblRateCount: UILabel!

    @IBOutlet weak var btnDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
