//
//  CenterComparisionViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/4/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit


class CenterComparisionViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout {
    
      let appDelegate = UIApplication.shared.delegate as! AppDelegate
    @IBOutlet weak var collHeight: NSLayoutConstraint!
 var isPresented = true
    @IBOutlet weak var collView: UICollectionView!
      var typeArray = NSMutableArray()
    var dataArray = NSMutableArray()
    @IBOutlet weak var collViewHeader: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "CenterCompareCollectionCell", bundle: nil)
        collView?.register(nib, forCellWithReuseIdentifier: "CenterCompareCollectionCell")
      //  collView.backgroundColor  = UIColor.clear
        
        let nib1 = UINib(nibName: "ComparisionheaderCollectionCell", bundle: nil)
        collViewHeader?.register(nib1, forCellWithReuseIdentifier: "ComparisionheaderCollectionCell")
        collViewHeader.backgroundColor  = ZamModel().themecolor
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        collHeight.constant = CGFloat(typeArray.count-1) * CGFloat(60)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        isPresented = false
         appDelegate.compareSelected = true
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - tableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return typeArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 54
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
         let cell = tableView.dequeueReusableCell(withIdentifier: "InterestExamTableViewCell") as! InterestExamTableViewCell
        if tableView.tag == 0 {
            cell.lblName.text = (typeArray[indexPath.row] as AnyObject).value(forKey:"criteriaName") as! String
           // cell.backgroundColor = ZamModel().themecolor
              cell.backgroundColor = UIColor.clear
             cell.lblName.textAlignment = .left
             cell.imgView.backgroundColor = ZamModel().themecolor
            cell.lblName.textColor = UIColor.white
        }else {
            let data =  ((dataArray[tableView.tag - 1] as AnyObject).value(forKey:"centreRates") as! NSArray).mutableCopy() as! NSMutableArray
            cell.lblName.text = String(describing :(data.object(at: indexPath.row) as AnyObject).value(forKey:"rateValue") as! NSNumber)  + "/5"
             cell.lblName.textAlignment = .center
             cell.backgroundColor = UIColor.white
            cell.lblName.textColor = UIColor(red: 101/255.0, green: 101/255.0, blue: 101/255.0, alpha: 1.0)
        }
        return cell
    }
      // MARK: - CollectionView Delegate & DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count+1
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 100{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComparisionheaderCollectionCell", for: indexPath as IndexPath) as! ComparisionheaderCollectionCell
            if indexPath.row == 0{
                cell.lblName.isHidden = true
            }else {
                cell.lblName.isHidden = false
                cell.lblName.text = (dataArray[indexPath.row - 1] as AnyObject).value(forKey:"centreName") as! String
//                cell.lblName.font = UIFont(name: "Roboto-Bold", size: 16)
            }
            
            
        return cell
        }else {
        
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CenterCompareCollectionCell", for: indexPath as IndexPath) as! CenterCompareCollectionCell
        // get a reference to our storyboard cell
        let nibName = UINib(nibName: "InterestExamTableViewCell", bundle:nil)
        cell.tblView.register(nibName, forCellReuseIdentifier: "InterestExamTableViewCell")
        cell.tblView.delegate = self
        cell.tblView.dataSource = self
        cell.tblView.tag = indexPath.row
        cell.tblView.reloadData()
        return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView.tag == 100 {
         return CGSize(width:(collectionView.frame.size.width / (dataArray.count+1))  , height:collectionView.frame.size.height)
        }else {
         return CGSize(width:(collectionView.frame.size.width / (dataArray.count+1))  , height:collectionView.frame.size.height)
        }
     
    }
//    func collectionView(_ collectionView: UICollectionView,
//                        layout collectionViewLayout: UICollectionViewLayout,
//                        sizeForItemAt indexPath: IndexPath) -> CGSize {
//        // your code here
//    }
   
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 100 {
            if indexPath.row != 0 {
             let centerId = (dataArray[indexPath.row - 1] as AnyObject).value(forKey:"centreID") as! NSNumber
                 appDelegate.comparedCenterId = Int(centerId)
                 NotificationCenter.default.post(name: Notification.Name("CompareCenterDetailClicked"), object: nil)
                self.isPresented = false
                self.dismiss(animated: false, completion: nil)
               
                
                
               //getDetail(centerId: String(describing: centerId))
            }
        }
    }

//func getDetail(centerId :String){
//    if ZamModel().isConnectedToNetwork() == false {
//        let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
//        controller.message = networkError
//        addChildViewController(controller)
//        controller.view.frame = view.bounds
//        view.addSubview(controller.view)
//        controller.didMove(toParentViewController: self)
//        return
//    }
//    let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : centerId]
//    self.view.makeToastActivity(message: "Loading", view: self.view)
//    AppEngine().getCenterDetail(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
//        self.view.hideToastActivity(view: self.view)
//        if response == nil{
//            self.view.makeToast(message: ServerError)
//            return
//        }
//        if response?.object(forKey: "success") as! Bool  == true{
//            
//            if((self.presentingViewController) != nil){
//                self.isPresented = false
//                self.dismiss(animated: false, completion: nil)
//                //                self.dismiss(animated: false, completion: nil)
//            }
//           comparedCenterId
//           
//            
//           
////            DispatchQueue.main.async{
////                self.isPresented = false
////                self.dismiss(animated: false, completion: nil)
////            }
//            
//           
//        } else {
//            self.view.makeToast(message: response?.object(forKey: "message") as! String)
//        }
//    }
//}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
