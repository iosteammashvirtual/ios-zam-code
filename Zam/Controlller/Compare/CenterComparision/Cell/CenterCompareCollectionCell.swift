//
//  CenterCompareCollectionCell.swift
//  Zam
//
//  Created by Dinesh Rana on 1/11/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit

class CenterCompareCollectionCell: UICollectionViewCell {
    @IBOutlet weak var tblView: UITableView!

    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCentreName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
