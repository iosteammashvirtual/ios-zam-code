//
//  CompareViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/2/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import AudioToolbox

class CompareViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var dataArray = NSMutableArray()
    
    @IBOutlet weak var btnCompare: UIButton!
    @IBOutlet weak var lblCompareListNil: UILabel!
    var ScrollEnable:Bool = false
    var DeleteselectedIndex = Int()
    @IBOutlet weak var tblView: UITableView!
    var selectedIndex = NSMutableArray()
     var SelectedcenterIdList = NSMutableArray()
     let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var comparedDataArray  = NSMutableArray()
    var comparedCnameArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = UINib(nibName: "CompareTableViewCell", bundle:nil)
        tblView.register(nibName, forCellReuseIdentifier: "CompareTableViewCell")
         NotificationCenter.default.addObserver(self, selector: #selector(CompareViewController.PopYesClicked(notification:)), name: Notification.Name("CompareYesClicked"), object: nil)
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(CompareViewController.CenterComparisionOpenFromCenterDetail(notification:)), name: Notification.Name("CenterComparisionOpenFromCenterDetail"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CompareViewController.CompareCenterDetailClicked(notification:)), name: Notification.Name("CompareCenterDetailClicked"), object: nil)
            ZamModel().buttonCornerWithThemeBorder(Button: btnCompare, radius: 1, borderWidth: 0)
        self.tblView.rowHeight = UITableViewAutomaticDimension;
        self.tblView.estimatedRowHeight = 180;
        // Do any additional setup after loading the view.
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(CompareViewController.SwipeRightGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        
          }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        if  appDelegate.compareSelected == true{
        }else {
            SelectedcenterIdList = NSMutableArray()
            selectedIndex = NSMutableArray()
        }
        NotificationCenter.default.post(name: Notification.Name("SliderClosed"), object: nil)
        lblCompareListNil.text =  ""
        //
        if  appDelegate.addToCompareClicked == true{
            getdata()
        }
        self.tabBarController?.tabBar.isHidden = false
    }
    // MARK: - tableView Delegate & DataSource
    override func viewWillDisappear(_ animated: Bool) {
          appDelegate.addToCompareClicked = true
        appDelegate.compareSelected = false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataArray.count == 0{
         lblCompareListNil.text = "You have not added any centre to compare list."
        }
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       return UITableViewAutomaticDimension
       // return 200
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:CompareTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CompareTableViewCell") as! CompareTableViewCell
        let imageurl = (dataArray[indexPath.row] as AnyObject).value(forKey: "photoPath") as! String
         cell.btnImgView.sd_setBackgroundImage(with: NSURL(string: imageurl) as! URL, for: .normal)
                cell.btnDelete.tag = indexPath.row
                cell.lblName.text = (dataArray[indexPath.row] as AnyObject).value(forKey: "centreName") as? String
                let data = (dataArray[indexPath.row] as AnyObject).object(forKey: "exams") as? NSArray
                cell.lblExamName.text = (data?[0] as AnyObject).value(forKey: "examName") as? String
                cell.lblRateCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber)) + "/5"
                cell.btnDelete.addTarget(self, action: #selector(CompareViewController.deleteClicked), for: .touchUpInside)
        cell.btnRate.addTarget(self, action: #selector(CompareViewController.RateClicked), for: .touchUpInside)
         cell.btnReview.addTarget(self, action: #selector(CompareViewController.ReviewClicked), for: .touchUpInside)
         cell.btnInfo.addTarget(self, action: #selector(CompareViewController.InfoClicked), for: .touchUpInside)
          cell.btnContact.addTarget(self, action: #selector(CompareViewController.btnContactClicked), for: .touchUpInside)
         cell.lblReviewCount.text = String(describing: ((dataArray[indexPath.row] as AnyObject).value(forKey: "noReviews") as! NSNumber))
        if selectedIndex.contains(indexPath.row){
          cell.imgBackGround.backgroundColor = ZamModel().themecolor
            cell.imgBackGround.image = nil
            cell.lblName.textColor = UIColor.white
            cell.lblExamName.textColor = UIColor.white
            cell.lblRateCount.textColor = UIColor.white
            cell.lblReviewCount.textColor = UIColor.white
             ZamModel().buttonCorner(Button: cell.btnImgView, radius: Int(cell.btnImgView.frame.size.height / 2.0))
            
            cell.btnInfo.setImage(UIImage(named:"information50w.png"), for: .normal)
             ZamModel().setSmileImageWhite(btn: cell.btnRate, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber))
             cell.btnDelete.setImage(UIImage(named:"removefav_Cart_w.png"), for: .normal)
            cell.btnReview.setImage(UIImage(named:"review50w.png"), for: .normal)
            cell.btnContact.setImage(UIImage(named:"contact.png"), for: .normal)
        }else{
           ZamModel().buttonCornerWithThemeBorder(Button: cell.btnImgView, radius: Int(cell.btnImgView.frame.size.height / 2.0), borderWidth: Int(2))
             cell.btnDelete.setImage(UIImage(named:"removefav_Cart.png"), for: .normal)
        cell.imgBackGround.backgroundColor = UIColor.white
         //   cell.imgBackGround.image = UIImage(named:"background-shop.png")
             cell.lblRateCount.textColor = ZamModel().themecolor
            cell.lblReviewCount.textColor = ZamModel().themecolor
            cell.lblName.textColor = UIColor(red: 101/255.0, green: 101/255.0, blue: 101/255.0, alpha: 1.0)
            cell.lblExamName.textColor = UIColor(red: 101/255.0, green: 101/255.0, blue: 101/255.0, alpha: 1.0)
             ZamModel().setSmileImageThemeColor(btn: cell.btnRate, rateCount: Int((dataArray[indexPath.row] as AnyObject).value(forKey: "ratevalue") as! NSNumber))
            cell.btnInfo.setImage(UIImage(named:"information50.png"), for: .normal)
            cell.btnReview.setImage(UIImage(named:"review50.png"), for: .normal)
            cell.btnContact.setImage(UIImage(named:"contact_50.png"), for: .normal)
            
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let id = (dataArray[indexPath.row] as AnyObject).value(forKey: "centreID") as! NSNumber
        if SelectedcenterIdList.contains(id){
        SelectedcenterIdList.remove(id)
        selectedIndex.remove(indexPath.row)
        }else {
            if SelectedcenterIdList.count == 4{
                AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
                 self.view.makeToast(message: compareMoreThanFour)
                return
            }
        SelectedcenterIdList.add(id)
        selectedIndex.add(indexPath.row)
        }
       tblView.reloadData()
    
    }
    func getdata(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getCompareList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                debugPrint(response!)
                print(response?.object(forKey: "data") as! NSArray)
                self.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                if self.dataArray.count > 0{
                self.lblCompareListNil.text =  ""
                }
                self.tblView.reloadData()
            } else {
                self.lblCompareListNil.text =  response?.object(forKey: "message") as! String
               // self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    @IBAction func btnCompareClicked(_ sender: Any) {
   
        var message = String()
    var isError:Bool = false
        if ZamModel().isConnectedToNetwork() == false {
            isError = true
            message = networkError
        }else if selectedIndex.count < 2
        {
            isError = true
            message = compareNil
        }
        if isError == true{
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = message
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }else {
         getCompareddata()
        }
    }
    
    func getCompareddata(){
//        for i in 0..<selectedIndex.count{
//         let id = (dataArray[i] as AnyObject).value(forKey: "centreID") as? String
        let parameters = ["userid" : UserDefaults.standard.object(forKey: "userId") as! String,"CentreID":SelectedcenterIdList] as [String : Any]
       self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getComparedCenterList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                debugPrint(response)
                let controller:CenterComparisionViewController = CenterComparisionViewController(nibName: "CenterComparisionViewController", bundle: nil)
        
                self.comparedDataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                self.comparedCnameArray = (response?.object(forKey: "cName") as! NSArray).mutableCopy() as! NSMutableArray
                
                controller.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                controller.typeArray = (response?.object(forKey: "cName") as! NSArray).mutableCopy() as! NSMutableArray
//               let navController = UINavigationController(rootViewController: controller)
//               navController.isNavigationBarHidden = true
                // Creating a navigation controller with VC1 at the root of the navigation stack.
              //  self.navigationController?.pushViewController(controller, animated: false)
                self.present(controller, animated:true, completion: nil)
               // self.present(controller, animated: true, completion: nil)
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    // MARK: - Selectors
    func deleteClicked(sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        DeleteselectedIndex = (indexPath?.row)!
        
        let controller: AlertConfirmViewController =  AlertConfirmViewController(nibName: "AlertConfirmViewController", bundle: nil)
        controller.message = confirmRemoveFromCompare
         controller.comFromCompare = true
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
    func RateClicked(sender:UIButton){
    let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
    let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
    let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
    
    let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID),"userID":UserDefaults.standard.object(forKey: "userId") as! String,]
    
    let vc = CenterRateViewController(nibName: "CenterRateViewController", bundle: nil)
    appDelegate.comeFromCenterRate = "compare"
    vc.parameter = parameters as NSDictionary
    vc.centerID = String(describing: centreID)
    self.tabBarController?.tabBar.isHidden = true
    self.navigationController?.pushViewController(vc, animated: false)

    }
    func ReviewClicked(sender:UIButton){
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber
        let vc = CenterReviewViewController(nibName: "CenterReviewViewController", bundle: nil)
        vc.cenetrID = String(describing: centreID)
        appDelegate.comeFromCenterReview = "compare"
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    func InfoClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        let centreID = Int((dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "centreID") as! NSNumber)
        
        
        let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
        vc.CentreID = String(describing: centreID)
        vc.comeFromCompare = true
        //  print(response?.object(forKey: "data") as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    func btnContactClicked(sender:UIButton){
        let controller:ContactAlertViewController = ContactAlertViewController(nibName: "ContactAlertViewController", bundle: nil)
        
        let buttonPosition = sender.convert(CGPoint.zero, to: tblView)
        let indexPath = self.tblView.indexPathForRow(at: buttonPosition)
        print(dataArray[(indexPath?.row)!])
        controller.phoneNo = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "mobileNo") as! String
         controller.email = (dataArray[(indexPath?.row)!] as AnyObject).value(forKey: "emailID") as! String
        addChildViewController(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParentViewController: self)
    }
  
    
    @IBAction func sliderclicked(sender: AnyObject) {
       SliderOpen()
    }
    
   
    func colseSliderView(){
        if(ScrollEnable == true){
            ScrollEnable = false
            let viewMenuBack : UIView = view.subviews.last!
            viewMenuBack.removeFromSuperview()
        }
    }
    // Delegate
    func PopYesClicked(notification: Notification) {
        let centreID = (dataArray[(DeleteselectedIndex)] as AnyObject).value(forKey: "centreID") as! NSNumber
        
        if SelectedcenterIdList.contains(centreID){
            
            SelectedcenterIdList.remove(centreID)
            selectedIndex.remove(DeleteselectedIndex)
            
            let SelectedIndexArray = NSMutableArray()
            
            for i in 0..<selectedIndex.count{
            
                if (DeleteselectedIndex  > selectedIndex[i] as! Int){
                SelectedIndexArray.add(selectedIndex[i])
                }else {
                    let value = Int(selectedIndex[i] as! Int) - 1
                SelectedIndexArray.add(value)
                }
                
            }
            selectedIndex =  SelectedIndexArray
        }
        
        self.view.makeToastActivity(message: "Loading", view: self.view)
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"CentreID" : String(describing: centreID)]
        AppEngine().removeFromComare(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                 self.view.makeToast(message: response?.object(forKey: "message") as! String)
                self.dataArray.removeObject(at: (self.DeleteselectedIndex))
                self.tblView.reloadData()
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
       // self.navigationController?.popViewController(animated: true)
    }
    
    
    func CompareCenterDetailClicked(notification: Notification) {
        
        let vc = CenterDetailViewController(nibName: "CenterDetailViewController", bundle: nil)
        vc.CentreID = String(appDelegate.comparedCenterId)
        vc.comeFromComparision = true
        //  print(response?.object(forKey: "data") as! NSDictionary)
        self.navigationController?.pushViewController(vc, animated: false)

    }
    func CenterComparisionOpenFromCenterDetail(notification: Notification) {
        let controller:CenterComparisionViewController = CenterComparisionViewController(nibName: "CenterComparisionViewController", bundle: nil)
        controller.dataArray = comparedDataArray
        controller.typeArray = comparedCnameArray
          self.present(controller, animated:false, completion: nil)

    }
    
    @IBAction func btnSearchClicked(_ sender: Any) {
        
        let vc = SearchViewController(nibName: "SearchViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    func SwipeRightGesture(gesture: UIGestureRecognizer) {
        
        
        
        if appDelegate.SwipeRightOpen == false
        {
            SliderOpen()
        }
    }
    
    
    func SliderOpen(){
        
        let menuVC : SliderViewController = SliderViewController(nibName: "SliderViewController", bundle: nil)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        appDelegate.SwipeRightOpen = true
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.frame = CGRect(x:0 - UIScreen.main.bounds.size.width, y:0, width:UIScreen.main.bounds.size.width, height:UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame = CGRect(x:0 , y:0, width:UIScreen.main.bounds.size.width , height:UIScreen.main.bounds.size.height);
        }, completion:nil)
        
    }
    
    
    
    
}
