//
//  SearchViewController.swift
//  Zam
//
//  Created by Dinesh Rana on 1/13/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITabBarDelegate {

    @IBOutlet weak var txtFieldCityTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var txtFieldExam: UITextField!
    @IBOutlet weak var txtFieldSearch: UITextField!
    @IBOutlet weak var btnCenter: UIButton!
    @IBOutlet weak var btnTutor: UIButton!
    @IBOutlet weak var btnShop: UIButton!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    var cityList = NSMutableArray()
    var ExamListArray = NSMutableArray()
    var cityListMainArray = NSMutableArray()
    var ExamListMainArray = NSMutableArray()
    var centerListArray = NSMutableArray()
    var ExamList = String()
   // var selectedExamId = NSMutableArray()
    var selectedCityId:Int = 0
    var SearchType = "Centre"
     var IsPicked = "false"
    var centerId : NSNumber = 0
   
    var selectedTag = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getdata()
        ZamModel().textFieldCorner(txtField: txtFieldCity, radius: Int(8))
         ZamModel().textFieldCorner(txtField: txtFieldExam, radius: Int(8))
         ZamModel().textFieldCorner(txtField: txtFieldSearch, radius: Int(8))
        ZamModel().buttonCornerWithThemeBorder(Button: btnShop, radius: 5, borderWidth: 2)
         ZamModel().buttonCornerWithThemeBorder(Button: btnCenter, radius: 5, borderWidth: 2)
         ZamModel().buttonCornerWithThemeBorder(Button: btnTutor, radius: 5, borderWidth: 2)
        // Do any additional setup after loading the view.//  navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
//////////////
        let nibName1 = UINib(nibName: "InterestExamTableViewCell", bundle:nil)
        tblView.register(nibName1, forCellReuseIdentifier: "InterestExamTableViewCell")
        
        self.tblView.layer.shadowOffset = CGSize(width:0, height:5)
        self.tblView.layer.shadowColor = UIColor.black.cgColor
       // self.tblView.layer.shadowRadius = 2
        self.tblView.layer.masksToBounds = false
        self.tblView.layer.shadowOpacity = 0.4
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        txtFieldCity.text = appDelegate.currentState
    }
    func getdata(){
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        let parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"SearchType" :  "Centre"]
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getSearchCityList(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                let data = response?.object(forKey: "data")
//              self.cityList = ((data as AnyObject).object(forKey: "centreList") as! NSArray).mutableCopy() as! NSMutableArray
//            self.cityListMainArray = ((data as AnyObject).object(forKey: "centreList") as! NSArray).mutableCopy() as! NSMutableArray
                self.cityList = ((data as AnyObject).object(forKey: "cityList") as! NSArray).mutableCopy() as! NSMutableArray
                self.cityListMainArray = ((data as AnyObject).object(forKey: "cityList") as! NSArray).mutableCopy() as! NSMutableArray
            self.ExamListArray = ((data as AnyObject).object(forKey: "examList") as! NSArray).mutableCopy() as! NSMutableArray
            self.ExamListMainArray = ((data as AnyObject).object(forKey: "examList") as! NSArray).mutableCopy() as! NSMutableArray
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    //////// Keyboard notification
    func keyboardWillShow(notification: NSNotification) {
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 200
        self.scrollView.contentInset = contentInset
    }
    func keyboardWillHide(notification: NSNotification) {
        let contentInset:UIEdgeInsets = .zero
        self.scrollView.contentInset = contentInset
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - TableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedTag == 1
        {
            return cityList.count
        }else if selectedTag == 2
        {
            return ExamListArray.count
        }
        else
        {
        return centerListArray.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
            let cell:InterestExamTableViewCell = tableView.dequeueReusableCell(withIdentifier: "InterestExamTableViewCell") as! InterestExamTableViewCell
        if selectedTag == 1{
          cell.lblName.text = cityList[indexPath.row] as? String
        }else if selectedTag == 2{
        cell.lblName.text = ExamListArray[indexPath.row] as? String
        }else {
            if  SearchType == "Shop"{
              cell.lblName.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "productName") as? String
            }else{
                if SearchType == "Trainer"{
                 cell.lblName.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "trainerName") as? String
                }else {
                 cell.lblName.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "centreName") as? String
                }
             
            }
        }
           return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          tblView.isHidden = true
        if selectedTag == 1{
          txtFieldCity.text = cityList[indexPath.row] as? String
            
            let predicate = NSPredicate(format: "SELF.cityName == '\(txtFieldCity.text!)'")
            let list = cityListMainArray.filtered(using: predicate) as NSArray
            selectedCityId = Int((list[0] as AnyObject).value(forKey: "cityID") as! NSNumber)
        }
        else if selectedTag == 2 {
            
           let str = txtFieldExam.text!
           let arr1  = str.components(separatedBy: ",").dropLast()
            if arr1.count == 0{
            ExamList = ""
            }else {
            for i in 0..<arr1.count {
            ExamList = ExamList + arr1[i] + ","
            }
            }
            txtFieldExam.text =  String(ExamList) + (ExamListArray[indexPath.row] as? String)! + ","
            ExamList = txtFieldExam.text!
            
//            if  SearchType == "Shop"{
//                self.getSearchShopByKeyword(centerID: String(describing: self.centerId))
//            }
            }
        else if selectedTag == 3{
            if  SearchType == "Shop"{
                self.txtFieldSearch.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "productName") as? String
                
                
                print(centerListArray[indexPath.row])
                let story = UIStoryboard(name: "ShopStory", bundle: nil)
                let VC = story.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetails
                //                    print(VC.productDetail)
                //  VC.navigate_Serach = true
                VC.marketPlaceID = (centerListArray[indexPath.row] as AnyObject).value(forKey: "productID") as! NSNumber
                self.navigationController?.pushViewController(VC, animated: false)
                
               // getSearchShopByKeyword(centerID: String(describing: ((centerListArray[indexPath.row] as AnyObject).value(forKey: "productID") as? NSNumber)))
            }else{
                
                if SearchType == "Trainer"{
                    txtFieldCity.resignFirstResponder()
                    txtFieldExam.resignFirstResponder()
                    txtFieldSearch.resignFirstResponder()
                    
                    self.txtFieldSearch.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "trainerName") as? String
//                    IsPicked = "true"
//                    self.centerId = 0
                    let vc = CenterTrainerProfileViewController(nibName: "CenterTrainerProfileViewController", bundle: nil)
                    vc.trainerId = String(describing: ((centerListArray[indexPath.row] as AnyObject).value(forKey: "trainerID") as! NSNumber))
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                   //  self.getSearchCenterByKeyword()
//                     self.txtFieldSearch.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "trainerID") as? String
                    
                    
                }else {
                    self.txtFieldSearch.text = (centerListArray[indexPath.row] as AnyObject).value(forKey: "centreName") as? String
                    IsPicked = "true"
                    self.centerId = ((centerListArray[indexPath.row] as AnyObject).value(forKey: "centreID") as? NSNumber)!
                     self.getSearchCenterByKeyword()
                }
            }
        }
        }
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "\n"{
            textField.resignFirstResponder()
            if  SearchType == "Shop"{
                 self.getSearchShopByKeyword(centerID: "0")
            
            }else {
                if txtFieldCity.text != ""{
                    self.getSearchCenterByKeyword()
                }else if selectedCityId == 0{
                    let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
                    controller.message = CityNil
                    addChildViewController(controller)
                    controller.view.frame = view.bounds
                    view.addSubview(controller.view)
                    controller.didMove(toParentViewController: self)
                }else {
                    self.getSearchCenterByKeyword()
                }
            }
        }
        else {
            if selectedTag == 1 {
                let data = cityListMainArray.value(forKey: "cityName") as! NSArray
                let str = textField.text! + string
                let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", str)
                let array = data.filtered(using: searchPredicate)
                cityList = array as! NSMutableArray
                if cityList.count > 0 {
                tblView.isHidden = false
                }else {
                tblView.isHidden = true
                }
                tblView.reloadData()
                
            }else if selectedTag == 2{
                tblView.isHidden = false
                let data = ExamListMainArray.value(forKey: "examName") as! NSArray
                let str = textField.text! + string
                let strdata = str.components(separatedBy: ",")
                let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", strdata.last!)
                let array = data.filtered(using: searchPredicate)
                
                ExamListArray = array as! NSMutableArray
                if ExamListArray.count > 0 {
                    tblView.isHidden = false
                }else {
                    tblView.isHidden = true
                }
                tblView.reloadData()
            }else if selectedTag == 3{
                
                let newLength = (textField.text?.characters.count)! + string.characters.count - range.length
               if newLength == 0{
                tblView.isHidden = true
                }else {
                 getCenterdata(SearchKey: textField.text!)
                }
            }
            
        }
       
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.tblView.isHidden = true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
      //  tblView.isHidden = false
        if textField.tag == 1{
            selectedTag = 1
            tblTopConstraint.constant = 8
           // tblView.reloadData()
        }else if textField.tag == 2{
            selectedTag = 2
            if SearchType == "Shop"{
            tblTopConstraint.constant = 8
            }else {
            tblTopConstraint.constant = 70
            }
           // tblView.reloadData()
        }else {
             selectedTag = 3
            if SearchType == "Shop"{
                tblTopConstraint.constant = 70
            }else {
                tblTopConstraint.constant = 138
            }
            // tblView.reloadData()
        }
    }
    
    
    //MARK: - Actions
    
    @IBAction func btnCenterClicked(_ sender: Any) {
        SearchType = "Centre"
        btnShop.backgroundColor = UIColor.white
        btnTutor.backgroundColor = UIColor.white
        btnCenter.backgroundColor = ZamModel().themecolor
        txtFieldCity.isHidden = false
        txtFieldCityTopConstraint.constant = 102
        
        btnShop.setTitleColor(ZamModel().themecolor, for: .normal)
        btnTutor.setTitleColor(ZamModel().themecolor, for: .normal)
        btnCenter.setTitleColor(UIColor.white, for: .normal)
        tblView.isHidden = true
        ExamList = ""
        centerId = 0
         txtFieldCity.text = appDelegate.currentState
        txtFieldExam.text = ""
        txtFieldSearch.text = ""
        
    }
    @IBAction func btnTutorClicked(_ sender: Any)
    { tblView.isHidden = true
        SearchType = "Trainer"
        btnShop.backgroundColor = UIColor.white
        btnTutor.backgroundColor = ZamModel().themecolor
        btnCenter.backgroundColor = UIColor.white
        txtFieldCity.isHidden = false
        txtFieldCityTopConstraint.constant = 102
        
        btnShop.setTitleColor(ZamModel().themecolor, for: .normal)
        btnTutor.setTitleColor(UIColor.white, for: .normal)
        btnCenter.setTitleColor(ZamModel().themecolor, for: .normal)
        
        ExamList = ""
        centerId = 0
        txtFieldCity.text = appDelegate.currentState
        txtFieldExam.text = ""
        txtFieldSearch.text = ""

    }
    @IBAction func btnShopClicked(_ sender: Any) {
        tblView.isHidden = true
        SearchType = "Shop"
        btnShop.backgroundColor = ZamModel().themecolor
        btnTutor.backgroundColor = UIColor.white
        btnCenter.backgroundColor = UIColor.white
        txtFieldCity.isHidden = true
        txtFieldCityTopConstraint.constant = 37
        btnShop.setTitleColor(UIColor.white, for: .normal)
        btnTutor.setTitleColor(ZamModel().themecolor, for: .normal)
        btnCenter.setTitleColor(ZamModel().themecolor, for: .normal)
        ExamList = ""
        centerId = 0
        txtFieldCity.text = ""
        txtFieldExam.text = ""
        txtFieldSearch.text = ""
        
        
    }
    
    func getCenterdata(SearchKey :String){
        
         let array = ExamList.components(separatedBy: ",")
        let selectedExamId = NSMutableArray()
        for i in 0..<array.count{
            let predicate = NSPredicate(format: "SELF.examName == '\(array[i])'")
            let list = ExamListMainArray.filtered(using: predicate) as NSArray
            
            if list.count>0{
                selectedExamId.add((list[0] as AnyObject).value(forKey: "examID")!)
            }
        }
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        var parameters = NSDictionary()
        if  SearchType == "Shop"{
         parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"SearchType" :  SearchType ,"CityID": "0","ExamID[]" :selectedExamId,"keywords":SearchKey] as [String : Any] as NSDictionary
        }else {
   //     print(selectedCityId)
            if selectedExamId.count > 0{
             parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"SearchType" :  SearchType ,"CityID": String(describing: selectedCityId),"ExamID[]" :selectedExamId,"keywords":SearchKey,"CityName":txtFieldCity.text!] as [String : Any] as NSDictionary
            }else {
             parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"SearchType" :  SearchType ,"CityID": String(describing: selectedCityId),"ExamID[]" :"0","keywords":SearchKey,"CityName":txtFieldCity.text!] as [String : Any] as NSDictionary
            }
        }
       // self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getSearchByKeyword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            //self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                 self.centerListArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                if  self.centerListArray.count > 0{
                self.tblView.isHidden = false
                self.tblView.reloadData()
                }else {
                self.tblView.isHidden = true
                }
            } else {
              //  self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    
    func getSearchCenterByKeyword(){
        txtFieldCity.resignFirstResponder()
        txtFieldExam.resignFirstResponder()
        txtFieldSearch.resignFirstResponder()
        let selectedExamId = NSMutableArray()
        if txtFieldExam.text! != ""{
            let array = ExamList.components(separatedBy: ",")
            for i in 0..<array.count{
                let predicate = NSPredicate(format: "SELF.examName == '\(array[i])'")
                let list = ExamListMainArray.filtered(using: predicate) as NSArray
                if list.count>0{
                    selectedExamId.add((list[0] as AnyObject).value(forKey: "examID")!)
                }
        }
        }
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        var parameters = NSDictionary()
            if selectedExamId.count == 0 {
                parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"SearchType" :  SearchType ,"CityID": String(describing: selectedCityId),"ExamID" :"0","keywords":txtFieldSearch.text!,"IsPicked": IsPicked,"centreID": String(describing: centerId),"CityName" : txtFieldCity.text!,"ExamName":txtFieldExam.text!] as [String : Any] as NSDictionary
            }
            else {
                parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"SearchType" :  SearchType ,"CityID": String(describing: selectedCityId),"ExamID" :selectedExamId,"keywords":txtFieldSearch.text!,"IsPicked": IsPicked,"centreID": String(describing: centerId),"CityName" : txtFieldCity.text!,"ExamName":txtFieldExam.text!] as [String : Any] as NSDictionary
            }
        print(parameters)
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getSearchCenterByKeyword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
           self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                if  self.SearchType == "Centre"{
                   // let vc = NearViewController(nibName: "NearViewController", bundle: nil)
                    self.appDelegate.comeFromSearchOnMap = true
                    self.appDelegate.searchparameter = parameters
                  //  vc.comeFromSearch = true
                  //  vc.Searchparameter = parameters
                  //  self.navigationController?.pushViewController(vc, animated: true)
                    self.appDelegate.currentState = self.txtFieldCity.text!
                    if  self.tabBarController!.selectedIndex == 0{
                    self.navigationController?.popViewController(animated: false)
                    }else{
                    self.tabBarController?.selectedIndex = 0
                    self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers?[0]
                    self.navigationController?.viewControllers.removeLast()
                    }
//                    print(response?.object(forKey: "data") as! NSArray)
                    
                  //  vc.dataArray = response?.object(forKey: "data") as! NSArray
                
                }else if self.SearchType == "Trainer"{
                    
                    if self.IsPicked == "true"{
                    
                    print("Open trainer profile")
                    }else {
                        let vc = TutorViewController(nibName: "TutorViewController", bundle: nil)
                        vc.dataArray = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
//                let vc = NearViewController(nibName: "NearViewController", bundle: nil)
//                vc.comeFromSearch = true
//                vc.dataArray = response?.object(forKey: "data") as! NSArray
                print(response?.object(forKey: "data") as! NSArray)
              //  self.navigationController?.pushViewController(vc, animated: false)
            } else {
                
                if  self.SearchType == "Centre"{
                    // let vc = NearViewController(nibName: "NearViewController", bundle: nil)
                    self.appDelegate.comeFromSearchOnMap = true
                    self.appDelegate.searchparameter = parameters
                    //  vc.comeFromSearch = true
                    //  vc.Searchparameter = parameters
                    //  self.navigationController?.pushViewController(vc, animated: true)
                    if  self.tabBarController!.selectedIndex == 0{
                        self.navigationController?.popViewController(animated: false)
                    }else{
                        self.tabBarController?.selectedIndex = 0
                        self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers?[0]
                        self.navigationController?.viewControllers.removeLast()
                    }
                    
                    //  vc.dataArray = response?.object(forKey: "data") as! NSArray
                    
                }
                
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    func getSearchShopByKeyword(centerID:String){
        
        let array = ExamList.components(separatedBy: ",")
         let selectedExamId = NSMutableArray()
        for i in 0..<array.count{
            let predicate = NSPredicate(format: "SELF.examName == '\(array[i])'")
            let list = ExamListMainArray.filtered(using: predicate) as NSArray
            if list.count>0{
                selectedExamId.add((list[0] as AnyObject).value(forKey: "examID")!)
            }
        }
        if ZamModel().isConnectedToNetwork() == false {
            let controller:AlertViewController = AlertViewController(nibName: "AlertViewController", bundle: nil)
            controller.message = networkError
            addChildViewController(controller)
            controller.view.frame = view.bounds
            view.addSubview(controller.view)
            controller.didMove(toParentViewController: self)
            return
        }
        
        
        var parameters = NSDictionary()
        if selectedExamId.count == 0
        {
          parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID": UserDefaults.standard.object(forKey: "userId") as! String ,"ExamID" :"0","SearchWord":txtFieldSearch.text!,"ExamName": txtFieldExam.text!,"SearchType" : "Shop"] as [String : Any] as NSDictionary
        }
        else
        {
            parameters = ["PrimaryInfoID" : UserDefaults.standard.object(forKey: "primaryInfoId") as! String,"userID": UserDefaults.standard.object(forKey: "userId") as! String,"ExamID" :selectedExamId,"SearchWord":txtFieldSearch.text!,"ExamName": txtFieldExam.text!,"SearchType" : "Shop"] as [String : Any] as NSDictionary
        }
        print(parameters )
        self.view.makeToastActivity(message: "Loading", view: self.view)
        AppEngine().getSearchCenterByKeyword(parameter: parameters as NSDictionary) { (response : NSDictionary?) in
            self.view.hideToastActivity(view: self.view)
            if response == nil{
                self.view.makeToast(message: ServerError)
                return
            }
            if response?.object(forKey: "success") as! Bool  == true{
                
                print(response?.object(forKey: "data"))
                
                
                 print(response?.object(forKey: "data") as! NSArray)
                if centerID == "0"
                {
                    print("Shop Controller")
//                    let vc = ShopViewController(nibName: "ShopViewController", bundle: nil)
                   let dataArray = (response?.object(forKey: "data") as! NSArray)
//                    vc.navigate_Search = true
                    let listProduct = NSKeyedArchiver.archivedData(withRootObject: dataArray)
                    UserDefaults.standard.set(listProduct, forKey: "dataList")
                    UserDefaults.standard.setValue(true, forKey: "navigate_Search")
                    
                    if  self.tabBarController!.selectedIndex == 2{
                        self.navigationController?.popViewController(animated: false)
                    }else{
                       
                        self.tabBarController?.selectedIndex = 2
                        self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers?[2]
                         self.navigationController?.viewControllers.removeLast()
                    }
                    
//                     self.tabBarController?.selectedIndex = 2
//                    self.tabBarController?.selectedViewController = self.tabBarController?.viewControllers?[2]
                   //self.navigationController?.pushViewController(vc, animated: true)
                    // Sho  Controller
                }else
                {
                    let story = UIStoryboard(name: "ShopStory", bundle: nil)
                    let VC = story.instantiateViewController(withIdentifier: "ProductDetail") as! ProductDetails
                    let arr = (response?.object(forKey: "data") as! NSArray).mutableCopy() as! NSMutableArray
                    let dicDetail = arr[0] as! NSDictionary
//                    print(VC.productDetail)
                  //  VC.navigate_Serach = true
                    VC.marketPlaceID = dicDetail["marketPlaceID"] as! NSNumber
                    self.navigationController?.pushViewController(VC, animated: false)
                     print("Shop Detail")
                  //
                }
            } else {
                self.view.makeToast(message: response?.object(forKey: "message") as! String)
            }
        }
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    

}
