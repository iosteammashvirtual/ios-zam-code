//
//  AppEngine.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import Foundation
class AppEngine{

    
    
    ///////// Login & Register
    
    func LoginAuth( parameter:NSDictionary ,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.login(parameter: parameter,completionHandler: completionHandler)
    }
    func Register( parameter:NSDictionary ,url :String ,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.Register(parameter: parameter,url: url,completionHandler: completionHandler)
    }
    func GetAllRolesForApp(completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.GetAllRolesForApp(completionHandler: completionHandler)
    }
    
    func ForgotPassword(parameter:NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.ForgotPassword(parameter: parameter,completionHandler: completionHandler)
    }
    func Resetpassword(parameter:NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.Resetpassword(parameter: parameter,completionHandler: completionHandler)
    }
    func changePassword(parameter:NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.changePassword(parameter: parameter,completionHandler: completionHandler)
    }
    func createExaminterest(parameter:NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objLoginHandler = LoginHandler()
        objLoginHandler.CretaeExamInterest(parameter: parameter,completionHandler: completionHandler)
    }

    
   /////// map
    
    func getCenterList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objNearMeHandler = NearMeHandler()
        objNearMeHandler.getCenterList(parameter: parameter , completionHandler: completionHandler)
    }
    func getCenterDetail(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objNearMeHandler = NearMeHandler()
        objNearMeHandler.getCenterDetail(parameter: parameter , completionHandler: completionHandler)
    }
    
    ////// compare
    
    func addToCompare(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objCompareHandler = CompareHandler()
        objCompareHandler.AddtoCompare(parameter: parameter , completionHandler: completionHandler)
    }
    func removeFromComare(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objCompareHandler = CompareHandler()
        objCompareHandler.removeFromCompare(parameter: parameter , completionHandler: completionHandler)
    }
    func getCompareList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objCompareHandler = CompareHandler()
        objCompareHandler.getCompareList(parameter: parameter , completionHandler:completionHandler)
    
    }
    func getComparedCenterList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objCompareHandler = CompareHandler()
        objCompareHandler.getCentreComparison(parameter: parameter , completionHandler:completionHandler)
        
    }
    
    /// User info
    func getUserDetails(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getUserDetails(parameter: parameter , completionHandler: completionHandler)
    }
    func getUserInfo(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getUserInfo(parameter: parameter , completionHandler: completionHandler)
    }
    func getCityList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getCityList(parameter: parameter , completionHandler: completionHandler)
    }
    func updateProfile(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.updateProfile(parameter: parameter , completionHandler: completionHandler)
    }
    func uploaduserProfile( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.uploaduserProfile(parameter: parameter , completionHandler: completionHandler)
    }
    
    func updateUserRole( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.updateUserRole(parameter: parameter , completionHandler: completionHandler)
    }
    func getAboutUsData(completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getAboutUsData(completionHandler: completionHandler)
    }
    
    func getMyReviewList( parameter : NSDictionary ,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getMyReviewList(parameter: parameter , completionHandler: completionHandler)
    }
    func getMyRateList( parameter : NSDictionary ,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getMyRateList(parameter: parameter , completionHandler: completionHandler)
    }
    
    func GetTrainerInterestList( parameter : NSDictionary ,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.GetTrainerInterestList(parameter: parameter , completionHandler: completionHandler)
    }

   
    
    
    

    
    
    
    
    func addAssociatedCentre( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.addAssociatedCentre(parameter: parameter , completionHandler: completionHandler)
    }
    
    func removeAssociatedCentre( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.removeAssociatedCentre(parameter: parameter , completionHandler: completionHandler)
    }

    
    
 ///////////////////////// Search
    
    func getSearchCityList( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = SearchHandler()
        objUserInfoHandler.getSearchCityList(parameter: parameter , completionHandler: completionHandler)
    }
    func getSearchCenterByKeyword( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = SearchHandler()
        objUserInfoHandler.getSearchCenterByKeyword(parameter: parameter , completionHandler: completionHandler)
    }
    
    func getSearchByKeyword( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = SearchHandler()
        objUserInfoHandler.getSearchByKeyword(parameter: parameter , completionHandler: completionHandler)
    }
    
    func GetSearchCentreAssociateByKeyword( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = SearchHandler()
        objUserInfoHandler.GetSearchCentreAssociateByKeyword(parameter: parameter , completionHandler: completionHandler)
    }
    
    func GetSearchShopByKeyword( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = SearchHandler()
        objUserInfoHandler.GetSearchShopByKeyword(parameter: parameter , completionHandler: completionHandler)
    }
    
    
    
    /////////////////////////////////   Trainer Detail
    
    
    func getTrainerDetail(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getTrainerDetail(parameter: parameter , completionHandler: completionHandler)
    }
    func getCenterCoursesList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = UserInfoHandler()
        objUserInfoHandler.getCenterCoursesList(parameter: parameter , completionHandler: completionHandler)
    }
    ///////// Trainer Rate & Review
    
    
    
    func getTrainerrrateData(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.getTrainerrrateData(parameter: parameter , completionHandler: completionHandler)
    }
    
    func getTrainerRatingCriteria(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.getTrainerRatingCriteria(parameter: parameter , completionHandler: completionHandler)
    }
    func addtrainerRating(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.addtrainerRating(parameter: parameter , completionHandler: completionHandler)
    }
    func setTrainerRecomandation(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.setTrainerRecomandation(parameter: parameter , completionHandler: completionHandler)
    }
    
    func getTrainerReview(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.getTrainerReview(parameter: parameter , completionHandler: completionHandler)
    }
    func setTrainerReview(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.setTrainerReview(parameter: parameter , completionHandler: completionHandler)
    }

    

    
    
    
    
    
    
    
    
    
    
    
    
    //////////////////////// Rate Handler
    
    func getCenterrateData(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.getCenterrateData(parameter: parameter , completionHandler: completionHandler)
    }
    
    
    func getCenterratingCriteria(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.getCenterratingCriteria(parameter: parameter , completionHandler: completionHandler)
    }
    
    func addCentrerating(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.addCentrerating(parameter: parameter , completionHandler: completionHandler)
    }
    
    func setUserRecommenndation(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = RateHandler()
        objUserInfoHandler.setUserRecommenndation(parameter: parameter , completionHandler: completionHandler)
    }
    
    
    
    ////////////////   Center Review
    
    
    func getCenterReviewList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = ReviewHandler()
        objUserInfoHandler.getCenterReviewList(parameter: parameter , completionHandler: completionHandler)
    }
    func saveCenterReview(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objUserInfoHandler = ReviewHandler()
        objUserInfoHandler.saveCenterReview(parameter: parameter , completionHandler: completionHandler)
    }

    
    

    
    

    
    //Shop info
    func getProductList( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getProductList(parameter: parameter, completionHandler: completionHandler)
        
    }
    func addToCart( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.addToCart(parameter: parameter, completionHandler: completionHandler)
        
    }
    func getAddList( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getAddList(parameter: parameter, completionHandler: completionHandler)
        
    }
    func getProductDetails( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getProductDetails(parameter: parameter, completionHandler: completionHandler)
        
    }
    
    func getCartList( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getCartList(parameter: parameter, completionHandler: completionHandler)
    }
    
    func removefromCart( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.removefromCart(parameter: parameter, completionHandler: completionHandler)
    }
    func updateCart( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.updateCart(parameter: parameter, completionHandler: completionHandler)
    }
    
    func getShipping( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getShipping(parameter: parameter, completionHandler: completionHandler)
    }
    
    func getStateList( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getStateList(parameter: parameter, completionHandler: completionHandler)
    }
    
    func getCityList_State( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.getCityList_State(parameter: parameter, completionHandler: completionHandler)
    }
    func createShipping( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.createShippig(parameter: parameter, completionHandler: completionHandler)
    }
    func buyNow( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.buyNow(parameter: parameter, completionHandler: completionHandler)
    }
    func PayNow( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.PayNow(parameter: parameter, completionHandler: completionHandler)
    }
    func GetCouponDiscount( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.GetCouponDiscount(parameter: parameter, completionHandler: completionHandler)
    }
    func GetProductRating( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.GetProductRating(parameter: parameter, completionHandler: completionHandler)
    }
    func AddProductRating( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.AddProductRating(parameter: parameter, completionHandler: completionHandler)
    }
    func GetProductRatingCriteria( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.GetProductRatingCriteria(parameter: parameter, completionHandler: completionHandler)
    }
    func UpdateProductRecomandation( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.UpdateProductRecomandation(parameter: parameter, completionHandler: completionHandler)
    }
    func GetProductReview( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.GetProductReview(parameter: parameter, completionHandler: completionHandler)
    }
    
    func AddProductReview( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.AddProductReview(parameter: parameter, completionHandler: completionHandler)
    }
    func GetCourseRating( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.GetCourseRating(parameter: parameter, completionHandler: completionHandler)
    }
    func CourseRatingCriteria( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.CourseRatingCriteria(parameter: parameter, completionHandler: completionHandler)
    }
    func CourseAddRating( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.CourseAddRating(parameter: parameter, completionHandler: completionHandler)
    }
    func GetCourseReview( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.GetCourseReview(parameter: parameter, completionHandler: completionHandler)
    }
    func AddCourseReview( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.AddCourseReview(parameter: parameter, completionHandler: completionHandler)
    }
    func UpdateCourseRecomandation( parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let objshopandler = ShopHandler()
        objshopandler.UpdateCourseRecomandation(parameter: parameter, completionHandler: completionHandler)
    }
 
    
    
}
