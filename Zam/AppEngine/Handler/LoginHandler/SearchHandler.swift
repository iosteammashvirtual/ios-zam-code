//
//  SearchHandler.swift
//  Zam
//
//  Created by Dinesh Rana on 1/13/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import Foundation
import Alamofire
class SearchHandler {
    
    
    func getSearchCityList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetSearchCity, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    func getSearchByKeyword(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetSearchKeywords, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    
    
    
    func getSearchCenterByKeyword(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + SearchCentrebyKeyword, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    func GetSearchCentreAssociateByKeyword(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetSearchCentreAssociate, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    
    func GetSearchShopByKeyword(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + SearchCentrebyKeyword, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    
    
   
    
    
    
    
}
