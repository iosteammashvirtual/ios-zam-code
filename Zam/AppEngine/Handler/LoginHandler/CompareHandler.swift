//
//  CompareHandler.swift
//  Zam
//
//  Created by Dinesh Rana on 1/5/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import Foundation
import Alamofire


class CompareHandler{
func AddtoCompare(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
{
    let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
    
    Alamofire.request(appMainUrl + AddToFavourite, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
        debugPrint(response)
        switch(response.result) {
        case .success(_):
            if let data = response.result.value{
                debugPrint(data)
                completionHandler(data as? NSDictionary)
            }
        case .failure(_):
            completionHandler(nil)
            
        }
    }
    
}
    
 func removeFromCompare(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + RemoveFavouriteFromList, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    
    
    
    func getCompareList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetUserFavorites, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    func getCentreComparison(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + CentreComparison, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }

}
