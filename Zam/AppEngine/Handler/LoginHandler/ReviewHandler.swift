//
//  ReviewHandler.swift
//  Zam
//
//  Created by Dinesh Rana on 1/24/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import Foundation
import Alamofire
class ReviewHandler{

    func getCenterReviewList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + ViewCentreReviews, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    
    func saveCenterReview(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + AddCentreRiview, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }

    
    

}
