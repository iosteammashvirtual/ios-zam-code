//
//  UserInfoHandler.swift
//  Zam
//
//  Created by Dinesh Rana on 1/6/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import Foundation
import Alamofire


class UserInfoHandler{

    func getUserInfo(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        
        Alamofire.request(appMainUrl + GetUserInfo, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    func getUserDetails(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        
        Alamofire.request(appMainUrl + GetUserDetails, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    func updateProfile(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + UpdateProfile, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    
    func updateUserRole(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + UpdateRole, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    func getAboutUsData(completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + AboutApp, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    func getMyReviewList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + ViewAllUserReviews, method: .post, parameters: parameter as! Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    func getMyRateList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + ViewAllUserRates, method: .post, parameters: parameter as! Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    func GetTrainerInterestList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetTrainerInterest, method: .post, parameters: parameter as! Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
  
    
//    func uploaduserProfile(image :UIImage , parameters : [String:String],completionHandler: @escaping (NSDictionary?) -> ()) {
//        //let image = UIImage(named: "bodrum")!
//        // define parameters
////        let parameters = [
////            "hometown": "yalikavak",
////            "living": "istanbul"
////        ]
//         let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
//        Alamofire.upload(multipartFormData: { multipartFormData in
//            if let imageData = UIImageJPEGRepresentation(image, 1) {
//                multipartFormData.append(imageData, withName: "UserPhoto", fileName: "file.png", mimeType: "image/png")
//            }
//            for (key, value) in parameters {
//                multipartFormData.append((value.data(using: .utf8))!, withName: key)
//            }}, to: appMainUrl+UpdateProfileImage, method: .post, headers: header,
//                encodingCompletion: { encodingResult in
//                    switch encodingResult {
//                    case .success(let upload, _, _):
//                        upload.response { [weak self] response in
////                            guard self != nil else {
////                               // return
////                            }
//                             completionHandler(response as? NSDictionary)
//                            debugPrint(response.data)
//                        }
//                    case .failure(let encodingError):
//                        print("error:\(encodingError)")
//                        completionHandler(nil)
//                    }
//        })
//    }
    func uploaduserProfile(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + UpdateProfileImage, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
   
    func getCityList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        
        Alamofire.request(appMainUrl + GetCityList, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    
  
    
    func getTrainerDetail(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        
        Alamofire.request(appMainUrl + ViewTrainerProfile, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
           // debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                   // debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    
    
    func getCenterCoursesList(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        
        Alamofire.request(appMainUrl + GetCentreCourses, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
    func addAssociatedCentre(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        
        Alamofire.request(appMainUrl + CreateCentreAssociate, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
        
    }
        func removeAssociatedCentre(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
        {
            let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
            
            Alamofire.request(appMainUrl + RemoveCentreAssociate, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
                debugPrint(response)
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value{
                        debugPrint(data)
                        completionHandler(data as? NSDictionary)
                    }
                case .failure(_):
                    completionHandler(nil)
                    
                }
            }
        
    }

    
    
    
    
    


    
}
