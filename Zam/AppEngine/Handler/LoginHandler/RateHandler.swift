//
//  RateHandler.swift
//  Zam
//
//  Created by Dinesh Rana on 1/24/17.
//  Copyright © 2017 Dinesh Rana. All rights reserved.
//

import Foundation
import Alamofire
class RateHandler{
    
    func getCenterrateData(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + ViewAllCentreRates, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func getCenterratingCriteria(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetRatingCritaria, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    
    func addCentrerating(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + AddCentreRate, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func setUserRecommenndation(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + UpdateUserRecomandation, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    
    
 /////////// TRAINER 
    
    func getTrainerrrateData(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + ViewTrainerRates, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func getTrainerRatingCriteria(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + GetTrainerRatingCritaria, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func addtrainerRating(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + AddTrainerRate, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func setTrainerRecomandation(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + UpdateTrainerRecomandation, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func getTrainerReview(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + ViewTrainerReviews, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }
    func setTrainerReview(parameter : NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        let header = ["Authorization": "Bearer " + (UserDefaults.standard.object(forKey: "token") as! String)]
        Alamofire.request(appMainUrl + AddTrainerReview, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response:DataResponse<Any>) in
            debugPrint(response)
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
            case .failure(_):
                completionHandler(nil)
                
            }
        }
    }




    
  
  
    


}
