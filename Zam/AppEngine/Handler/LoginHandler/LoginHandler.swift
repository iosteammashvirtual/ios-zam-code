//
//  LoginHandler.swift
//  Zam
//
//  Created by Dinesh Rana on 12/29/16.
//  Copyright © 2016 Dinesh Rana. All rights reserved.
//

import Foundation
import Alamofire
class LoginHandler: NSObject {
    
    func login(parameter : NSDictionary , completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request( loginBase_url, method: .post, parameters: parameter as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    
                    completionHandler(data as? NSDictionary)
                }
                
            case .failure(_):
                 completionHandler(nil)
                
            }
        }
    }
    
    func Register(parameter : NSDictionary ,url: String , completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request( appAuthUrl + url, method: .post, parameters: parameter as? Parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    
                    completionHandler(data as? NSDictionary)
                }
                
            case .failure(_):
                completionHandler(nil)
                
            }
        }
   
    }
    func GetAllRolesForApp(completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request(appAuthUrl + GetAllRolesApp, method: .get, parameters: ["":""], encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                debugPrint(data)
               completionHandler(data as? NSDictionary)
                }
                break
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    
    
    func ForgotPassword(parameter: NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request(appAuthUrl + GenerateForgotOtp, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
                break
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    
    func Resetpassword(parameter: NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request(appAuthUrl + ResetPasswordWithOtp, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
                break
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    func changePassword(parameter: NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request(appAuthUrl + ChangePassword, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
                break
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
    func CretaeExamInterest(parameter: NSDictionary,completionHandler: @escaping (NSDictionary?) -> ())
    {
        Alamofire.request(appMainUrl + CreateExamsInterest, method: .post, parameters: parameter as? Parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                if let data = response.result.value{
                    debugPrint(data)
                    completionHandler(data as? NSDictionary)
                }
                break
            case .failure(_):
                completionHandler(nil)
            }
        }
    }
    
   
//   
//    func a(image :UIImage){
//        let parameters = [ "userId": ""  ]
//        
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            multipartFormData.append(UIImageJPEGRepresentation(image, 0.5)!, withName: "photo_path", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
//            for (key, value) in parameters {
//                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
//            }
//        }, to:"http://server1/upload_img.php")
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//                
//                upload.uploadProgress(closure: { (Progress) in
//                    print("Upload Progress: \(Progress.fractionCompleted)")
//                })
//                
//                upload.responseJSON { response in
//                    //self.delegate?.showSuccessAlert()
//                    print(response.request)  // original URL request
//                    print(response.response) // URL response
//                    print(response.data)     // server data
//                    print(response.result)   // result of response serialization
//                    //                        self.showSuccesAlert()
//                    //self.removeImage("frame", fileExtension: "txt")
//                    if let JSON = response.result.value {
//                        print("JSON: \(JSON)")
//                    }
//                }
//                
//            case .failure(let encodingError):
//                //self.delegate?.showFailAlert()
//                print(encodingError)
//            }
//            
//        }
//    }
    
    
}
